<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
require_once(__DIR__."/../../../../includes/node.mongo.config.php");

$url = $NODE_URL."/unit/list/material";
session_start();

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
}


if(!isset($_GET['q']) || !isset($_GET['id'])) {
    if(sizeof($rows) == 0) {
        displayError("Unit Details Not Entered");
        return;
    }
}

$UNIT_SUB_ID = $_GET['id'];
$COURSE_CODE = $_GET['q'];


$UNIT_ID = $COURSE_CODE.$UNIT_SUB_ID;

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $UNIT_ID];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    displayError("Unit Details Not Entered");
    return;
}

$UNIT_NAME = $rows[0]->unitName;


$data = array(
    'unitID' => $UNIT_ID,
    'courseID' => $COURSE_CODE
);

$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = json_decode(curl_exec($ch));

//close connection
curl_close($ch);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Session Rationale & Outcomes</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        .deleteBtn, .downloadBtn {
            cursor: pointer;
        }

    </style>
</head>
<body>

<nav>

    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-graduation-cap"></i> Learning Resources
                <span class="right"><div class="chip">Unit <?php echo $UNIT_SUB_ID." - ".$UNIT_NAME; ?></div></span><div class="clearfix"> </div>
            </h5>
            <hr>
<br>
            <h5>AVAILABLE MATERIAL</h5><br>
            <div id="availableQuestionDiv">

                <?php


                $isEmpty = true;
                if($result) {
                    foreach ($result as $d) {
                        $data = $d->name;
                        $isEmpty = false;
                        $hrefURL = $NODE_IP_ARR."/public?q=".$d->path;
                        echo "<div class=\"card\">
                        <div class=\"card-content\">
                            File Name: $data
                        </div>
                        <div class=\"card-action\">
                            <a target='_blank' href='$hrefURL' class=\"blue-text downloadBtn text-darken-1\"><i class=\"fa fa-download\"></i>&nbsp; Download</a>
                            </div>
                    </div>";
                    }
                }


                if($isEmpty) {
                    echo "<div class='center'><div class='chip'>NO FILE PRESENT</div></div>";
                }


                ?>

            </div>
        </div>
    </div>



    <!-- JS FILES DON'T CHANGE -->
    <script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
    <script type="text/javascript" src="script.js"></script>

</body>
</html>
