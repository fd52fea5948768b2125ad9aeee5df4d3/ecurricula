<?php
    include_once(__DIR__."/../../../includes/general.config.php");
    include_once(__DIR__."/../../../includes/mongo.db.config.php");

    session_start();
    if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
        $errorMsg = "Bag Request... Login Again";
        $BASE_PATH = "../../..";
        include_once(__DIR__."/../../../error.php");
        echo $errorHTML;
        exit;
    }


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
}


$MAPPING_DATA = $A_K_MAPPING;

$DEPARTMENTS_LIST = ["Computer Science and Eng.",'Information Technology','Software Engineering',
    'Civil Engineering','Mechanical engineering', 'Automobile Engineering',
    'Aerospace Engineering','Mechatronics','Electronics and Comm.','Telecommunication',
    'Electrical & Electronics','Electronics and Instru.','Instru. & Ctrl Eng',
    'Chemical Engineering','Biotechnology','Biomedical Engineering','Genetic Engineering',
    'Food Process Engineering','Nanotechnology','Nuclear Engineering','Chemistry'];


    $username = $_SESSION['username'];
    $courseCode = $_SESSION['clcp_courseCode'];

    $studentDBName = "STD_DB_".$username;
    $courseDBName = "COURSE_TABLE";
    $unitDBName = "UNITS_TABLE";

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['courseCode' => $courseCode];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.$courseDBName",$query);
    $rows = $rows->toArray();
    $len = sizeof($rows);

    if($len != 1) {
        $errorMsg = "Invalid Course Selected";
        $BASE_PATH = "../../..";
        include_once(__DIR__."/../../../error.php");
        echo $errorHTML;
        exit;
    }

    //Get static Data
    $CLR = $rows[0]->clr;
    $STANDARD = $rows[0]->standards;
    $PRE_REQUISITE = join(" ; ",$rows[0]->requisite->pre);
    $CO_REQUISITE = join(" ; ",$rows[0]->requisite->co);
    $PROGRESSIVE_COURSE = join(" ; ",$rows[0]->progressiveCourse);
    $COURSE_NAME = $rows[0]->courseName;
    $DEPARTMENT = $DEPARTMENTS_LIST[intval($rows[0]->department)];
    $COURSE_CATEGORY_CODE = $rows[0]->courseCategory->categoryCode;
    $COURSE_CATEGORY_NAME = $rows[0]->courseCategory->categoryName;
    $L = $rows[0]->courseCategory->l;
    $T = $rows[0]->courseCategory->t;
    $P = $rows[0]->courseCategory->p;
    $C = $rows[0]->courseCategory->c;
    $CLO = $rows[0]->clo;

    $query = ['courseCode' => $courseCode];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.$unitDBName",$query);
    $rows = $rows->toArray();
    $len = sizeof($rows);

    if($len != 5) {
        $errorMsg = "Invalid Course Selected";
        $BASE_PATH = "../../..";
        include_once(__DIR__."/../../../error.php");
        echo $errorHTML;
        exit;
    }

    $regex = new MongoDB\BSON\Regex("$courseCode([1-5])$");
    $query = ['_id' => $regex];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $unitRows = $client->executeQuery("$DB_NAME.$studentDBName",$query);
    $unitRows = $unitRows->toArray();
    $len = sizeof($unitRows);

    if($len != 5) {
        $errorMsg = "$len Insufficient Unit Data...";
        $BASE_PATH = "../../..";
        include_once(__DIR__."/../../../error.php");
        echo $errorHTML;
        exit;
    }

    //Calculate each unit marks as avg. of all 3
    $unitMarks = array();
    foreach ($unitRows as $unitMark) {
        $unitMarks[$unitMark->_id] = round((floatval($unitMark->quizMarks) + floatval($unitMark->sAMarks))/2,2);
    }


    $UNITS = $rows;

    $regex = new MongoDB\BSON\Regex("$courseCode([1-5][0-9A-z]+)");
    $query = ['_id' => $regex];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $studentRows = $client->executeQuery("$DB_NAME.$studentDBName",$query);
    $studentRows = $studentRows->toArray();
    $len = sizeof($studentRows);


    if($len != 30*($L+$P+$T)) {
        $errorMsg = "Insufficient Data...";
        $BASE_PATH = "../../..";
        include_once(__DIR__."/../../../error.php");
        echo $errorHTML;
        exit;
    }

    //Data for each session
    $sessionData = array();
    $sessionMarks = array();
    foreach ($studentRows as $stdRow) {
        $sessionData[$stdRow->sloID] = $stdRow->submittedStatus;
        $sessionMarks[$stdRow->sloID] = round((floatval($stdRow->quizMarks) + floatval($stdRow->sAMarks) + floatval($stdRow->lAMarks))/3,2);
    }

    //echo json_encode($sessionMarks);


    //Constants for rendering table
    $ONE_SESSION_THEORY_COUNT = $L + $T;
    $ONE_SESSION_PRACTICAL_COUNT = $P;
    $SESSION_FINAL_COUNT = 3*$ONE_SESSION_PRACTICAL_COUNT + 3*$ONE_SESSION_THEORY_COUNT;

    $S_THEORY_MARKS = array();
    $S_PRACTICAL_MARKS = array();
    $S_THEORY_PERCENTAGE = array();
    $S_PRACTICAL_PERCENTAGE = array();

    $UNIT_COMPLETED = array();

    $UNIT_FINAL_MARKS = array();
    for($unitCount=1;$unitCount<=5;$unitCount++) {
        $isUnitCompleted = true;
        $S_THEORY_MARKS[$unitCount] = $S_PRACTICAL_MARKS = 0;
        for($sessionNos=1;$sessionNos<=2;$sessionNos++) {
            for($sessionCount=1;$sessionCount<=$SESSION_FINAL_COUNT;$sessionCount++) {
                $ID = $courseCode."".($unitCount*100 + $sessionCount)."".$sessionNos;




                $currentMarks = $sessionMarks[$ID];
                if($sessionData[$ID] != 111) $isUnitCompleted = false;

                if($sessionCount%3 >= $ONE_SESSION_THEORY_COUNT) {
                    //Practical
                    if($P != 0) {
                        $S_PRACTICAL_MARKS[$unitCount] = $S_PRACTICAL_MARKS[$unitCount] + $currentMarks;
                    }
                }else{
                    //Theory
                    $S_THEORY_MARKS[$unitCount] = $S_THEORY_MARKS[$unitCount] + $currentMarks;
                }
            }
        }

        $S_THEORY_PERCENTAGE[$unitCount] = ($S_THEORY_MARKS[$unitCount]/(double) (2*3*$ONE_SESSION_THEORY_COUNT));
        $S_THEORY_PERCENTAGE[$unitCount] = (floatval($S_THEORY_PERCENTAGE[$unitCount]) + floatval($unitMarks[$courseCode."".$unitCount]))/2;
        $S_THEORY_PERCENTAGE[$unitCount] = round($S_THEORY_PERCENTAGE[$unitCount],2);
        $UNIT_FINAL_MARKS[$unitCount] = ($S_THEORY_PERCENTAGE[$unitCount]*0.1);


        if($P > 0) {
            $S_PRACTICAL_PERCENTAGE[$unitCount] = ($S_PRACTICAL_MARKS[$unitCount] /(double)2 * 3 * $ONE_SESSION_PRACTICAL_COUNT) * 100;
            $UNIT_FINAL_MARKS[$unitCount] = ($S_THEORY_PERCENTAGE[$unitCount]*0.05) + ($S_PRACTICAL_PERCENTAGE[$unitCount]*0.05);
        }

        $UNIT_COMPLETED[$unitCount] = false;//$isUnitCompleted;
    }


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>CLO &amp; PLAN</title>
    <style>
        .card-title {
            padding: 10px;
        }


        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .title {
            text-transform: uppercase;
            font-size: 1.2em;
        }
        .reduced-size {
            font-size: 14px !important;
        }
        .big {
            font-size: 2em;
        }
        td {
            padding: 5px !important;
        }
        .flex-center {
            display: flex;
            justify-content: space-around;
            padding-top: 5px;
            overflow: visible;
        }
        .click {
            cursor: pointer;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./notice">Notices</a></li>
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div></nav>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large red tooltipped" data-position="left" data-delay="50" data-tooltip="Course Info">
        <i class="large fa fa-info grey darken-3"></i>
    </a>
    <ul>
        <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Course Info" href="#courseInfoModal"><i class="fa fa-info-circle"></i></a></li>
        <li><a class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="CLO 1" href="#clo1Modal">1</a></li>
        <li><a class="btn-floating green tooltipped" data-position="left" data-delay="50" data-tooltip="CLO 2" href="#clo2Modal">2</a></li>
        <li><a class="btn-floating blue tooltipped" data-position="left" data-delay="50" data-tooltip="CLO 3" href="#clo3Modal">3</a></li>
        <li><a class="btn-floating orange tooltipped" data-position="left" data-delay="50" data-tooltip="CLO 4" href="#clo4Modal">4</a></li>
        <li><a class="btn-floating pink tooltipped" data-position="left" data-delay="50" data-tooltip="CLO 5" href="#clo5Modal">5</a></li>
    </ul>
</div>

<div class="container">
    <br><br>
    <div class="card">
        <div class="card-title green darken-3">
            <h5 class="white-text">COURSE LEARNING CONTENT & PLAN</h5>
        </div>
        <div class="card-content">

            <h5><?php echo $courseCode ?> - <?php echo $COURSE_NAME ?></h5>
            <hr><br>
            <ul class="collapsible" data-collapsible="accordion">
                <?php


                    $UNITS = $rows;

                    $INTERNAL_MARKS = array();
                    $SLO_COMPLETED_COUNT = [0,0,0,0,0];


                    for($unitCount=1;$unitCount<=5;$unitCount++) {
                        $unitName = $UNITS[$unitCount-1]->unitName;
                        $percentage = round($UNIT_FINAL_MARKS[$unitCount],2);
                        $colour = "grey";
                        if($percentage < 3.33) {
                            $colour = "red";
                        }else if($percentage < 6.66) {
                            $colour = "orange";
                        }else if($percentage != 10){
                            $colour = "green";
                        }else {
                            $colour = "blue";
                        }
                        array_push($INTERNAL_MARKS,$percentage);
                        $unitTestMark = round($unitMarks[$courseCode."".$unitCount],2);
                        $cloTheoryMarks = round($S_THEORY_MARKS[$unitCount]/200,2);
                        $cloPracticalMarks = round($S_PRACTICAL_MARKS[$unitCount]/200,2);
                        $cloTotalMarks = 3*$ONE_SESSION_THEORY_COUNT;
                        $cloPracticalTotalMarks = 3*$ONE_SESSION_PRACTICAL_COUNT;
                        echo "<li>
                                <div class=\"collapsible-header\">
                                    <b>Unit $unitCount -&nbsp;</b> $unitName  <a class='secondary-content'>
                                    <span class='badge $colour new' data-badge-caption=\"/10\">$percentage</span></a>
                                    <div class='clearfix'> </div>
                                </div>
                                <div class=\"collapsible-body\">
                                    <h5>Marks Obtained</h5><hr>
                                    <div class='row'>
                                        <div class='col s12 m4 center center-align'>
                                         <div class='chip'>Unit Test Score: $unitTestMark/100</div>
                                        </div>
                                        <div class='col s12 m4  center center-align'>
                                        <div class='chip'>Theory Score: $cloTheoryMarks/$cloTotalMarks</div>
                                        </div>
                                        <div class='col s12 m4 center center-align'>
                                        

                                   
                                    ";
                                    if($P>0){
                                        echo "<div class='chip'>Practical: $cloPracticalMarks/$cloPracticalTotalMarks</div></div>";
                                    }
                                    echo "</div><br><h5>Available Sessions</h5><hr>
                                    <table class='bordered centered highlight'>
                                        <thead>
                                            <tr>
                                                <th>Session</th>
                                                <th>SLO - 1</th>
                                                <th>SLO - 2 </th>
                                            </tr>
                                        </thead>
                                        <tbody>";



                        for($sessionCount = 1;$sessionCount<=$SESSION_FINAL_COUNT;$sessionCount++) {
                            $sessionID = 100*$unitCount + $sessionCount;
                            echo "<tr><td data-d='session' class='click blue-text darken-2' data-course='$courseCode' data-id='$sessionID'>Session $sessionCount</td>";
                            $ID = $courseCode."".(100*$unitCount + $sessionCount)."1";
                            if($sessionData[$ID] == 111) {
                                $SLO_COMPLETED_COUNT[$unitCount-1]++;
                                echo "<td class='click' data-d='slo' data-course='$courseCode' data-id='$sessionID' data-index='1' ><i class=\"fa fa-check-circle light-green-text text-darken-3 big\"></i> </td>";
                            }else if($sessionData[$ID] != 0){
                                echo "<td class='click' data-d='slo' data-course='$courseCode' data-id='$sessionID' data-index='1'><i class=\"fa fa-minus-circle yellow-text text-darken-2 big\"></i> </td>";
                            }else{
                                echo "<td class='click' data-d='slo' data-course='$courseCode' data-id='$sessionID' data-index='1'><i class=\"fa fa-times-circle red-text text-lighten-1  big\"></i> </td>";
                            }


                            $ID = $courseCode."".(100*$unitCount + $sessionCount)."2";
                            if($sessionData[$ID] == 111) {
                                $SLO_COMPLETED_COUNT[$unitCount-1]++;
                                echo "<td class='click' data-d='slo' data-course='$courseCode' data-id='$sessionID' data-index='2'><i class=\"fa fa-check-circle light-green-text text-darken-3 big\"></i> </td>";
                            }else if($sessionData[$ID] != 0){
                                echo "<td class='click' data-d='slo' data-course='$courseCode' data-id='$sessionID' data-index='2'><i class=\"fa fa-minus-circle yellow-text text-darken-2 big\"></i> </td>";
                            }else{
                                echo "<td class='click' data-d='slo' data-course='$courseCode' data-id='$sessionID' data-index='3'><i class=\"fa fa-times-circle red-text text-lighten-1 big\"></i> </td>";
                            }
                        }
/*
                        $unitTest = $unitMarks[$courseCode.$unitCount];
                        echo "<tr><td><b>Unit Test</b></td><td>$unitTest</td></tr></tbody>
                                        <tr><td><b>CLO Theory</b></td><td>TODO</td></tr></tbody>";

                        if($P != 0) {
                            echo "<tr><td><b>CLO Practical</b></td><td>TODO</td></tr>
                            <tr><td><b>Learning Resources</b></td><td>LINK</td></tr>";
                        }
*/
                            echo "</tbody>
                                    </table>";


                        $QUERY_STR = "?q=".$courseCode."&id=".$unitCount;
                        echo "<br><a href='./material$QUERY_STR' class='yellow darken-3 btn'>Unit Materials</a>";
                        if($UNIT_COMPLETED[$unitCount]) {
                            echo "<a href='./../test$QUERY_STR' class='right indigo btn'>Unit Test</a>";
                        }


                echo "</li>";
                    }
                ?>
            </ul>

        </div>
    </div>
</div>

<!-- MODALS -->
<div id="courseInfoModal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5><?php echo $courseCode ?> - <?php echo $COURSE_NAME ?></h5>
        <hr><br>
        <ul class="collection">
            <li class="collection-item"><b>Course Category - </b> <?php echo "$COURSE_CATEGORY_CODE ($COURSE_CATEGORY_NAME)"?></li>
            <li class="collection-item"><b>Lecture-</b><?php echo $L ?> &nbsp;&nbsp;<b>Tutorial-</b><?php echo $T ?>  &nbsp;&nbsp;<b>Practical-</b><?php echo $P ?>  &nbsp;&nbsp;<b>Credit-</b><?php echo $C ?> </li>
            <li class="collection-item"><b>Pre-requisite Courses - </b> <?php echo $PRE_REQUISITE?></li>
            <li class="collection-item"><b>Co-requisite Courses - </b> <?php echo $CO_REQUISITE ?></li>
            <li class="collection-item"><b>Progressive Courses - </b> <?php echo $PROGRESSIVE_COURSE ?></li>
            <li class="collection-item"><b>Offering Department - </b> <?php echo $DEPARTMENT ?></li>
            <li class="collection-item"><b>Data Book / Codes / Standards - </b> <?php echo $STANDARD ?></li>
            <li class="collection-item"><b>Course Learning Rationale - </b> The purpose of learning this course is to,
                <?php echo $CLR ?></li>
        </ul>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">ok</a>
    </div>
</div>

<div id="clo1Modal" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>CLO 1</h4>
        <p>At the end of this course learners will be able to <?php echo $CLO[0]->cloName ?> </p>
        <h6><b><u>Program Learning Outcome</u></b></h6>
        <table class="centered">
            <thead>
            <tr>
                <?php
                foreach ($MAPPING_DATA as $DATUM) {
                    $MTEXT = $DATUM[0];
                    $MCODE = $DATUM[1];
                    echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                }

                ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $cloNOS = 0;
                $i = 0;
                $myArray = explode(',', $CLO[$cloNOS]->plo);

                for($i=0;$i<=14;$i++) {
                    if($myArray[$i] == 1) {
                        echo '<td><i class="fa fa-check green-text text-darken-3"></i> </td>';
                    }else {
                        echo '<td><i class="fa fa-times red-text text-darken-3"></i> </td>';
                    }
                }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">ok</a>
    </div>
</div>
<div id="clo2Modal" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>CLO 2</h4>
        <p>At the end of this course learners will be able to <?php echo $CLO[1]->cloName ?>  </p>
        <h6><b><u>Program Learning Outcome</u></b></h6>
        <table class="centered">
            <thead>
            <tr>
                <?php
                foreach ($MAPPING_DATA as $DATUM) {
                    $MTEXT = $DATUM[0];
                    $MCODE = $DATUM[1];
                    echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                }

                ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                    $cloNOS = 1;
                    $i = 0;
                    $myArray = explode(',', $CLO[$cloNOS]->plo);

                    for($i=0;$i<=14;$i++) {
                        if($myArray[$i] == 1) {
                            echo '<td><i class="fa fa-check green-text text-darken-3"></i> </td>';
                        }else {
                            echo '<td><i class="fa fa-times red-text text-darken-3"></i> </td>';
                        }
                    }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">ok</a>
    </div>
</div>

<div id="clo3Modal" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>CLO 3</h4>
        <p>At the end of this course learners will be able to <?php echo $CLO[2]->cloName ?>  </p>
        <h6><b><u>Program Learning Outcome</u></b></h6>
        <table class="centered">
            <thead>
            <tr><?php
                foreach ($MAPPING_DATA as $DATUM) {
                    $MTEXT = $DATUM[0];
                    $MCODE = $DATUM[1];
                    echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                }

                ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $cloNOS = 2;
                $i = 0;
                $myArray = explode(',', $CLO[$cloNOS]->plo);

                for($i=0;$i<=14;$i++) {
                    if($myArray[$i] == 1) {
                        echo '<td><i class="fa fa-check green-text text-darken-3"></i> </td>';
                    }else {
                        echo '<td><i class="fa fa-times red-text text-darken-3"></i> </td>';
                    }
                }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">ok</a>
    </div>
</div>

<div id="clo4Modal" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>CLO 4</h4>
        <p>At the end of this course learners will be able to <?php echo $CLO[3]->cloName ?>  </p>
        <h6><b><u>Program Learning Outcome</u></b></h6>
        <table class="centered">
            <thead>
            <tr><?php
                foreach ($MAPPING_DATA as $DATUM) {
                    $MTEXT = $DATUM[0];
                    $MCODE = $DATUM[1];
                    echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                }

                ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $cloNOS = 3;
                $i = 0;
                $myArray = explode(',', $CLO[$cloNOS]->plo);

                for($i=0;$i<=14;$i++) {
                    if($myArray[$i] == 1) {
                        echo '<td><i class="fa fa-check green-text text-darken-3"></i> </td>';
                    }else {
                        echo '<td><i class="fa fa-times red-text text-darken-3"></i> </td>';
                    }
                }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">ok</a>
    </div>
</div>

<div id="clo5Modal" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>CLO 5</h4>
        <p>At the end of this course learners will be able to <?php echo $CLO[4]->cloName ?> </p>
        <h6><b><u>Program Learning Outcome</u></b></h6>
        <table class="centered">
            <thead>
            <tr><?php
                foreach ($MAPPING_DATA as $DATUM) {
                    $MTEXT = $DATUM[0];
                    $MCODE = $DATUM[1];
                    echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                }

                ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $cloNOS = 4;
                $i = 0;
                $myArray = explode(',', $CLO[$cloNOS]->plo);

                for($i=0;$i<=14;$i++) {
                    if($myArray[$i] == 1) {
                        echo '<td><i class="fa fa-check green-text text-darken-3"></i> </td>';
                    }else {
                        echo '<td><i class="fa fa-times red-text text-darken-3"></i> </td>';
                    }
                }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <a class=" modal-action modal-close waves-effect waves-green btn-flat">ok</a>
    </div>
</div>

<?php
for($i=0;$i<5;$i++) {
    $SLO_COMPLETED_COUNT[$i] = round(($SLO_COMPLETED_COUNT[$i]/(2*$SESSION_FINAL_COUNT))*100,2);
}
$query = ['_id' => $username.$courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();
$facultyID = $rows[0]->facultyID;

$_SESSION['clcp_facultyID'] = $facultyID;

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $username.$courseCode],
    ['$set' => ['internalMarks' => $INTERNAL_MARKS,"sloCompleted" => $SLO_COMPLETED_COUNT]],
    ['multi' => false, 'upsert' => false]
);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".FACULTY_$facultyID", $bulkWrite, $writeConcern);




?>


<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
