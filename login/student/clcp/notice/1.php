<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$STUDENT_ID = $_POST['studentID'];
$MESSAGE = $_POST['message'];

$TYPE = $_POST['type'];
$client = new MongoDB\Driver\Manager($MONGO_URL);


    $query = ['sender' => $SESSION_USERNAME, 'receiver' => $STUDENT_ID, 'course' => $COURSE_CODE];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.NOTICE_TABLE",$query);
    $rows = $rows->toArray();


if($TYPE == "GET") {
    if(sizeof($rows) != 0) {
        die(json_encode(['error' => 400]));
    }
}

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);

if(sizeof($rows) !== 0) {
    $bulkWrite->update(['_id'=> $rows[0]->_id],['sender' => $SESSION_USERNAME, 'receiver' => $STUDENT_ID, 'course' => $COURSE_CODE, 'message' => $MESSAGE]);
}else {
    $bulkWrite->insert(['sender' => $SESSION_USERNAME, 'receiver' => $STUDENT_ID, 'course' => $COURSE_CODE, 'message' => $MESSAGE]);
}

$result = $client->executeBulkWrite("$DB_NAME.NOTICE_TABLE",$bulkWrite);
echo(json_encode(['error' => 200]));

exit;
?>