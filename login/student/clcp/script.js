$(document).ready(function () {
    $('.modal').modal();

    var isDetailsShowing = false;
    var isCloShowing = false;
    $("#courseDetails").hide();
    $("#cloDetails").hide();

    $("#courseHeading").click(function () {
        isDetailsShowing = !isDetailsShowing;
        if(isDetailsShowing) {
            $("#courseDetails").show();
        }else {
            $("#courseDetails").hide();
        }
    });

    $("#cloHeading").click(function () {
       isCloShowing = !isCloShowing;
       if(isCloShowing) {
           $("#cloDetails").show();
       }else {
           $("#cloDetails").hide();
       }
    });

    $('.click').click(function () {
        var type = $(this).attr('data-d');
        if(type == "session") {
            var courseID = $(this).attr('data-course');
            var dataID = $(this).attr('data-id');
            window.location.href = "./../slo/1?q="+courseID+"&id="+dataID;
        }else if(type =="slo") {
            var courseID = $(this).attr('data-course');
            var dataID = $(this).attr('data-id');
            var sloID = $(this).attr('data-index');

            if(sloID == 1) {
                window.location.href = "./../slo/3?q="+courseID+"&id="+dataID;
            }else {
                window.location.href = "./../slo/5?q="+courseID+"&id="+dataID;
            }

        }
    });

});