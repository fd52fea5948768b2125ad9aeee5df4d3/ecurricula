<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    die("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['studentID' => $SESSION_USERNAME];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    echo "<br><div class='center'><div class='chip'>No Courses Requested</div></div> ";
    exit;
}

echo "
                <table class=\"bordered stripped\">
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Course Code</th>
                            <th>Faculty ID</th>
                            <th>Status</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>";
$count = 0;
foreach ($rows as $data) {
    $count++;
    $courseCode = $data->courseID;
    $facultyID = $data->facultyID;
    if($data->status == 0) {
        $status = "Pending";
        $color = "yellow-text text-darken-2";
        $delete = "";
    }else {
        $status = "Accepted";
        $color = "green-text text-darken-1";
        $delete = "disabled";
    }



    echo "<tr><td>$count. </td>
                            <td>$courseCode</td>
                            <td>$facultyID</td>
                            <td class='$color'>$status</td>
                            <td><a data-course='$courseCode' class=\"btn deleteBtn $delete btn-flat red-text right\"><i class=\"fa fa-trash\"></i> &nbsp;Delete</a> </td>
                        </tr>";
}






echo "</tbody></table>";
exit;


?>