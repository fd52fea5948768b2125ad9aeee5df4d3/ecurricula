<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$finalResult['error'] = 0;

$courseCode = $_POST['courseCode'];
$facultyID = $_POST['facultyId'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) != 1) {
    displayError("Invalid Course");
}

$coordinatorTableName = $courseCode."_COORDINATOR_TABLE";
$query = ['_id' => $facultyID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$coordinatorTableName",$query);
$rows = $rows->toArray();

if(sizeof($rows) != 1) {
    $str = json_encode($rows);
    displayError("Invalid Faculty Name, or Faculty not available for selected course.");
}


$query = ['_id' => $SESSION_USERNAME.$courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 1) {
    displayError("Course Already Requested");
}


$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert(["_id" => $SESSION_USERNAME.$courseCode,'studentID' => $SESSION_USERNAME,
    'facultyID' => $facultyID,
    'courseID' => $courseCode,
    'stdName' => $_SESSION['userFullName'],
    'status' => 0
]);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".COURSE_REQUEST_TABLE", $bulkWrite, $writeConcern);

echo json_encode($finalResult);
exit;


?>