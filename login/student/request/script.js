$(document).ready(function () {

    $('#requestCourseButton').click(function () {
       var courseCode = $('#courseIdInputField').val();

       var facultyId = $('#facultyIDInputField').val();
        courseCode = courseCode.substr(0,courseCode.indexOf("-")-1);

        facultyId = facultyId.substr(0,facultyId.indexOf("-")-1);
        if(!courseCode) {
           window.alert("Enter a Valid Course Code");
           return;
       }

       if(!facultyId) {
           window.alert("Enter a Valid Faculty ID");
           return;
       }

       var data = {
           courseCode: courseCode,
           facultyId: facultyId
       };

       $.ajax({
           'method': 'POST',
           'data': data,
           'url': './1.php',
           'dataType': 'JSON',
           'error': function () {
               window.alert("Something Wrong!");
           },
           'success': function (phpdata) {
               if(phpdata['error'] == 1) {
                   window.alert("Error!!!");
               }if(phpdata['error'] == 2) {
                   window.alert("Error: " + phpdata['errorMsg']);
               }else {
                   window.alert("Request Successful");
               }
               setData();


           }
       });


    });



    function setData() {

        $.ajax({
            'method': 'POST',
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                $('#requestedCourseDiv').html(phpdata);

                $('.deleteBtn').click(function () {
                    deleteRequest($(this).attr('data-course'))
                });
            }
        });
    }

    setData();
    
    function deleteRequest(courseID) {
        $.ajax({
            'method': 'POST',
            'url': './3.php',
            'data': {
                courseCode: courseID
            },
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Delete Request Successful");
                }
                setData();
            }
        });
    }

    function startCourseSuggestion() {
        $.ajax({
            'method': 'POST',
            'url': './4.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                var data = {};
                for(var i=0;i<phpdata.length;i++) {
                    var subject = phpdata[i];
                    data[subject.code + " - " + subject.name] = null;
                }
                $('#courseIdInputField').autocomplete({
                    data: data,
                    limit: 10, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val) {
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
            }
        });

        $.ajax({
            'method': 'POST',
            'url': './5.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                var data = {};
                for(var i=0;i<phpdata.length;i++) {
                    var subject = phpdata[i];
                    data[subject.code + " - " + subject.name] = null;
                }
                $('#facultyIDInputField').autocomplete({
                    data: data,
                    limit: 10, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val) {
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
            }
        });


    }

    startCourseSuggestion();



});