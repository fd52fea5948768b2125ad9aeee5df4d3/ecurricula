<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
    exit;
}


$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = [];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();

$result = array();
foreach ($rows as $row) {
    array_push($result,['code' => $row->courseCode,'name' => $row->courseName]);
}

echo json_encode($result);
exit;


?>