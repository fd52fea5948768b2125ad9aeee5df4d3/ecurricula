<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../error.php");
    echo $errorHTML;
    exit;
}

session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];
$SESSION_NAME = $_SESSION['userFullName'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['studentID' => $SESSION_USERNAME, 'status' => 1];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/materialize.min.css">
    <title><?php echo $SESSION_NAME; ?> Home</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 25px 0px 15px 0px;
        }

    </style>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="./request">Request Course</a></li>
    <li><a href="./profile/">Profile</a></li>
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a class="dropdown-button" data-activates="dropdown1">Options&nbsp;&nbsp; <i class="fa big fa-arrow-circle-down"></i></a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Student Login - <?php echo $SESSION_USERNAME;  ?></span>
            <br><h5><i class="fa fa-graduation-cap"></i> Registered Courses</h5><hr>

            <div class="row">
                <?php
                    if(sizeof($rows) == 0) {
                        echo "<br><div class='center'><div class='chip'>No Courses Registered</div></div> ";
                    }
                    foreach ($rows as $data) {
                        $courseCode = $data->courseID;
                        $courseName = $data->courseName;

                        echo "<div class=\"col s12 m6 l4\">
                                <div class=\"card\">
                                    <div class=\"card-content\">
                                        <span class=\"card-title white-text indigo sub-heading\">$courseCode</span>
                                        <p class=\"plus-pad\">$courseName</p>
                                    </div>
                                    <div class=\"card-action\">
                                        <a href='#' class='right goToCourse blue-grey-text text-accent-1' data-course='$courseCode'>Go to course&nbsp;&nbsp; <i class='fa fa-play'></i> </a>
                                        <div class='clearfix'> </div>
                                    </div>
                                    
                                </div>
                            </div>";
                    }

                ?>

            </div>

        </div>
    </div>
</div>




<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
