<?php
    session_start();
    include_once(__DIR__."/../../../../includes/general.config.php");
    include_once(__DIR__."/../../../../includes/mongo.db.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/images/list/quiz";

    function displayError($str) {
        $errorHTML = "";
        $errorMsg = "$str";
        $BASE_PATH = "../../../..";
        include_once(__DIR__."/../../../../error.php");
        echo $errorHTML;
        exit;
    }

    $courseCode = $_GET['q'];
    $unitID = $_GET['id'];

    $_SESSION['test_unit_id'] = $courseCode.$unitID;

    $QUERY_STRING = "?q=".$courseCode."&id=".$unitID;
    if(!$courseCode || strlen($courseCode) > 10) {
        displayError("Invalid Subject");
    }

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['unitID' => ($courseCode."".$unitID)];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.UNIT_QUESTIONS_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) {
        $res = ["error" => '404',"errorMsg" => "Insufficient Question"];
        echo json_encode($res);
        exit;
    }

    $query = ['_id' => array("\$in" => $rows[0]->qAIDs)];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);
    $rows = $rows->toArray();

    $res = array();
    $count = 1;
    foreach ($rows as $data) {
        $n = $count-1;
        $res[$count] = ['id' => $rows[$n]->_id,'name' => $rows[$n]->name,'options'=>$rows[$n]->options, "sessionName" => $rows[$n]->sessionID];
        $count++;
    }


    function printQuizBlock($res,$i,$courseCode,$url,$NODE_IP_ARR) {
            $NAME = $res['name'];
            $OPTION1 = $res['options'][0];
            $OPTION2 = $res['options'][1];
            $OPTION3 = $res['options'][2];
            $OPTION4 = $res['options'][3];
            $ID = $res['id'];

        /*** GET THE IMAGES ***/
        $data = array(
            'sessionID' => $res['sessionName'],
            'courseID' => $courseCode,
            'qID' => $ID
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));


        echo "<div class=\"quiz-section\">
                    <a><b>$i. $NAME</b> </a><form>";
    foreach ($result as $res) {
        $hrefURL = $NODE_IP_ARR."/public?q=".$res->path;
        echo "<image class='responsive-img' src='$hrefURL' ></image><br>";
    }
                    echo "
                    <p>
                        <input data='$ID' class=\"with-gap\" checked='checked' name=\"group$i\" value='1' type=\"radio\" id=\"test1$i\" />
                        <label for=\"test1$i\">$OPTION1</label>
                    </p>
                    <p>
                        <input data='$ID' class=\"with-gap\" name=\"group$i\" value='2' type=\"radio\" id=\"test2$i\" />
                        <label for=\"test2$i\">$OPTION2</label>
                    </p>
                    <p>
                        <input data='$ID' class=\"with-gap\" name=\"group$i\" value='3' type=\"radio\" id=\"test3$i\"  />
                        <label for=\"test3$i\">$OPTION3</label>
                    </p>
                    <p>
                        <input data='$ID' class=\"with-gap\" name=\"group$i\" value='4' type=\"radio\" id=\"test4$i\"/>
                        <label for=\"test4$i\">$OPTION4</label>
                    </p></form>
                </div>
                <br>
                <br>";
    }


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Quiz</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .quiz-section {
            margin-left: 15px;
        }

        .quiz-section > p {
            margin-left: 15px !important;
        }

        .hint {
            font-family: monospace;
            font-size: 20px;

        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
    </div>
</nav>



<div class="container">
        <div class="hide-on-small-only">
            <br><br><br>
        </div>
        <div class="card">
            <div class="card-content">
                <h5><i class="fa fa-list-alt"></i>&nbsp;&nbsp; QUIZ - <?php echo $res[1]['sessionName']; ?> </h5>
                <hr>
                <br>

                <div class="center" id="resultSection" hidden="hidden">
                    <h5 class="indigo-text text-darken-2">YOUR SCORE: <span id="reultSlot"></span>/6</h5>
                    <a id="closeBtn" class="waves-effect red right white-text waves-teal btn-flat">CLose &nbsp;&nbsp;&nbsp;<i class="fa fa-times-circle"></i> </a></div>
                <div class="clearfix"></div>

                <div id="quizContainer">

                    <h5 class="hint green-text">Level 1 Questions</h5><hr>
                    <?php printQuizBlock($res[1],1,$courseCode,$url,$NODE_IP_ARR);
                    printQuizBlock($res[2],2,$courseCode,$url,$NODE_IP_ARR)?>
                    <h5 class="hint yellow-text text-darken-2">Level 2 Questions</h5><hr>
                    <?php printQuizBlock($res[3],3,$courseCode,$url,$NODE_IP_ARR);
                    printQuizBlock($res[4],4,$courseCode,$url,$NODE_IP_ARR)?>
                    <h5 class="hint red-text">Level 3 Questions</h5><hr>
                    <?php printQuizBlock($res[5],5,$courseCode,$url,$NODE_IP_ARR);
                    printQuizBlock($res[6],6,$courseCode,$url,$NODE_IP_ARR)?>
                    <div class="row">
                        <a id="submitBtn" class="waves-effect right green white-text waves-teal btn-flat">Submit &nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-circle-right"></i> </a>
                        <div class="clearfix"></div>

                </div>
                </div>
            </div>
        </div>

</div>

<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
