$(document).ready(function () {
    var quizContiner = $('#quizContainer');
    var resultDiv = $("#resultSection");
    resultDiv.hide();

    $("#submitBtn").click(function () {
        var option1 = $("input:radio[name='group1']:checked").val();
        var option2 = $("input:radio[name='group2']:checked").val();
        var option3 = $("input:radio[name='group3']:checked").val();
        var option4 = $("input:radio[name='group4']:checked").val();
        var option5 = $("input:radio[name='group5']:checked").val();
        var option6 = $("input:radio[name='group6']:checked").val();


        var id1 = $("input:radio[name='group1']:checked").attr('data');
        var id2 = $("input:radio[name='group2']:checked").attr('data');
        var id3 = $("input:radio[name='group3']:checked").attr('data');
        var id4 = $("input:radio[name='group4']:checked").attr('data');
        var id5 = $("input:radio[name='group5']:checked").attr('data');
        var id6 = $("input:radio[name='group6']:checked").attr('data');

        $.ajax({
            url: './quiz.module.php',
            method: "POST",
            data: {
                options: [option1,option2,option3,option4,option5,option6],
                ids: [id1,id2,id3,id4,id5,id6]
            },
            dataType: "json",
            beforeSend: function () {
                quizContiner.html(preloaderHTML);
            },
            success: function (res) {
                if(res['error'] != "200") {
                    resultDiv.show();
                    $("#reultSlot").text(res['score']);
                    quizContiner.html("");
                }else {
                    window.alert("Something went wrong. Contact Admin!!!")
                }
            }
        });
    });

    $('#closeBtn').click(function () {
        window.close();
    });

    var preloaderHTML = '<div class="center"><div class="preloader-wrapper big active">'+
        '      <div class="spinner-layer spinner-blue">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        ''+
        '      <div class="spinner-layer spinner-red">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        ''+
        '      <div class="spinner-layer spinner-yellow">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        ''+
        '      <div class="spinner-layer spinner-green">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        '    </div></div>';


});