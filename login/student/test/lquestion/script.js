$(document).ready(function() {

    var questionContainer = $("#quizContainer");
    var resultDiv = $("#resultSection");

    $("#submitBtn").click(function() {
        var a1 = $("#textarea"+1).val();
        var a2 = $("#textarea"+2).val();
        var a3 = $("#textarea"+3).val();

        var id1 = $("#textarea"+1).attr('data-qid');
        var id2 = $("#textarea"+2).attr('data-qid');
        var id3 = $("#textarea"+3).attr('data-qid');

        var data = {
            'answer': [a1,a2,a3],
            'ids': [id1,id2,id3]
        };

        $.ajax({
            url: './question.module.php',
            method: "POST",
            data: data,
            dataType: "json",
            beforeSend: function () {
                questionContainer.html(preloaderHTML);
            },
            success: function (res) {
                if(res['error'] != "200") {
                    resultDiv.show();
                    $("#reultSlot").text(res['score']);
                    questionContainer.html("");
                }else {
                    window.alert("Something went wrong. Contact Admin!!!")
                }
            }
        });


    });

    var preloaderHTML = '<div class="center"><div class="preloader-wrapper big active">'+
        '      <div class="spinner-layer spinner-blue">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        ''+
        '      <div class="spinner-layer spinner-red">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        ''+
        '      <div class="spinner-layer spinner-yellow">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        ''+
        '      <div class="spinner-layer spinner-green">'+
        '        <div class="circle-clipper left">'+
        '          <div class="circle"></div>'+
        '        </div><div class="gap-patch">'+
        '          <div class="circle"></div>'+
        '        </div><div class="circle-clipper right">'+
        '          <div class="circle"></div>'+
        '        </div>'+
        '      </div>'+
        '    </div></div>';

    $('#closeBtn').click(function () {
        window.close();
    });
});