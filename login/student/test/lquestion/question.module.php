<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
$client = new MongoDB\Driver\Manager($MONGO_URL);

$username = $_SESSION['username'];
$STD_TABLE_NAME = "STD_DB_".$username;
$UNIT_ID = $_SESSION['test_unit_id'];

$query = ['_id' => $UNIT_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.$STD_TABLE_NAME",$query);
$rows = $rows->toArray();

$presentStatus = $rows[0]->submittedStatus;
$OPTIONS = $_POST['answer'];
$IDS= $_POST['ids'];
$query = ['_id' => array("\$in" => $IDS)];

$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
$rows = $rows->toArray();

$correctAns = array();
foreach ($rows as $r) {
    $correctAns[$r->_id] = $r->answer;
}

$score = 0;
for($i=0;$i<5;$i++) {
    //TODO: Compare $OPTIONS[$i], and $correctAns[$IDS[$i]]
    //TODO: Generate score out of 100
}
//Remove this
$score = 80;

$res = ["score" => $score, "answers" => $correctAns, "id" => $UNIT_ID];

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $UNIT_ID],
    ['$set' => ['submittedStatus' => ((intval($presentStatus/10)*10) + 1),'sAMarks' => $score]],
    ['multi' => false, 'upsert' => false]
);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".$STD_TABLE_NAME", $bulkWrite, $writeConcern);

echo json_encode($res);
exit;
?>