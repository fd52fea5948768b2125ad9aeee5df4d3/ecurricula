<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

$client = new MongoDB\Driver\Manager($MONGO_URL);
$username = $_SESSION['username'];
$STD_TABLE_NAME = "STD_DB_".$username;

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

$courseCode = $_GET['q'];
$sessionID = $_GET['id'];

/*** Check for Faculty Enabled ***/
$query = ['_id' => $username.$courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();
$FACULTY_ID = $rows[0]->facultyID;

$query = ['_id' => $username.$courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.FACULTY_$FACULTY_ID",$query);
$rows = $rows->toArray();

$isEnabled = ($rows[0]->unitStatus[intval($sessionID)-1]);

if($isEnabled != 1) {
    include_once("./error.php");
    exit;
}
/***   End of Faculty Check    ***/

/*** Check for Marks Entered **/

$query = ['_id' => $courseCode.$sessionID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$STD_TABLE_NAME",$query);
$rows = $rows->toArray();

if(intval($rows[0]->submittedStatus) == 111) {
    $isCompleted = true;
}else {
    $isCompleted = false;
}

/*** End of Check for Marks Entered ***/

/*** Add the Question to DB **/
if(!$isCompleted) {
    $query = ['_id' => $username . $courseCode . $sessionID];
    $option = [];
    $query = new MongoDB\Driver\Query($query, $option);
    $rows = $client->executeQuery("$DB_NAME.UNIT_QUESTIONS_TABLE", $query);
    $rows = $rows->toArray();

    if (sizeof($rows) == 0) {
        $unitID = $sessionID;
        $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);

        $qAID = array();
        $count = 0;
        for($l = 1;$l<4;$l++) {
            $query = ['unitID' => ($courseCode . "" . $unitID), 'level' => "".$l];
            $query = new MongoDB\Driver\Query($query, $option);
            $rows = $client->executeQuery("$DB_NAME.QUIZ_TABLE", $query);
            $rows = $rows->toArray();

            if (sizeof($rows) < 2) {
                $res = ["error" => '404', "errorMsg" => "Insufficient Quiz Question"];
                echo json_encode($res);
                exit;
            }

            $randomNumbers = UniqueRandomNumbersWithinRange(1, sizeof($rows), 2);

            foreach ($randomNumbers as $n2) {
                $n = $n2 - 1;
                $qAID[$count] = $rows[$n]->_id;
                $count++;
            }
        }


        $sAID = array();
        $count = 0;

        for($l=1;$l<4;$l++) {
            $query = ['unitID' => ($courseCode . "" . $unitID), 'level' => "".$l];
            $option = [];

            $query = new MongoDB\Driver\Query($query, $option);
            $rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE", $query);
            $rows = $rows->toArray();

            if (sizeof($rows) < 1) {
                $res = ["error" => '404', "errorMsg" => "Insufficient Short Question"];
                echo json_encode($res);
                exit;
            }

            $randomNumbers = UniqueRandomNumbersWithinRange(1, sizeof($rows), 1);
            foreach ($randomNumbers as $n2) {
                $n = $n2 - 1;
                $sAID[$count] = $rows[$n]->_id;
                $count++;
            }
        }

        $lAID = array();
        $count = 0;

        for($l=1;$l<4;$l++) {
            $query = ['unitID' => ($courseCode . "" . $unitID), 'level' => "".$l];
            $option = [];

            $query = new MongoDB\Driver\Query($query, $option);
            $rows = $client->executeQuery("$DB_NAME.LONG_QUESTION_TABLE", $query);
            $rows = $rows->toArray();

            if (sizeof($rows) < 1) {
                $res = ["error" => '404', "errorMsg" => "Insufficient Long Question"];
                echo json_encode($res);
                exit;
            }

            $randomNumbers = UniqueRandomNumbersWithinRange(1, sizeof($rows), 1);

            foreach ($randomNumbers as $n2) {
                $n = $n2 - 1;
                $lAID[$count] = $rows[$n]->_id;
                $count++;
            }
        }




        $bulkWrite->insert([
            "_id" => $username . $courseCode . $sessionID,
            'courseID' => $courseCode,
            'stdID' => $username,
            'unitID' => $courseCode . $sessionID,
            'facultyID' => $FACULTY_ID,
            'qAIDs' => [$qAID[0], $qAID[1], $qAID[2], $qAID[3], $qAID[4],$qAID[5]],
            'lAIDs' => [$lAID[0], $lAID[1],$lAID[2]],
            'sAIDs' => [$sAID[0], $sAID[1], $sAID[2]]
        ]);


        $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
        $writeResult = $client->executeBulkWrite($DB_NAME . ".UNIT_QUESTIONS_TABLE", $bulkWrite, $writeConcern);

    }
    /*** End of adding the question to DB ***/
}

$query = ['_id' => $courseCode . $sessionID];
$option = [];

$query = new MongoDB\Driver\Query($query, $option);

$rows = $client->executeQuery("$DB_NAME.UNITS_TABLE", $query);
$rows = $rows->toArray();
$UNIT_NAME = $rows[0]->unitName;

$sloID = $courseCode . "" . $sessionID;

$query = ['_id' => $sloID];
$option = [];

$query = new MongoDB\Driver\Query($query, $option);

$rows = $client->executeQuery("$DB_NAME.$STD_TABLE_NAME", $query);
$rows = $rows->toArray();

$MARKS = round((floatval($rows[0]->quizMarks) + floatval($rows[0]->sAMarks) + floatval($rows[0]->lAMarks)) / 3, 2);

$QUERY_STRING = "?q=" . $courseCode . "&id=" . $sessionID;

$QUIZ_MARKS = $rows[0]->quizMarks;
$SA_MARKS = $rows[0]->sAMarks;
$LA_MARKS = $rows[0]->lAMarks;
$STATUS = $rows[0]->submittedStatus;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Unit Test Home</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .big {
            font-size: 25px;
        }

        .mono {
            font-family: monospace;
        }

        .mid {
            font-size: 20px;
        }

        .collection .collection-item {
            line-height: 2.4em !important;
        }

        .small-text {
            padding-left: 15px;
            font-size: 10px;
            text-transform: uppercase;
        }

        .medium-text {
            font-size: 12px;
            font-weight: 600;
            font-family: monospace;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>


<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h4>Unit Test - <?php echo $UNIT_NAME ?></h4>
            <hr><br>
                <?php

                if($isCompleted) {
                    echo "<h5 class='blue-text text-darken-2 center'>TOTAL MARKS: ".$MARKS."%</h5><br>";
                    echo "
            <ul class=\"collection\">";
                    echo "<li class=\"collection-item\"><span class=\"mid\">Quiz</span><span class=\"small-text blue-grey-text medium-text text-darken-2\">(MARKS SCORED: $QUIZ_MARKS%)</span><br><span> </span> </li>";
                    echo "<li class=\"collection-item\"><span class=\"mid\">Short Answer</span><span class=\"small-text blue-grey-text medium-text text-darken-2\">(MARKS SCORED:  $SA_MARKS%)</span><br><span> </span> </li>";
                    echo "<li class=\"collection-item\"><span class=\"mid\">Long Question <a class='blue-grey-text medium-text text-darken-2'>(MARKS SCORED: $LA_MARKS%)</a> </span> <br><span> </span> </li>";
                    echo "</ul>";
                }else {
                    echo "
            <ul class=\"collection\">";
                    if (intval($STATUS / 10) == 1) {
                        echo "<li class=\"collection-item\"><span class=\"mid\">Quiz</span><span class=\"small-text blue-grey-text medium-text text-darken-2\">(MARKS SCORED: $QUIZ_MARKS%)</span><a target='_blank' disabled='disabled' class=\"secondary-content blue btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                    } else {
                        echo "<li class=\"collection-item\"><span class=\"mid\">Quiz</span><a target='_blank' href='./quiz$QUERY_STRING' class=\"secondary-content blue btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                    }

                    if ($STATUS % 10 == 1) {
                        echo "<li class=\"collection-item\"><span class=\"mid\">Short Answer</span><span class=\"small-text blue-grey-text medium-text text-darken-2\">(MARKS SCORED:  $SA_MARKS%)</span><a target='_blank' disabled='disabled' class=\"secondary-content orange btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                    } else {
                        echo "<li class=\"collection-item\"><span class=\"mid\">Short Answer</span><a target='_blank' href='./lquestion$QUERY_STRING' class=\"secondary-content orange btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                    }


                    echo "<li class=\"collection-item\"><span class=\"mid\">Long Question <a class='blue-grey-text medium-text text-darken-2'>(MARKS SCORED: $LA_MARKS)*</a> </span> <a target='_blank' href='./question$QUERY_STRING' class=\"secondary-content indigo btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";

                    echo "
            </ul>
            <a class=\" grey-text mono right text-darken-2\">Total Score: <?php echo $MARKS.\"/100\"?></a><br>
            <a class=\" grey-text mono right text-accent-4\">* Long Question Marks is Uploaded by Respective Faculty</a>
            <div class=\"clearfix\"> </div>";
                }
                ?><br>

                <a target="_blank" href="<?php echo "./paper$QUERY_STRING"; ?>" id="getPaperBtn" class="btn waves-effect red text-accent-2">Download Question Paper</a>






        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
