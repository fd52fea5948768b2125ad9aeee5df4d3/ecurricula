<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
include_once(__DIR__."/../../../../includes/node.mongo.config.php");

function getImages($sessionId,$courseCode,$questionId,$type,$NODE_IP_ARR,$NODE_URL) {
    $imageSTR = "";
    $url = $NODE_URL."/images/list/$type";

    $data = array(
        'sessionID' => $sessionId,
        'courseID' => $courseCode,
        'qID' => $questionId
    );


    $ch = curl_init();
    curl_setopt($ch,CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = json_decode(curl_exec($ch));

    foreach ($result as $res) {
        $hrefURL = $NODE_IP_ARR."/public?q=".$res->path;
        $imageSTR = $imageSTR."<image class='responsive-img' src='$hrefURL' ></image><br>";
    }

    return $imageSTR;
}
?>