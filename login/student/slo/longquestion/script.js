$(document).ready(function () {
    $('#closeBtn').click(function () {
        window.close();
    });

    function initKatex() {
        $('.equationField').each(function() {
            console.log("function Called");
            //console.log($(this)[0].id);
            katex.render($(this).html(),document.getElementById($(this)[0].id));
        });
    }

    initKatex();
});