<?php
    session_start();
    include_once(__DIR__."/../../../includes/general.config.php");
    include_once(__DIR__."/../../../includes/mongo.db.config.php");
    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $username = $_SESSION['username'];

    $OPTIONS = $_POST['options'];
    $IDS= $_POST['ids'];

    $STD_TABLE_NAME = "STD_DB_".$username;
    $SLO_ID = $_SESSION['quiz_slo'];

    $query = ['_id' => $SLO_ID];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.$STD_TABLE_NAME",$query);
    $rows = $rows->toArray();
    $presentStatus = $rows[0]->submittedStatus;


    $query = ['_id' => array("\$in" => $IDS)];

    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);

    $rows = $rows->toArray();

    $correctAns = array();
    foreach ($rows as $r) {
        $correctAns[$r->_id] = $r->correctOption;
    }

    $score = 0;
    for($i=0;$i<6;$i++) {
        if($OPTIONS[$i] == $correctAns[$IDS[$i]]) {
            $score++;
        }
    }

    $res = ["score" => $score, "answers" => $correctAns, "id" => $SLO_ID];

    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->update(
        ['_id' => $SLO_ID],
        ['$set' => ['submittedStatus' => (($presentStatus%100) + 100),'quizMarks' => round((($score/6)*100),2)]],
        ['multi' => false, 'upsert' => false]
    );


    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".$STD_TABLE_NAME", $bulkWrite, $writeConcern);


echo json_encode($res);
    exit;
?>