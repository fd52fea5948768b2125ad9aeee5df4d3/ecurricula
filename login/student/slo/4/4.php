<?php
require_once (__DIR__."./../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
include_once(__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/student/list/practice";

session_start();
if(!isset($_SESSION['course_code_id'])) {
    echo "Session not found";
    exit;
}


$COURSE_CODE = $_SESSION['course_code_practice'];
$SLO_ID = $_SESSION['course_code_id'];
$SESSION_USERNAME = $_SESSION['username'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $SESSION_USERNAME.$COURSE_CODE.$SLO_ID];
$SESSION_TABLE_NAME = "PRACTICE_REQUEST_TABLE";
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.$SESSION_TABLE_NAME",$query);
$rows = $rows->toArray();

//echo json_encode($rows);

if (sizeof($rows) == 0) {
    //Not Submitted a request
    $html = "";
    $finalResult = ['error'=> 200, 'html' => $html, 'render' => 0];
    echo json_encode($finalResult);
    exit;
}else {
    //Submitted a request
    $timestamp = $rows[0]->timestamp;
    $hrefURL = $NODE_IP_ARR."/public?q=".$rows[0]->url;

    $submittedStatus = $rows[0]->status;
    if($submittedStatus == -1) {
        $color = "red-text";
        $msg = "Rejected";
    }else if($submittedStatus == 0) {
        $color = "yellow-text text-darken-2";
        $msg = "Pending";
    }else {
        $color = "green-text text-darken-1";
        $msg = "Accepted";
    }

    $html = "<div class=\"wrapper-div\">
                <div class=\"row short\">
                    <br><br><p class=\"heading\">
                        Submission Details </span> </p><hr>
                </div>
            
                <div class='card report-card'>
                    <div class='card-content'>
                    Submitted On:  <span class='plain-text'>$timestamp </span>
                       
            </div><div class='card-action'>
                       <a class='$color'>$msg</a>
                       <a target='_blank' class='indigo-text' href='$hrefURL'><i class='fa fa-download'> </i> Download</a>
            </div>
            </div>
            </div>";


    $finalResult = ['error'=> 200, 'html' => $html, 'render' => 1];
    echo json_encode($finalResult);
    exit;
}

?>