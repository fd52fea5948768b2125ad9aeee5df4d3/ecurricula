<?php
require_once (__DIR__."./../../../../includes/general.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
//TODO Contact Node Server, and Upload File

$url = $NODE_URL."/student/upload/";


session_start();
if(!isset($_SESSION['course_code_id'])) {
    echo "Session not found";
    exit;
}


$COURSE_CODE = $_SESSION['course_code_practice'];
$SLO_ID = $_SESSION['course_code_id'];
$SESSION_USERNAME = $_SESSION['username'];


$tmpfile = $_FILES['file']['tmp_name'];

$path_parts = pathinfo($_FILES["file"]["name"]);
$extension = $path_parts['extension'];

$filename = $COURSE_CODE." - ".$SESSION_USERNAME.".".$extension;

$data = array(
    'sessionID' => $COURSE_CODE.$SLO_ID,
    'courseID' => $COURSE_CODE,
    'studentID' => $SESSION_USERNAME
);
 
$data['file'] = new CurlFile($_FILES['file']['tmp_name'],'file/exgd',$filename);



$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = json_decode(curl_exec($ch));


//close connection
curl_close($ch);

date_default_timezone_set('Asia/Kolkata');


if($result) {
    $url2 = $result->url;

    $client = new MongoDB\Driver\Manager($MONGO_URL);

    $query = ['_id' => $SESSION_USERNAME.$COURSE_CODE];
    $SESSION_TABLE_NAME = "COURSE_REQUEST_TABLE";
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);

    $rows = $client->executeQuery("$DB_NAME.$SESSION_TABLE_NAME",$query);
    $rows = $rows->toArray();


    $facultyID = $rows[0]->facultyID;
    $dbChange = ["_id" => $SESSION_USERNAME.$COURSE_CODE.$SLO_ID,'sessionID' => $COURSE_CODE.$SLO_ID,
        'courseID' => $COURSE_CODE,
        'studentID' => $SESSION_USERNAME,
        'status' => 0,
        'facultyID' => $facultyID,
        'timestamp' => date("Y-m-d H:i:s"),
        'url' => $url2
    ];

    $bulkWrite = new MongoDB\Driver\BulkWrite;

    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->update(["_id" => $SESSION_USERNAME.$COURSE_CODE.$SLO_ID],
        ['$set' => $dbChange],
        ['multi' => false, 'upsert' => true]
    );

    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".PRACTICE_REQUEST_TABLE", $bulkWrite, $writeConcern);

}
?>