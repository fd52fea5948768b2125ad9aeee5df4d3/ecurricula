<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/images/list/sa";


function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}


session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
}


$courseCode = $_GET['q'];
$sessionID = $_GET['id'];
$sloID = $_GET['i'];

$_SESSION['question_slo'] = $courseCode.$sessionID.$sloID;
$QUERY_STRING = "?q=".$courseCode."&id=".$sessionID;
if(!$courseCode || strlen($courseCode) > 10) {
    displayError("Invalid Subject");
}

$client = new MongoDB\Driver\Manager($MONGO_URL);

function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

$res = array();

$count = 1;
for($l=1;$l<4;$l++) {
    $query = ['sessionID' => ($courseCode."".$sessionID.$sloID), 'level' => "".$l];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) < 1) {
        $size = sizeof($rows);
        $res = ["error" => '404',"errorMsg" => "Insufficient Questions for Level:  $l ($size)"];
        echo json_encode($res);
        exit;
    }

    $randomNumbers = UniqueRandomNumbersWithinRange(1,sizeof($rows),1);
    foreach ($randomNumbers as $n2) {
        $n = $n2-1;
        $res[$count] = ['id' => $rows[$n]->_id,'question' => $rows[$n]->name, "sessionName" => $rows[$n]->sessionID, "answer" => $rows[$n]->answer, 'aEquation' => $rows[$n]->aEquation, 'qEquation' => $rows[$n]->qEquation];
        $count++;
    }
}





?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/katex.min.css">
    <title>Short Q/A</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .answer {
            margin-left: 5% !important;
        }

        .quiz-section > p {
            margin-left: 15px !important;
        }



    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"> </span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>

    </div>
</nav>



<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-pencil"></i>&nbsp;&nbsp; Short Q/A </h5>
            <hr>
            <br>

            <div class="center" id="resultSection" hidden="hidden">
                <h5 hidden="hidden" class="indigo-text text-darken-2">YOUR SCORE: <span id="reultSlot"></span>/100</h5>
                <a id="closeBtn" class="waves-effect red right white-text waves-teal btn-flat">CLose &nbsp;&nbsp;&nbsp;<i class="fa fa-times-circle"></i> </a></div>
            <div class="clearfix"></div>

            <div id="quizContainer">
                <?php
                for($i=1;$i<=3;$i++) {
                    $QUESTION = nl2br($res[$i]['question']);
                    $ID = $res[$i]['id'];
                    $ANSWER = nl2br($res[$i]['answer']);

                    $data = array(
                        'sessionID' => $courseCode."".$sessionID.$sloID,
                        'courseID' => $courseCode,
                        'qID' => $ID
                    );

                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_VERBOSE, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    $result = json_decode(curl_exec($ch));


                    echo "<div class=\"qna-section\">
                    <a><b>Level $i Question: $QUESTION</b> </a><br>";

                    $EQUATION_TEXT_A = $res[$i]['aEquation'];
                    $EQUATION_TEXT_Q = $res[$i]['qEquation'];

                    $qEquations = explode('\\\\',$EQUATION_TEXT_Q);
                    $qcount= 0;
                    $qHtml = "";
                    foreach ($qEquations as $equation) {
                        if($equation == '') continue;
                        $qHtml = $qHtml."<div class='equationField' id='Q$ID$qcount'>$equation</div>";
                        $qcount++;
                    }

                    $aEquation = explode('\\\\',$EQUATION_TEXT_Q);
                    $acount = 0;
                    $aHtml = "";
                    foreach ($aEquation as $equation) {
                        if($equation == '') continue;
                        $aHtml = $aHtml."<div class='equationField' id='A$ID$acount'>$equation</div>";
                        $acount++;
                    }

                    echo "<div>$qHtml</div>";
                    foreach ($result as $res2) {
                        if($res2->type == 'answer') continue;
                        $hrefURL = $NODE_IP_ARR."/public?q=".$res2->path;
                        echo "<br><image class='responsive-img' src='$hrefURL' ></image><br>";
                    }

                   // echo json_encode($data);

                    echo "<div class=\"row answer\">
    <form class=\"col s12\">
      <div class=\"row\">
        <div class=\"input-field col s12\">
          <textarea data-qid='$ID' id=\"textarea$i\" class=\"materialize-textarea answerText\"></textarea>
          <label for=\"textarea$i\">Answer $i</label>
        </div>
        <p id=\"textbox$ID\" class='answerDiv'>Answer:  $ANSWER <br><div>";
                    echo "<div>$aHtml</div>";
                    foreach ($result as $res2) {
                        if($res2->type == 'question') continue;
                        $hrefURL = $NODE_IP_ARR."/public?q=".$res2->path;
                        echo "<br><image class='responsive-img' src='$hrefURL' ></image><br>";
                    }
                    echo "</div> </p>";

                    echo"
      </div>
    </form>
  </div>
                    
                </div>
                <br>
                <br>";
                }

                ?><div class="row">
                    <a id="getAnswerBtn" class="waves-effect right red white-text waves-teal btn-flat">View Answer &nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-circle-right"></i> </a>
                    <div class="clearfix"></div><br>
                    <a id="submitBtn" class="waves-effect right green white-text waves-teal btn-flat">Submit &nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-circle-right"></i> </a>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>

</div>

<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../../../static/js/katex.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
