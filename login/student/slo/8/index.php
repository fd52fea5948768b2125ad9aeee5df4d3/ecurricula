<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

$username = $_SESSION['username'];
$STD_TABLE_NAME = "STD_DB_".$username;

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
}


$courseCode = $_GET['q'];
$sessionID = $_GET['id'];

$sloID = $courseCode."".$sessionID."2";

$_SESSION['quiz_slo'] = $sloID;

$QUERY_STRING = "?q=".$courseCode."&id=".$sessionID;
if(!$courseCode || strlen($courseCode) > 10) {
    displayError("Invalid Subject");
}

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $sloID];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.$STD_TABLE_NAME",$query);
$rows = $rows->toArray();


$SLO_MARKS_1 = $rows[0]->quizMarks;
$SLO_MARKS_2 = $rows[0]->sAMarks;
$SUBMISSION_STATUS = $rows[0]->submittedStatus;


$query = ['_id' => $username.$sloID];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.PRACTICE_REQUEST_TABLE",$query);
$rows = $rows->toArray();
$practiceStatus = $rows[0]->status;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>SLO 2 - Assessment</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        body {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {
            body {
                padding-left: 0;
            }
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .big {
            font-size: 25px;
        }

        .mid {
            font-size: 20px;
        }


        .mono {
            font-family: monospace;
            font-size: 0.8em;
        }

        .collection .collection-item {
            line-height: 2.4em !important;
        }

        .small-text {
            padding-left: 15px;
            font-size: 10px;
            text-transform: uppercase;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"> </img>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a href="<?php echo "../1$QUERY_STRING" ?>">Rationale & Outcomes</a></li>
    <li><a href="<?php echo "../2$QUERY_STRING" ?>">Learning Plans</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -1</a></li>
    <li><a class="waves-effect" href="<?php echo "../3$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../4$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -2</a></li>
    <li><a class="waves-effect" href="<?php echo "../5$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Assessment</a></li>
    <li><a href="<?php echo "../7$QUERY_STRING"?>" class="waves-effect" href="<?php echo "../7$QUERY_STRING"?>">SLO-1</a></li>
    <li class="grey lighten-2"><a class="waves-effect">SLO-2</a></li>
</ul>


<ul id="slide-out-small" class="hide-on-med-and-up side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a href="<?php echo "../1$QUERY_STRING" ?>">Rationale & Outcomes</a></li>
    <li><a href="<?php echo "../2$QUERY_STRING" ?>">Learning Plans</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -1</a></li>
    <li><a class="waves-effect" href="<?php echo "../3$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../4$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -2</a></li>
    <li><a class="waves-effect" href="<?php echo "../5$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Assessment</a></li>
    <li><a class="waves-effect" href="<?php echo "../7$QUERY_STRING"?>">SLO-1</a></li>
    <li class="grey lighten-2"><a class="waves-effect">SLO-2</a></li>
</ul>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">

            <?php
            if($practiceStatus == 1) {
                echo "<ul class=\"collection\">";
                if(intval($SUBMISSION_STATUS/100) == 1) {
                    echo "<li class=\"collection-item\"><span class=\"mid\">Quiz</span><span class=\"small-text red-text text-accent-4\">(Last Attempt - $SLO_MARKS_1%)</span> <a target='_blank' href=\"./../quiz$QUERY_STRING&i=2\" class=\"secondary-content blue btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                }else {
                    echo "<li class=\"collection-item\"><span class=\"mid\">Quiz</span> <a target='_blank' href=\"./../quiz$QUERY_STRING&i=2\" class=\"secondary-content blue btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                }

                if(intval(($SUBMISSION_STATUS/10)%10) == 1) {
                    echo "<li class=\"collection-item\"><span class=\"mid\">Short Question</span><span class=\"small-text red-text text-accent-4\">(Last Attempt -  $SLO_MARKS_2%)</span> <a target='_blank' href=\" ./../question$QUERY_STRING&i=2\" class=\"secondary-content orange btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                }else {
                    echo "<li class=\"collection-item\"><span class=\"mid\">Short Question</span><a target='_blank' href=\"./../question$QUERY_STRING&i=2\" class=\"secondary-content orange btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                }

                if(intval($SUBMISSION_STATUS%10) == 1) {
                    echo "<li class=\"collection-item\"><span class=\"mid\">Long Question</span><span class=\"small-text green-text text-accent-4\">OPENED</span><a target='_blank' href=\"./../longquestion$QUERY_STRING&i=2\" class=\"secondary-content deep-purple btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                }else {
                    echo "<li class=\"collection-item\"><span class=\"mid\">Long Question</span><a target='_blank' target='_blank' href=\"./../longquestion$QUERY_STRING&i=2\" class=\"secondary-content deep-purple btn\"><i class=\"fa big fa-arrow-circle-right\"></i> </a><br><span> </span> </li>";
                }



                echo "</ul><br>
            <div class=\"right grey-text text-lighten-1 mono\">
                * Refresh the page after completion, to see the result!!!
            </div>
            <div class=\"clearfix\"> </div>";
            }else {
                echo "<div class='center'><div class='chip'><i class='fa fa-warning'></i> Learning Practice Material Must be Completed</div></div> ";
            }
            ?>

        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
