<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}


if(!isset($_SESSION) || $_SESSION['role'] != 'S') {
    displayError("Session Expired!!!");
}

$courseCode = $_GET['q'];
$sessionID = $_GET['id'];

$QUERY_STRING = "?q=".$courseCode."&id=".$sessionID;
if(!$courseCode || strlen($courseCode) > 10) {
    displayError("Invalid Subject");
}
$TITLES = ['Recap: Yesterday\'s Topic', 'SLO-1 Content','SLO-1 Practice','SLO-2 Content','SLO-2 Practice','Summarize Today\'s Topic'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $courseCode.$sessionID];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.LEARNING_PLAN_TABLE",$query);
$rows = $rows->toArray();




?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Learning Plan</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        body {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {
            body {
                padding-left: 0;
            }
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .big {
            font-size: 1.4em;
        }

        .sub {
            font-family: monospace;
            font-size: .8em;
        }
    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"> </img>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a href="<?php echo "../1$QUERY_STRING" ?>">Rationale & Outcomes</a></li>
    <li class="grey lighten-2"><a>Learning Plans</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -1</a></li>
    <li><a class="waves-effect" href="<?php echo "../3$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../4$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -2</a></li>
    <li><a class="waves-effect" href="<?php echo "../5$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Assessment</a></li>
    <li><a class="waves-effect" href="<?php echo "../7$QUERY_STRING"?>">SLO-1</a></li>
    <li><a class="waves-effect" href="<?php echo "../8$QUERY_STRING"?>">SLO-2</a></li>
</ul>


<ul id="slide-out-small" class="hide-on-med-and-up side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a href="<?php echo "../1$QUERY_STRING" ?>">Rationale & Outcomes</a></li>
    <li class="grey lighten-2"><a>Learning Plans</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -1</a></li>
    <li><a class="waves-effect" href="<?php echo "../3$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../4$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">SLO -2</a></li>
    <li><a class="waves-effect" href="<?php echo "../5$QUERY_STRING"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6$QUERY_STRING"?>">Learning Practice</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Assessment</a></li>
    <li><a class="waves-effect" href="<?php echo "../7$QUERY_STRING"?>">SLO-1</a></li>
    <li><a class="waves-effect" href="<?php echo "../8$QUERY_STRING"?>">SLO-2</a></li>
</ul>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-table"></i> Learning Plan</h5>
            <hr><br>

            <table class="bordered striped">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>Topic Name</th>
                        <th class="center">Conceive</th>
                        <th class="center">Design</th>
                        <th class="center">Implement</th>
                        <th class="center">Operate</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    function getClass($val) {
                        if($val == 0) {
                            return "<td class=\"center\"><i class=\"fa fa-times red-text text-darken-3 big\"></i> </td>";
                        }
                        return "<td class=\"center\"><i class=\"fa fa-check green-text text-darken-3 big\"></i> </td>";
                    }
                    for($i=0;$i<6;$i++) {
                        $min = $rows[0]->time[$i];
                        $name = $TITLES[$i];
                        echo "
                    <tr>
                        <td>$min &nbsp; <span class=\"sub grey-text\">Min.</span></td>
                        <td>$name</td>";
                        echo getClass($rows[0]->conceive[$i]);
                        echo getClass($rows[0]->design[$i]);
                        echo getClass($rows[0]->implement[$i]);
                        echo getClass($rows[0]->operate[$i]);

                    echo "</tr>";
                    }

                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
