$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    $('select').material_select();
    goBtn = $('#goBtn');
    goBtn.hide();

    var studentReportDiv = $('#studentStatusDiv');

    var facultySelect = $('#facultySelectContainer');
    facultySelect.hide();

    $('#courseSelectDiv').change(function () {
        var courseID = $(this).val();

        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseID
            },
            'url': './1.php',
            'success': function (phpdata) {
                facultySelect.html(phpdata);
                facultySelect.show();
                $('select').material_select();
                goBtn.show();
            }
        });
    });


    goBtn.click(function () {
        var facultyID = $('#facultySelectDiv').val();
        var courseCode = $('#courseSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'facultyID': facultyID,
                'courseID': courseCode
            },
            'url': './2.php',
            'success': function (phpdata) {
                studentReportDiv.show();
                studentReportDiv.html(phpdata);
            }
        });
    });
});