<?php

session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");


if(!isset($_SESSION) || $_SESSION['role'] != 'O') {
    displayError("Session Expired!!!");
}

$client = new MongoDB\Driver\Manager($MONGO_URL);
$COURSE_CODE = $_POST['courseCode'];

$query = [];
$option = [];
$TABLE_NAME = $COURSE_CODE."_COORDINATOR_TABLE";
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
$rows = $rows->toArray();

if($rows) {
    foreach ($rows as $data) {
        $courseID = $data->facultyID;
        $facultyName = $data->facultyName;
        echo "<option value='$courseID'>$courseID"." - "."$facultyName</option>";
    }
}

?>
