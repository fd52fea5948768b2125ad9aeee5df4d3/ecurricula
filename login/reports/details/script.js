$(document).ready(function() {
    $('select').material_select();
    var contentDiv = $('#contentDiv');

    var type = 2;

    var unitBtn = $('#unitBtn');
    var sloBtn = $('#sloBtn');
    var internalBtn = $('#internalBtn');

    unitBtn.click(function () {
       type = 0;
       contentDiv.html('');
    });

    $('#courseSelectDiv').change(function () {
        var courseCode = $(this).val();
        if(!courseCode) {
            window.alert("Enter a Valid Course Code");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'courseCode': courseCode
            },
            'url': './4.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                $('#facultySelectDiv').html(phpdata);
                $('select').material_select();
            }
        });
    });

    internalBtn.click(function () {
       type = 2;
       contentDiv.html('');
    });

    sloBtn.click(function () {
       type = 1;
        contentDiv.html('');
    });

    $('#goBtn').click(function () {
        if(type === 1) {
            renderSLOData();
        }else if(type == 0) {
            renderUnitData();
        }else if(type == 2) {
            renderInternalData();
        }
    });

    function renderInternalData()  {
        var facultyID = $('#facultySelectDiv').val();
        var courseCode = $('#courseSelectDiv').val();

        if(!facultyID || !courseCode) {
            window.alert("Select a Valid Subject and Faculty");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'facultyID': facultyID,
                'courseCode': courseCode
            },
            'url': './3.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
            }
        });
    }

    function renderUnitData() {var facultyID = $('#facultySelectDiv').val();
        var courseCode = $('#courseSelectDiv').val();

        if(!facultyID || !courseCode) {
            window.alert("Select a Valid Subject and Faculty");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'facultyID': facultyID,
                'courseCode': courseCode
            },
            'url': './1.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
            }
        });
    }

    function renderSLOData() {
        var facultyID = $('#facultySelectDiv').val();
        var courseCode = $('#courseSelectDiv').val();

        if(!facultyID || !courseCode) {
            window.alert("Select a Valid Subject and Faculty");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'facultyID': facultyID,
                'courseCode': courseCode
            },
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
            }
        });
    }





    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});