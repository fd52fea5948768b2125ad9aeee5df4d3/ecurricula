<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'O') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);

$query = ['_id' => $SESSION_USERNAME];
$query = new MongoDB\Driver\Query($query,[]);
$rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);
$rows = $rows->toArray();

$PREFILL_USERNAME = $rows[0]->username;
$PREFILL_DEPARTMENT = $rows[0]->department;
$PREFILL_FNAME = $rows[0]->firstName;
$PREFILL_LNAME = $rows[0]->lastName;
$PREFILL_MOBILE = $rows[0]->mobile;
$PREFILL_EMAIL = $rows[0]->email;


date_default_timezone_set("asia/kolkata");
$PREFILL_DOB = $rows[0]->dob."";




?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Admin Profile</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down"><img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>

        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Admin Profile</span><br>

            <!-- Username -->
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-user prefix"></i>
                            <input id="username" disabled value="<?php echo $PREFILL_USERNAME; ?>" type="text" name="username" class="validate">
                            <label for="username">Registration Number</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l6">
                    <div class="row input-field">
                        <div class="col s12 input-field">
                            <select disabled id="departmentList">
                                <option value="" disabled selected><?php echo $PREFILL_DEPARTMENT; ?></option>
                            </select>
                            <label>Department</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Username -->

            <!-- Password -->
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-lock prefix"></i>
                            <input id="password" type="password" name="username" class="validate">
                            <label for="password">Password</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-lock prefix"></i>
                            <input id="passwordRetype" type="password" name="username" class="validate">
                            <label for="passwordRetype">Confirm Password</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Password -->
            <!-- Name -->
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-user-circle-o prefix"></i>
                            <input id="firstName" value="<?php echo $PREFILL_FNAME; ?>" type="text" name="username" class="validate">
                            <label for="firstName">First Name</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 l6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-user-circle-o prefix"></i>
                            <input id="lastName" value="<?php echo $PREFILL_LNAME; ?>" type="text" name="username" class="validate">
                            <label for="lastName">Last Name</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Name -->
            <!-- Email -->
            <div class="row">
                <div class="col s12 m8 l8">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-envelope prefix"></i>
                            <input id="email" value="<?php echo $PREFILL_EMAIL; ?>" type="text" name="username" class="validate">
                            <label for="email">Email</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4 l4">

                    <label for="dob">Date of Birth </label>
                    <input id="dob" data-current="<?php echo $PREFILL_DOB; ?>" type="date" class="datepicker">

                </div>
            </div>
            <!-- Email -->
            <!-- Mobile -->
            <div class="row">
                <div class="col s10 m8 l6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <i class="fa fa-mobile-phone prefix"></i>
                            <input value="<?php echo $PREFILL_MOBILE; ?>" id="mobilenumber" type="tel" name="username" class="validate">
                            <label for="mobilenumber">Mobile Number</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Moblie -->
            <div class="row center red-text text-darken-2 flow-text">
                <p id ="errorMsg">Error Message Will Come Here...</p>
            </div>
            <div class="row center">
                <a class="waves-effect waves-light btn-flat grey darken-3 white-text" id="button"><b>Update</b></a>
            </div>




        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
