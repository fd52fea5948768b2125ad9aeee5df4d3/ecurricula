<?php
session_start();
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_POST['facultyID'];
$COURSE_CODE = $_POST['courseID'];
$client = new MongoDB\Driver\Manager($MONGO_URL);


$regex = new MongoDB\BSON\Regex("");
$query = ['courseID' => $COURSE_CODE, 'studentID' => $regex];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.FACULTY_$SESSION_USERNAME",$query);
$rows = $rows->toArray();


echo "<table class=\"striped bordered\">
                    <thead>
                    <tr>
                        <th rowspan='2'>Sr. No</th>
                        <th rowspan='2'>Student ID</th>
                        <th rowspan='2'>Student Name</th>
                        <th colspan='5' class='center'>Unit Marks</th>
                        <th rowspan='2'> </th>
                        <th colspan='5' class='center'>SLO Completed</th>
                    </tr>
                    <tr>
                        <th>Unit 1</th>
                        <th>Unit 2</th>
                        <th>Unit 3</th>
                        <th>Unit 4</th>
                        <th>Unit 5</th>
                        
                        <th>Unit 1</th>
                        <th>Unit 2</th>
                        <th>Unit 3</th>
                        <th>Unit 4</th>
                        <th>Unit 5</th>
                    </tr>
                    </thead>
                    <tbody>";

$count = 0;
foreach ($rows as $data) {
    $studentID = $data->studentID;
    $studentName = $data->studentName;
    $count++;

    $marks = $data->sloCompleted;
    $status = $data->unitMarks;

    echo "<tr>
                        <td class='center'>$count. </td>
                        <td>$studentID</td>
                        <td>$studentName</td>";

    for($i=0;$i<5;$i++) {
        echo "<td class='center'>".($status[$i])."</td>";
    }

    echo "<th> </th>";

    for($i=0;$i<5;$i++) {
        echo "<td class='center'>".($marks[$i])." %</td>";
    }
    echo "</tr>";
}



exit;


?>