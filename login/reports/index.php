<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
$client = new MongoDB\Driver\Manager($MONGO_URL);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/materialize.min.css">
    <title>Session Rationale & Outcomes</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .dark {
            width: 1px;
            border-right: 1px solid green;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }
    </style>
</head>
<body>

<ul id="dropdown1" class="dropdown-content">
    <li><a href="./profile/">Profile</a></li>
    <li><a href="./details">Student Overall Report</a></li>
    <li><a href="./student/">Student Details</a></li>
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png">
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a class="dropdown-button" data-activates="dropdown1">Options&nbsp;&nbsp; <i class="fa big fa-arrow-circle-down"></i></a></li>
            <li><a href="./../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Reports</span><br>

            <br><h5>STUDENT REPORT</h5><hr><br>
            <div class="row">
                <div class="col s12 m6 input-field">
                    <select id="courseSelectDiv">
                        <option disabled value="-1">Select a Course</option>
                        <?php
                        $query = [];
                        $option = [];
                        $TABLE_NAME = "COURSE_TABLE";
                        $query = new MongoDB\Driver\Query($query,$option);
                        $rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
                        $rows = $rows->toArray();

                        if($rows) {
                            foreach ($rows as $data) {
                                $courseCode = $data->courseCode;
                                $courseName = $courseCode." - ".$data->courseName;
                                echo "<option value='$courseCode'>$courseName</option>";
                            }
                        }
                        ?>
                    </select>
                    <label for="courseSelectDiv">Select Course</label>
                </div>
                <div class="col s12 m6 input-field">
                    <div id="facultySelectContainer"><select id="facultySelectDiv">

                    </select>
                    <label for="facultySelectDiv">Select Faculty</label></div>
                </div>
                <div class="col s12 m6">
                    <br>
                    <a id="goBtn" class="btn grey darken-1 waves-effect">GO</a>
                </div>
            </div>

            <div hidden id="studentStatusDiv">

            </div>


        </div>
    </div>
</div>

<div id="sessionSelectModal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Select a Session</h4>
        <div class="row">
            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="unitSelectModal">
                            <option value="0" disabled selected>Choose a Unit</option>
                            <option value="1">Unit 1</option>
                            <option value="2">Unit 2</option>
                            <option value="3">Unit 3</option>
                            <option value="4">Unit 4</option>
                            <option value="5">Unit 5</option>
                        </select>
                        <label for="unitSelectModal">Select Unit</label>
                    </div>
                </div>
            </div>


            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="sessionSelectModal2">
                            <option value="0" disabled selected>Choose a Session</option>
                            <?php
                            for($z=1;$z<=$TOTAL_L_T_P;$z++) {
                                echo "<option value=".$z.">Session $z</option>";
                            }
                            ?>
                        </select>
                        <label for="sessionSelectModal2">Select Session</label>
                    </div>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="sloSelectModal">
                            <option value="1">SLO 1</option>
                            <option value="2">SLO 2</option>
                        </select>
                        <label for="sloSelectModal">Select SLO</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <a id="modalSubmitButton" class="btn right waves-ripple blue">select session</a>
        </div>
        <div class="clearfix">
            <h6 class="red-text center text-darken-2" id="modalErrorText"></h6>
        </div>

        <h4>Select a Unit</h4>
        <div class="row">
            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="unitSelectModal2">
                            <option value="0" disabled selected>Choose a Unit</option>
                            <option value="1">Unit 1</option>
                            <option value="2">Unit 2</option>
                            <option value="3">Unit 3</option>
                            <option value="4">Unit 4</option>
                            <option value="5">Unit 5</option>
                        </select>
                        <label for="unitSelectModal2">Select Unit</label>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <a id="modalSubmitButton2" class="btn right waves-ripple blue">select unit</a>
        </div>


        <div class="clearfix">
            <h6 class="red-text center text-darken-2" id="modalErrorText2"></h6>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
