<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");


$client = new MongoDB\Driver\Manager($MONGO_URL);
$courseCode = $_POST['courseID'];

$query = [];
$option = [];
$TABLE_NAME = "$courseCode"."_COORDINATOR_TABLE";
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
$rows = $rows->toArray();
echo "<select id=\"facultySelectDiv\">";


echo "<option disabled value='-1'>Select a Faculty</option>";

if($rows) {
    foreach ($rows as $data) {
        $facultyID = $data->facultyID;
        $facultyName = $facultyID." - ".($data->facultyName);
        echo "<option value='$facultyID'>$facultyName</option>";
    }
}

echo "</select>
                    <label for=\"facultySelectDiv\">Select Faculty</label>";
?>