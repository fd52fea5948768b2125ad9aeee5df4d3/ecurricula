$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    $('select').material_select();
    var studentCount = $('#studentCountDiv');
    studentCount.hide();

    var countP = $('#studentCountP');


    $('#modalSubmitButton').click(function () {
        var session =  $('#sessionSelectModal2').val();
        var errorDiv = $('#modalErrorText');
        var unit =  $('#unitSelectModal').val();
        var slo = $('#sloSelectModal').val();

        if(!unit) {
            errorDiv.text("Please Select a Unit");
            return;
        }

        if(!session) {
            errorDiv.text("Please Select a Session");
            return;
        }


        window.location.href = "./content/1?u="+unit+"&i="+slo+"&s="+session;

    });

    $('#goBtn').click(function () {
        var courseCode = $('#courseSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './1.php',
            'success': function (phpdata) {
                studentCount.show();
                countP.html(phpdata);
            }
        });
    });
});