$(document).ready(function() {
    $('select').material_select();
    var contentDiv = $('#contentDiv');

    var type = 2;

    var unitBtn = $('#unitBtn');
    var sloBtn = $('#sloBtn');
    var internalBtn = $('#internalBtn');

    unitBtn.click(function () {
       type = 0;
       contentDiv.html('');
    });

    internalBtn.click(function () {
       type = 2;
       contentDiv.html('');
    });

    sloBtn.click(function () {
       type = 1;
        contentDiv.html('');
    });

    $('#goBtn').click(function () {
        if(type === 1) {
            renderSLOData();
        }else if(type == 0) {
            renderUnitData();
        }else if(type == 2) {
            renderInternalData();
        }
    });

    function renderInternalData()  {
        var courseCode = $('#courseSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './3.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
            }
        });
    }

    function renderUnitData() {
        var courseCode = $('#courseSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './1.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
            }
        });
    }

    function renderSLOData() {
        var courseCode = $('#courseSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
            }
        });
    }





    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});