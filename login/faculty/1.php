<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    die("Session Expired!!!");
}

include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];

$client = new MongoDB\Driver\Manager($MONGO_URL);


$regex = new MongoDB\BSON\Regex("");
$query = ['courseID' => $COURSE_CODE,'studentID' => $regex];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.FACULTY_$SESSION_USERNAME",$query);
$rows = $rows->toArray();

echo sizeof($rows);

exit;
?>