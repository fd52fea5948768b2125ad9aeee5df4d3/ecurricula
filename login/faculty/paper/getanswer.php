<?php
session_start();
include_once(__DIR__ . "/../../../includes/general.config.php");
include_once(__DIR__ . "/../../../includes/mongo.db.config.php");
include_once(__DIR__ . "/getImages.php");

date_default_timezone_set('Asia/Kolkata');

$COURSE_CODE = $_POST['courseCode'];
$STUDENT_ID = $_POST['studentID'];
$UNIT_ID = $COURSE_CODE.$_POST['unitID'];
$UNIT_SUB_ID = $_POST['unitID'];
$client = new MongoDB\Driver\Manager($MONGO_URL);

$query = ['_id' => $COURSE_CODE];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
$courseName = $rows[0]->courseName;

$query = ['_id' => $STUDENT_ID.$UNIT_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.UNIT_QUESTIONS_TABLE",$query);
$rows = $rows->toArray();
$qAIDS = $rows[0]->qAIDs;
$sAIDS = $rows[0]->sAIDs;
$lAIDS = $rows[0]->lAIDs;

$query = ['_id' => array("\$in" => $qAIDS)];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$qARows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);
$qARows = $qARows->toArray();

$query = ['_id' => array("\$in" => $sAIDS)];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$sARows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
$sARows = $sARows->toArray();

$query = ['_id' => array("\$in" => $lAIDS)];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$lARows = $client->executeQuery("$DB_NAME.LONG_QUESTION_TABLE",$query);
$lARows = $lARows->toArray();

?>
<html>

<head>
    <title>Question Paper</title>
    <style>

        @media print {
            .container {
                padding: 10px;
                margin: 0;
                font-weight: 200;
                font-family: Serif, serif;
            }


            .center {
                text-align: center;
            }

            .h1 {
                font-size: 20px;
            }

            .h2 {
                font-size: 15px;
            }

            h3, a.h3 {
                font-size: 14px;
            }

            p {
                font-size: 12px;
                padding: 0;
                margin: 0 0 5px 0;
            }
            .bold {
                font-weight:bold;
            }

            .questions {
                padding: 0 0 0 10px;
                margin: 0 0 5px 0;
                font-family: sans-serif;
            }

            .options {
                padding: 0 0 5px 40px;
                margin: 0 0 5px 0;
                font-family: sans-serif;
            }
            .right {
                float: right;
            }
        }

        @media screen {
            body {
                width: 70%;
                margin: auto;

            }
            .container {
                border: 1px solid black;
                padding: 15px 15px 15px 15px;
                margin: 10px;
                font-weight: 200;
                font-family: Serif, serif;
            }
            .center {
                text-align: center;
            }

            .h1 {
                font-size: 1.7em;
            }

            .h2 {
                font-size: 1.4em;
            }

            .h3, a.h3 {
                font-size: 1.1em;
            }

            p {
                font-size: 1em;
                padding: 0;
                margin: 0 0 10px 0;
            }
            .bold {
                font-weight:bold;
            }

            .questions {
                padding: 0 0 0 30px;
                margin: 0 0 15px 0;
                font-family: sans-serif;
            }

            .options {
                padding: 0 0 8px 50px;
                margin: 0 0 8px 0;
                font-family: sans-serif;
            }
            .right {
                float: right;
            }

            .clearfix {
                clear: both;
            }
        }

    </style>
</head>
<body>
<div class="container">
    <p class="h1 center"><b>UNIT TEST QUESTION PAPER, <?php echo strtoupper(date('F Y')); ?></b></p>
    <p class="h3 center">Unit <?php echo $UNIT_SUB_ID; ?></p>
    <p class="h2 center"><?php echo strtoupper($COURSE_CODE." - ".$courseName);?></p>
    <br>
    <a class="h3">Reg. No: <?php echo $STUDENT_ID; ?></a>
    <a class="h3 right">Max Marks: 100</a><br>
    <p class="clearfix"> </p>
    <hr>
    <p class="h2 bold center">Part A - MCQ</p><br>
    <?php
    $levelCount = 1;
    $count = 0;
    foreach ($qARows as $data) {
        if($count%2 == 0) {
            echo "<h4>Level $levelCount</h4>";
            $levelCount++;
        }
        $count++;
        $qName = nl2br($data->name);
        echo "<h3 class='questions'> ".$count.". $qName</h3>";
        echo getImages($data->sessionID,$COURSE_CODE,$data->_id,'quiz',$NODE_IP_ARR,$NODE_URL);

        $options = $data->options;
        $ch = 'A';
        $optionCount = 1;
        foreach ($options as $option) {
            $option = nl2br($option);
            if($data->correctOption == $optionCount)
                echo "<p class='options'>&#x2713; $ch. ".$option."</br></p>";
            else
                echo "<p class='options'>&nbsp;&nbsp;&nbsp;&nbsp;$ch. ".$option."</br></p>";
            $ch++;
            $optionCount++;
        }
        echo "<br>";
    }
    ?>
    <br>
    <p class="h2 bold center">Part B - Short Answer Question</p>
    <?php
    $count = 0;
    foreach ($sARows as $data) {
        $count++;
        $qName = nl2br($data->name);
        $qAns = nl2br($data->answer);
        echo "<h3 class='questions'>Level ".$count.". $qName</h3>";
        echo getImages($data->sessionID,$COURSE_CODE,$data->_id,'sa',$NODE_IP_ARR,$NODE_URL);
        echo "<p class='questions'>Ans. $qAns</p>";
    }
    ?>
    <br>
    <p class="h2 bold center">Part C - Long Answer Question</p>
    <?php
    $count = 0;
    foreach ($lARows as $data) {
        $count++;
        $qName = nl2br($data->name);
        $qAns = nl2br($data->answer);
        echo "<h3 class='questions'>Level ".$count.". $qName</h3>";
        echo getImages($data->sessionID,$COURSE_CODE,$data->_id,'la',$NODE_IP_ARR,$NODE_URL);
        echo "<p class='questions'>Ans. $qAns</p>";

    }
    ?>
</div>

</body>
</html>

