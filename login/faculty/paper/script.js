$(document).ready(function() {
    $('select').material_select();
    var goBtn = $('#goBtn');
    var go2Btn = $('#go2Btn');
    goBtn.hide();
    go2Btn.hide();
    var contentDiv = $('#tableDiv');
    $('#unitSelectContainer').hide();

    $('#courseSelectDiv').on('change',function () {
        getStudentList();
    });

    function getStudentList() {
        var courseCode = $('#courseSelectDiv').val();
        if(courseCode == -1) {
            window.alert("Select a valid course");
        }
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    $('#studentSelectDiv').html(phpdata['html']);
                    $('select').material_select();
                    goBtn.show();
                    go2Btn.show();
                    $("#unitSelectContainer").show();
                }
            }
        });
    }

    goBtn.click(function(){
        var courseCode = $('#courseSelectDiv').val();
        var studentID = $('#studentSelect').val();
        var unitID = $('#unitSelect').val();

        //$.redirect('./getpaper', {'courseCode': courseCode, 'arg2': 'value2'});
        //sreturn;

        var url = 'getpaper.php';
        var form = $('<form hidden action="' + url + '" method="POST">' +
            '<input type="text" name="courseCode" value="' + courseCode + '" />' +
            '<input type="text" name="studentID" value="' + studentID + '" />' +
            '<input type="text" name="unitID" value="' + unitID + '" />' +
            '</form>');
        $('body').append(form);
        form.submit();
    });

    go2Btn.click(function(){
        var courseCode = $('#courseSelectDiv').val();
        var studentID = $('#studentSelect').val();
        var unitID = $('#unitSelect').val();

        //$.redirect('./getpaper', {'courseCode': courseCode, 'arg2': 'value2'});
        //sreturn;

        var url = 'getanswer.php';
        var form = $('<form hidden action="' + url + '" method="POST">' +
            '<input type="text" name="courseCode" value="' + courseCode + '" />' +
            '<input type="text" name="studentID" value="' + studentID + '" />' +
            '<input type="text" name="unitID" value="' + unitID + '" />' +
            '</form>');
        $('body').append(form);
        form.submit();
    });



    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});