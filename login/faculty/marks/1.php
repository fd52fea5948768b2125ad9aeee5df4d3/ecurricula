<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$UNIT_ID = $_POST['unitID'];

$finalResult['error'] = 0;

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['facultyID' => $SESSION_USERNAME, 'courseID' => $COURSE_CODE, 'status' => 1];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();

$html = "<h5>$COURSE_CODE - Marks Entry for Unit $UNIT_ID</h5><hr><table class=\"striped bordered\">
                    <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Student ID</th>
                        <th>Student Name</th>
                        <th class=\"center\">MARKS &nbsp;(out of 100)</th>
                    </tr>
                    </thead>
                    <tbody>";

$count = 0;
foreach ($rows as $data) {
    $studentID = $data->studentID;
    $studentName = $data->stdName;
    $count++;

    $STD_DB_NAME = "STD_DB_".$studentID;

    $query = ['_id' => $COURSE_CODE.$UNIT_ID];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $row2 = $client->executeQuery("$DB_NAME.$STD_DB_NAME",$query);
    $row2 = $row2->toArray();
    $value = $row2[0]->lAMarks;

    $html = $html."<tr>
                        <td>$count. </td>
                        <td>$studentID</td>
                        <td>$studentName</td>
                        <td class='center'>
      <input  class='narrow unitMarksInput' value=\"$value\" data-uniqueID='$studentID' type=\"number\"></td>
                        </tr>";
}


$html = $html."</tbody>
                </table><div class='right'>
                    <br><a id='updateBtn' class='blue btn wave-effect'>Update</a>
                </div><div class='clearfix'> </div>";
$finalResult['html'] = $html;
echo json_encode($finalResult);
exit;


?>