<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}
$client = new MongoDB\Driver\Manager($MONGO_URL);

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$UNIT = $_POST['unit'];
$MARKS = $_POST['marks'];

foreach ($MARKS as $studentID => $laMarks) {
    if(intval($laMarks) < 0 || intval($laMarks) > 100) {
        displayError("Enter a Valid Marks");
    }
}

$finalResult['error'] = 0;
foreach ($MARKS as $studentID => $laMarks) {
    $STD_DB_NAME = "STD_DB_".$studentID;

    /*
    //GET STUDENT EXISTING MARKS
    $query = ['_id' => $COURSE_CODE.$UNIT];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.$STD_DB_NAME",$query);
    $rows = $rows->toArray();

    $netMarks = round((intval($rows[0]->quizMarks) + intval($rows[0]->sAMarks) + floatval($laMarks))/3,2);

    */

    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->update(
        ['_id' => $COURSE_CODE.$UNIT],
        ['$set' => ['submittedStatus' => (111),
            'lAMarks' => round(floatval($laMarks),2),
            'quizMarks' => round(floatval($laMarks),2),
            'sAMarks' => round(floatval($laMarks),2)]],
        ['multi' => false, 'upsert' => false]
    );


    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".$STD_DB_NAME", $bulkWrite, $writeConcern);



    $FACULTY_TABLE = "FACULTY_".$SESSION_USERNAME;

    $query = ['_id' => $studentID.$COURSE_CODE];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.$FACULTY_TABLE",$query);
    $rows = $rows->toArray();

    $unitMarks = $rows[0]->unitMarks;
    $unitMarks[$UNIT-1] = $laMarks;

    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->update(
        ['_id' => $studentID.$COURSE_CODE],
        ['$set' => ['unitMarks' => $unitMarks]],
        ['multi' => false, 'upsert' => false]
    );

    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".$FACULTY_TABLE", $bulkWrite, $writeConcern);
}
echo json_encode($finalResult);
exit;


?>