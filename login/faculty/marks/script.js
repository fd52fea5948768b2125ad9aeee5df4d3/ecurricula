$(document).ready(function() {
    $('select').material_select();
    var contentDiv = $('#tableDiv');

    $('#goBtn').click(function () {
        renderTable();
    });

    function renderTable() {
        var courseCode = $('#courseSelectDiv').val();
        var unitSelect = $('#unitSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode,
                'unitID': unitSelect
            },
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html("");
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                    contentDiv.html("");
                }else {
                    contentDiv.html(phpdata['html']);
                    initialize();
                }
            }
        });
    }

    function initialize() {
        $('#updateBtn').click(function () {
            var courseCode = $('#courseSelectDiv').val();
            var unitSelect = $('#unitSelectDiv').val();
            var inputs = $('.unitMarksInput');
            var data2 = {};
            for(var i=0;i<inputs.length;i++) {
                var input = inputs[i];
                data2[input.dataset.uniqueid] = input.value;
            }

            console.log(data2);
            $.ajax({
                'method': 'POST',
                'data': {
                    'courseID': courseCode,
                    'unit': unitSelect,
                    'marks': data2
                },
                'url': './2.php',
                'dataType': 'JSON',
                'error': function () {
                    window.alert("Something Wrong!");
                },
                'beforeSend': function () {
                },
                'success': function (phpdata) {
                    if(phpdata['error'] == 1) {
                        window.alert("Error!!!");
                    }if(phpdata['error'] == 2) {
                        window.alert("Error: " + phpdata['errorMsg']);
                    }else {
                        window.alert("Marks successfully Updated");
                    }
                }
            });
        });
    }


    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});