<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}

if(!isset( $_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Bad Login... Login Again!!");
}

$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Unit Marks Upload</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .block {
            overflow: hidden;
        }

        .data-div {
            padding-left: 14px;
        }

        .mono {
            font-family: monospace;
        }

        td {
            padding: 0 !important;
        }

        input[type=number].narrow {
            max-width: 75px;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">UNIT MARKS UPLOAD</span>
            <br>

            <div class="row">
                <div class="col s12">
                    <div class="row">
                        <div class="col s12 m6 input-field">
                            <select id="courseSelectDiv">
                                <?php
                                $regex = new MongoDB\BSON\Regex("UNIT_STATUS_");
                                $query = ['_id' => $regex];
                                $option = [];
                                $TABLE_NAME = "FACULTY_".$SESSION_USERNAME;
                                $query = new MongoDB\Driver\Query($query,$option);
                                $rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
                                $rows = $rows->toArray();

                                if($rows) {
                                    foreach ($rows as $data) {
                                        $courseID = $data->courseID;
                                        echo "<option value='$courseID'>$courseID</option>";
                                    }
                                }
                                ?>
                            </select>
                            <label for="courseSelectDiv">Select Course</label>
                        </div>
                        <div class="col s12 m6 input-field">
                            <select id="unitSelectDiv">
                                <option value="1">Unit 1</option>
                                <option value="2">Unit 2</option>
                                <option value="3">Unit 3</option>
                                <option value="4">Unit 4</option>
                                <option value="5">Unit 5</option>
                            </select>
                            <label for="unitSelectDiv">Select Unit</label>
                        </div>
                        <div class="col right s12 m6">
                            <br>
                            <a id="goBtn" class="right btn grey darken-1 waves-effect">GO</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tableDiv">

            </div>

        </div>
    </div>
</div>


<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
