$(document).ready(function() {
    $('select').material_select();

    $('#goBtn').click(function () {
        var studentID = $('#studentSelectDiv').val();
        var password = $('#password').val();
        var passwordRetype = $('#passwordRetype').val();

        if(!studentID || !passwordRetype || !password) {
            window.alert("Blank Field are not Allowed");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'studentID': studentID,
                'password': password,
                'passwordRe': passwordRetype
            },
            'url': './1.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                $('#resultDiv').html(phpdata);
            }
        });
    });

});