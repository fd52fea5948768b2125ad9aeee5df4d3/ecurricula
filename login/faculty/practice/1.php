<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$finalResult['error'] = 0;
$REQUEST_ID = $_POST['reqID'];

$client = new MongoDB\Driver\Manager($MONGO_URL);




$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $REQUEST_ID],
    ['$set' => ['status' => 1]],
    ['multi' => false, 'upsert' => false]
);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".PRACTICE_REQUEST_TABLE", $bulkWrite, $writeConcern);


echo json_encode($finalResult);
exit;


?>