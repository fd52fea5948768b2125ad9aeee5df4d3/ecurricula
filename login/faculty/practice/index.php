<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");
include_once(__DIR__."./../../../includes/node.mongo.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();

if(!isset( $_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Bad Login... Login Again!!");
}

$SORT_FIELD_NAME = "studentID";
if(isset($_GET['o']) && $_GET['o'] == 1) {
    $SORT_FIELD_NAME = 'courseID';
}

$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['facultyID' => $SESSION_USERNAME,'status'=>0];
$option = ['sort' => ["$SORT_FIELD_NAME" => 1]];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.PRACTICE_REQUEST_TABLE",$query);
$rows = $rows->toArray();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Pending Practice Assessment</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .block {
            overflow: hidden;
        }

        .left-margin {
            margin-left: 10px;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Pending Practice Assessment</span>
            <br>
            <div class="row">
                <div class="col s12">
                    <a class="btn waves-effect red right left-margin" id="orderStudent">Order By Student</a>
                    <a class="btn waves-effect blue right" id="orderCourse">Order By Course</a>
                </div>
            </div>
            <div class="requestTable">
                <table class="striped bordered">
                    <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Course</th>
                        <th class="center">Unit</th>
                        <th class="center">Session</th>
                        <th>Student ID</th>
                        <th>Date &amp; Time</th>
                        <th class="center">File</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $count = 0;
                    foreach ($rows as $data) {
                        $count++;
                        $regNo = $data->studentID;
                        $courseId = $data->courseID;
                        $time = $data->timestamp;
                        $hrefURL = $NODE_IP_ARR."/public?q=".$rows[0]->url;
                        $fieldID = $regNo.($data->sessionID);
                        $val = intval(preg_replace("/$courseId/", ' ', $data->sessionID, 1));
                        $unit = intval($val/1000);
                        $code = ($val%10 == 1)?'.A':'.B';
                        $session = intval(($val%1000)/10).$code;

                        echo "<tr>
                                <td>$count. </td>
                                <td>$courseId</td>
                                <td class='center'>$unit</td>
                                <td class='center'>$session</td>
                                <td>$regNo</td>
                                <td>$time</td>
                                <td class=\"center\"><a href='#' data-url='$hrefURL' class=\"btn modalButton blue\"><i class='fa fa-download'></i> </a> </td>
                                <td data-course='$courseId' data-std='$regNo' data-reqID='$fieldID' class=\"center\"><a class=\"reject btn red\">Reject</a> <a class=\"accept btn green\">Accept</a> </td>
                            </tr>";
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5>Practice Assesment</h5>
        <object id="objectPDFDisplay" type="application/pdf" width="100%" height="100%">
            <p><b>Not Supported</b>: This browser does not support PDFs. Please download the PDF to view it.</p>
        </object>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
        <a href="#!" target="_blank" id="modalDownloadButton" class="modal-action modal-close waves-effect waves-green btn-flat ">Download</a>
    </div>
</div>


<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
