$(document).ready(function() {
    $('select').material_select();
    var contentDiv = $('#tableDiv');

    $('#goBtn').click(function () {
        renderTable();
    });

    function renderTable() {
        var courseCode = $('#courseSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    contentDiv.html(phpdata['html']);
                    initiate();
                }
            }
        });
    }
    function initiate() {
        $('.selectAllCheckBox').click(function () {
            var colid = $(this).attr('data-col');
            var checked = $(this).prop('checked');

            $('.'+colid).each(function () {
                $(this).prop('checked',checked);
            });
        });

        $('#updateBtn').click(function () {
            var values = {};

            $('.unitControlCheckbox').each(function () {
                var uniqueID  = $(this).attr('data-uniqueID');
                var index  = $(this).attr('data-index');
                values[uniqueID] = values[uniqueID]?values[uniqueID]:{};
                values[uniqueID][index] = ($(this).prop('checked')) ? 1 : 0;
            });

            var courseCode = $('#courseSelectDiv').val();
            $.ajax({
                'method': 'POST',
                'data': {
                    'courseID': courseCode,
                    'values': values
                },
                'url': './2.php',
                'dataType': 'JSON',
                'error': function () {
                    window.alert("Something Wrong!");
                },
                'beforeSend': function () {
                    contentDiv.html(PRELOADER);
                },
                'success': function (phpdata) {
                    if(phpdata['error'] == 1) {
                        window.alert("Error!!!");
                    }if(phpdata['error'] == 2) {
                        window.alert("Error: " + phpdata['errorMsg']);
                    }else {
                        renderTable();
                    }
                }
            });
        });
    }

    var PRELOADER = '<div class="block preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});