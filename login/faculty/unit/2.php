<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$VALUES = $_POST['values'];

$finalResult['error'] = 0;

$client = new MongoDB\Driver\Manager($MONGO_URL);

foreach ($VALUES as $key => $value) {
    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->update(
        ['_id' => $key],
        ['$set' => ['unitStatus' => $value]],
        ['multi' => false, 'upsert' => false]
    );

    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".FACULTY_$SESSION_USERNAME", $bulkWrite, $writeConcern);

}

echo json_encode($finalResult);
exit;


?>