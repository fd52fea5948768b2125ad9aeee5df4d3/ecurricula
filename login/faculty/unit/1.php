<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];

$finalResult['error'] = 0;

$client = new MongoDB\Driver\Manager($MONGO_URL);
$regex = new MongoDB\BSON\Regex("");
$query = ['courseID' => $COURSE_CODE, 'studentID' => $regex];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.FACULTY_$SESSION_USERNAME",$query);
$rows = $rows->toArray();

$html = "<h5>$COURSE_CODE - Unit Test Control</h5><hr><table><thead>
    <tr><th rowspan='2'>Sr. NO.</th><th rowspan='2'>Reg. No.</th><th class='center'>Unit 1</th><th class='center'>Unit 2</th><th class='center'>Unit 3</th><th class='center'>Unit 4</th><th class='center'>Unit 5</th></tr>";


$html = $html."<tr>
                   <th class='center'>
                        <p>
                          <input type=\"checkbox\" data-col='col1' id='selectAll1' class=\"filled-in selectAllCheckBox\" />
                          <label for=\"selectAll1\"> </label>
                        </p>
                   </th>
                   
                   <th class='center'>
                        <p>
                          <input type=\"checkbox\" data-col='col2' id='selectAll2' class=\"filled-in selectAllCheckBox\" />
                          <label for=\"selectAll2\"> </label>
                        </p>
                   </th>
                   
                   
                   <th class='center'>
                        <p>
                          <input type=\"checkbox\" data-col='col3' id='selectAll3' class=\"filled-in selectAllCheckBox\" />
                          <label for=\"selectAll3\"> </label>
                        </p>
                   </th>
                   
                   
                   <th class='center'>
                        <p>
                          <input type=\"checkbox\" data-col='col4' id='selectAll4' class=\"filled-in selectAllCheckBox\" />
                          <label for=\"selectAll4\"> </label>
                        </p>
                   </th>
                   
                   
                   <th class='center'>
                        <p>
                          <input type=\"checkbox\" data-col='col5' id='selectAll5' class=\"filled-in selectAllCheckBox\" />
                          <label for=\"selectAll5\"> </label>
                        </p>
                   </th>
                   </tr></thead><tbody>";
$count = 0;
foreach ($rows as $row) {
    $count++;
    $regNo = $row->studentID;
    $html = $html."<tr>
            <td>$count.</td>
            <td>$regNo</td>";

    $unitStatus = $row->unitStatus;
    for($i=0;$i<5;$i++) {
        $uniqueID = $regNo.$COURSE_CODE;
        $id = $uniqueID.$i;
        $checked = ($unitStatus[$i]==0)?"":"checked";

        $colClass = "col".($i+1);

        $html = $html."<td class='center'>
        <p>
          <input type=\"checkbox\" data-uniqueID='$uniqueID' data-index='$i' id='$id' class=\"filled-in $colClass unitControlCheckbox\" $checked />
          <label for=\"$id\"> </label>
        </p>
    </td>";
    }



    $html = $html."</tr>";
}


$html = $html."</tbody></table><div class='right'>
                    <br><a id='updateBtn' class='blue btn wave-effect'>Update</a>
                </div><div class='clearfix'> </div>";
$finalResult['html'] = $html;
echo json_encode($finalResult);
exit;


?>