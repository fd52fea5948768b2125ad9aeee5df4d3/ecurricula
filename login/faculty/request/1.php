<?php
session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    die("Session Expired!!!");
}
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$STD_USERNAME = $_POST['studentID'];
$STD_NAME = $_POST['studentName'];

$finalResult['error'] = 0;
$REQUEST_ID = $_POST['reqID'];

$client = new MongoDB\Driver\Manager($MONGO_URL);


$query = ['_id' => $COURSE_CODE];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();



$L = $rows[0]->courseCategory->l;
$T = $rows[0]->courseCategory->t;
$P = $rows[0]->courseCategory->p;
$COURSE_NAME = $rows[0]->courseName;

$TOTAL = $L+$T+$P;


$STD_TABLE_NAME = "STD_DB_".$STD_USERNAME;

for($unit = 1;$unit<6;$unit++) {
    $COUNT = 0;
    for($week = 1;$week<4;$week++) {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
        for($index=1;$index<=$TOTAL;$index++) {
            $COUNT++;
            for($slot=1;$slot<3;$slot++) {
                $SLO_ID = $COURSE_CODE."".(($unit*100) + $COUNT).$slot;
                $DB_CHANGE = ['_id' => $SLO_ID,'courseCode'=>$COURSE_CODE,'sessionID' => ($unit*100 + $COUNT),
                'sloID' => $SLO_ID,'submittedStatus' => 0,'quizMarks' => 0, 'sAMarks' => 0, 'lAMarks' => 0, 'unitID' => $COURSE_CODE."".$unit];
                $bulkWrite->insert($DB_CHANGE);
            }
        }

        $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
        $writeResult = $client->executeBulkWrite($DB_NAME.".$STD_TABLE_NAME", $bulkWrite, $writeConcern);
    }
}

$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
for($unit = 1;$unit<6;$unit++) {
    $DB_CHANGE = ['_id' => $COURSE_CODE."".$unit,'courseCode'=>$COURSE_CODE,'submittedStatus' => 0,'quizMarks' => 0, 'sAMarks' => 0, 'lAMarks' => 0, 'unitID' => $COURSE_CODE."".$unit];
    $bulkWrite->insert($DB_CHANGE);
}
$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".$STD_TABLE_NAME", $bulkWrite, $writeConcern);




$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $REQUEST_ID],
    ['$set' => ['status' => 1,'courseName' => $COURSE_NAME]],
    ['multi' => false, 'upsert' => false]
);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".COURSE_REQUEST_TABLE", $bulkWrite, $writeConcern);

/*** Adding Data to Faculty ***/
$FACULTY_TABLE_NAME = "FACULTY_".$SESSION_USERNAME;
$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$DB_CHANGE = ['_id' => $STD_USERNAME.$COURSE_CODE,
                'studentID'=>$STD_USERNAME,
                'courseID' => $COURSE_CODE,
                'studentName' => $STD_NAME,
                'unitStatus' => [0,0,0,0,0],
                'sloCompleted' => [0,0,0,0,0],
                'unitMarks' => [0,0,0,0,0]];

$bulkWrite->insert($DB_CHANGE);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".$FACULTY_TABLE_NAME", $bulkWrite, $writeConcern);
/*** End of Adding Data to Faculty ***/
echo json_encode($finalResult);
exit;


?>