$(document).ready(function(){

    var PRELOADER = '<div class="block preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';

    var GREEN = '<p class="green-text text-darken-2">ACCEPTED</p>';
    var RED = '<p class="red-text text-darken-2">REJECTED</p>';



    $('.accept').click(function () {
        var data = {
            'reqID': $(this).parent().attr('data-reqID'),
            'studentID': $(this).parent().attr('data-std'),
            'courseID': $(this).parent().attr('data-course'),
            'studentName': $(this).parent().attr('data-name')
        };
        showPreloader($(this));

        var mParent = $(this).parent();
        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                showPreloader(mParent);
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    setHTML(mParent,GREEN);
                }
            }
        });
    });

    $('.reject').click(function () {
        var data = {
            'reqID': $(this).parent().attr('data-reqID')
        };
        var mParent = $(this).parent();

        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './2.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                showPreloader(mParent);
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    setHTML(mParent,RED);
                }
            }
        });
    });


    function showPreloader(mParent) {
        mParent.html(PRELOADER);
    }

    function setHTML(mparent,HTML) {
        mparent.html(HTML);
    }
});