<?php
session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    die("Session Expired!!!");
}
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$finalResult['error'] = 0;


$REQUEST_ID = $_POST['reqID'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $REQUEST_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) != 1) {
    displayError("Invalid Request");
}

if($rows[0]->status != 0) {
    displayError("Course Already Accepted");
}


$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->delete(["_id" => $REQUEST_ID]);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".COURSE_REQUEST_TABLE", $bulkWrite, $writeConcern);

echo json_encode($finalResult);
exit;


?>