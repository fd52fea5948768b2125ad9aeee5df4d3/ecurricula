<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../..";
    include_once(__DIR__."/../../error.php");
    echo $errorHTML;
    exit;
}

session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];
$NAME = $_SESSION['userFullName'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/materialize.min.css">
    <title><?php echo $NAME; ?> Home</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .title {
            font-size: 1.2em;
        }


        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }

    </style>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="./request">Student Requests</a></li>
    <li><a href="./practice">Practice Assessment</a></li>
    <li><a href="./marks">Marks Upload</a></li>
    <li><a href="./report">Student Detailed Report</a></li>
    <li><a href="./details">Student Overall Report</a></li>
    <li><a href="./profile/">Profile</a></li>
    <li><a href="./student/">Student Details</a></li>
    <li><a href="./passwords/">Change Student Passwords</a></li>
    <li><a href="./notice/">Notices</a></li>

    <!--
    <li><a href="./unit">Unit Controls</a></li>
    <li><a href="./reattempt">Student Unit Re-attempt</a></li>
    <li><a href="./paper">Student Question Paper</a></li>


    -->
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a class="dropdown-button" data-activates="dropdown1">Options&nbsp;&nbsp; <i class="fa big fa-arrow-circle-down"></i></a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Faculty Home</span>
            <br>
            <div class="row">
                <div class="col s12">
                    <p class="title">Name: <?php echo $NAME;?></p>
                    <p class="title">Faculty ID: <?php echo $SESSION_USERNAME; ?></p>
                </div>
            </div>
<br>
            <h5>COURSE STATUS</h5><hr>
            <div class="row">
                <div class="col s12 m6 input-field">
                    <select id="courseSelectDiv">
                        <?php
                        $regex = new MongoDB\BSON\Regex("UNIT_STATUS_");
                        $query = ['_id' => $regex];
                        $option = [];
                        $TABLE_NAME = "FACULTY_".$SESSION_USERNAME;
                        $query = new MongoDB\Driver\Query($query,$option);
                        $rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
                        $rows = $rows->toArray();

                        if($rows) {
                            foreach ($rows as $data) {
                                $courseID = $data->courseID;
                                echo "<option value='$courseID'>$courseID</option>";
                            }
                        }
                        ?>
                    </select>
                    <label for="courseSelectDiv">Select Course</label>
                </div>
                <div class="col s12 m6">
                    <br>
                    <a id="goBtn" class="btn grey darken-1 waves-effect">GO</a>
                </div>
            </div>
            <div class="row" hidden id="studentCountDiv">
                <div class="col s12 offset-m4 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title white-text indigo center-align sub-heading">Student Count</span>
                            <p id="studentCountP" class="center-align plus-pad">0</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
