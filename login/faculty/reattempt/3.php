<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}
function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}


$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$STUDENT_ID = $_POST['studentID'];
$UNIT_ID = $COURSE_CODE.$_POST['unitID'];

$STD_TABLE_NAME = "STD_DB_".$STUDENT_ID;

$finalResult['error'] = 0;

$client = new MongoDB\Driver\Manager($MONGO_URL);

$query = ['_id' => $UNIT_ID];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$STD_TABLE_NAME",$query);
$rows = $rows->toArray();

$presentStatus = $rows[0]->submittedStatus;

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $UNIT_ID],
    ['$set' => ['submittedStatus' => (intval($presentStatus/10)*10),'lAmarks' => 0]],
    ['multi' => false, 'upsert' => false]
);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".$STD_TABLE_NAME", $bulkWrite, $writeConcern);

$qAID = array();
$count = 0;
for($l=1;$l<4;$l++) {
    $query = ['unitID' => $UNIT_ID,'level' => "".$l];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) < 1) {
        $res = ["error" => '404',"errorMsg" => "Insufficient Short Question"];
        echo json_encode($res);
        exit;
    }

    $randomNumbers = UniqueRandomNumbersWithinRange(1,sizeof($rows),1);

    foreach ($randomNumbers as $n2) {
        $n = $n2-1;
        $qAID[$count] = $rows[$n]->_id;
        $count++;
    }
}


$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $STUDENT_ID.$UNIT_ID],
    ['$set' => ['sAIDs' => [$qAID[0],$qAID[1],$qAID[2]]]],
    ['multi' => false, 'upsert' => false]
);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".UNIT_QUESTIONS_TABLE", $bulkWrite, $writeConcern);


echo "Short Question Re-Attempt enabled for Student ".$STUDENT_ID;

exit;
?>