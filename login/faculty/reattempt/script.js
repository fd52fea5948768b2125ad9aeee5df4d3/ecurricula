$(document).ready(function() {
    $('select').material_select();
    var qBtn = $('#qBtn');
    var sABtn = $('#sABtn');
    qBtn.hide();
    sABtn.hide();
    var contentDiv = $('#tableDiv');
    var unitSelect = $('#unitSelectContainer');
    unitSelect.hide();

    $('#courseSelectDiv').on('change',function () {
        getStudentList();
    });

    function getStudentList() {
        var courseCode = $('#courseSelectDiv').val();
        if(courseCode == -1) {
            window.alert("Select a valid course");
        }
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    $('#studentSelectDiv').html(phpdata['html']);
                    $('select').material_select();
                    sABtn.show();
                    qBtn.show();
                    unitSelect.show();
                }
            }
        });
    }

    qBtn.click(function(){
        var c = window.confirm("Are you sure to continue?");
        if(!c) return;
        var courseCode = $('#courseSelectDiv').val();
        var studentID = $('#studentSelect').val();
        var unit = $('#unitSelectDiv').val();

        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode,
                'studentID': studentID,
                'unitID': unit
            },
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
                $('.collapsible').collapsible();
            }
        });
    });

    sABtn.click(function () {
        var c = window.confirm("Are you sure to continue?");
        if(!c) return;
        var courseCode = $('#courseSelectDiv').val();
        var studentID = $('#studentSelect').val();
        var unit = $('#unitSelectDiv').val();

        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode,
                'studentID': studentID,
                'unitID': unit
            },
            'url': './3.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
                $('.collapsible').collapsible();
            }
        });
    });



    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});