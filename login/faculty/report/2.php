<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}
$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];
$STUDENT_ID = $_POST['studentID'];

$finalResult['error'] = 0;

$client = new MongoDB\Driver\Manager($MONGO_URL);

/*** Collecting Data ***/
$username = $STUDENT_ID;
$courseCode = $COURSE_CODE;

$studentDBName = "STD_DB_".$username;
$courseDBName = "COURSE_TABLE";
$unitDBName = "UNITS_TABLE";

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['courseCode' => $courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$courseDBName",$query);
$rows = $rows->toArray();
$len = sizeof($rows);

if($len != 1) {
    $errorMsg = "Invalid Course Selected";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

//Get static Data
$CLR = $rows[0]->clr;
$STANDARD = $rows[0]->standards;
$PRE_REQUISITE = $rows[0]->requisite->pre;
$CO_REQUISITE = $rows[0]->requisite->co;
$PROGRESSIVE_COURSE = $rows[0]->progressiveCourse;
$COURSE_NAME = $rows[0]->courseName;
$DEPARTMENT = $rows[0]->department;
$COURSE_CATEGORY_CODE = $rows[0]->courseCategory->categoryCode;
$COURSE_CATEGORY_NAME = $rows[0]->courseCategory->categoryName;
$L = $rows[0]->courseCategory->l;
$T = $rows[0]->courseCategory->t;
$P = $rows[0]->courseCategory->p;
$C = $rows[0]->courseCategory->c;
$CLO = $rows[0]->clo;



$query = ['courseCode' => $courseCode];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$unitDBName",$query);
$rows = $rows->toArray();
$len = sizeof($rows);

if($len != 5) {
    $errorMsg = "Insufficient Unit Data";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

$regex = new MongoDB\BSON\Regex("$courseCode([1-5])$");
$query = ['_id' => $regex];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$unitRows = $client->executeQuery("$DB_NAME.$studentDBName",$query);
$unitRows = $unitRows->toArray();
$len = sizeof($unitRows);

if($len != 5) {
    $errorMsg = "$len Insufficient Unit Data...";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

//Calculate each unit marks as avg. of all 3
$unitMarks = array();
foreach ($unitRows as $unitMark) {
    $unitMarks[$unitMark->_id] = round((floatval($unitMark->quizMarks) + floatval($unitMark->sAMarks))/2,2);
}


$UNITS = $rows;

$regex = new MongoDB\BSON\Regex("$courseCode([1-5][0-9A-z]+)");
$query = ['_id' => $regex];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$studentRows = $client->executeQuery("$DB_NAME.$studentDBName",$query);
$studentRows = $studentRows->toArray();
$len = sizeof($studentRows);


if($len != 30*($L+$P+$T)) {
    $errorMsg = "Insufficient Data...";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

//Data for each session
$sessionData = array();
$sessionMarks = array();
foreach ($studentRows as $stdRow) {
    $sessionData[$stdRow->sloID] = $stdRow->submittedStatus;
    $sessionMarks[$stdRow->sloID] = round((floatval($stdRow->quizMarks) + floatval($stdRow->sAMarks) +floatval($stdRow->lAMarks))/3,2);
}


//Constants for rendering table
$ONE_SESSION_THEORY_COUNT = $L + $T;
$ONE_SESSION_PRACTICAL_COUNT = $P;
$SESSION_FINAL_COUNT = 3*$ONE_SESSION_PRACTICAL_COUNT + 3*$ONE_SESSION_THEORY_COUNT;

$S_THEORY_MARKS = array();
$S_PRACTICAL_MARKS = array();
$S_THEORY_PERCENTAGE = array();
$S_PRACTICAL_PERCENTAGE = array();

$UNIT_COMPLETED = array();

$UNIT_FINAL_MARKS = array();
for($unitCount=1;$unitCount<=5;$unitCount++) {
    $isUnitCompleted = true;
    $S_THEORY_MARKS[$unitCount] = $S_PRACTICAL_MARKS = 0;
    for($sessionNos=1;$sessionNos<=2;$sessionNos++) {
        for($sessionCount=1;$sessionCount<=$SESSION_FINAL_COUNT;$sessionCount++) {
            $ID = $courseCode."".($unitCount*100 + $sessionCount)."".$sessionNos;




            $currentMarks = $sessionMarks[$ID];
            if($sessionData[$ID] != 111) $isUnitCompleted = false;

            if($sessionCount%3 >= $ONE_SESSION_THEORY_COUNT) {
                //Practical
                if($P != 0) {
                    $S_PRACTICAL_MARKS[$unitCount] = $S_PRACTICAL_MARKS[$unitCount] + $currentMarks;
                }
            }else{
                //Theory
                $S_THEORY_MARKS[$unitCount] = $S_THEORY_MARKS[$unitCount] + $currentMarks;
            }
        }
    }

    $S_THEORY_PERCENTAGE[$unitCount] = ($S_THEORY_MARKS[$unitCount]/(double) (2*3*$ONE_SESSION_THEORY_COUNT));
    $S_THEORY_PERCENTAGE[$unitCount] = (floatval($S_THEORY_PERCENTAGE[$unitCount]) + floatval($unitMarks[$courseCode."".$unitCount]))/2;
    $S_THEORY_PERCENTAGE[$unitCount] = round($S_THEORY_PERCENTAGE[$unitCount],2);
    $UNIT_FINAL_MARKS[$unitCount] = ($S_THEORY_PERCENTAGE[$unitCount]*0.1);


    if($P > 0) {
        $S_PRACTICAL_PERCENTAGE[$unitCount] = ($S_PRACTICAL_MARKS[$unitCount] /(double)2 * 3 * $ONE_SESSION_PRACTICAL_COUNT) * 100;
        $UNIT_FINAL_MARKS[$unitCount] = ($S_THEORY_PERCENTAGE[$unitCount]*0.05) + ($S_PRACTICAL_PERCENTAGE[$unitCount]*0.05);
    }

    $UNIT_COMPLETED[$unitCount] = false;//$isUnitCompleted;
}


/*** End of Collecting Data ***/

echo "<h5>$COURSE_NAME - $username</h5><hr><br>";

echo "<ul class=\"collapsible\" data-collapsible=\"accordion\">";


                    $UNITS = $rows;


                    for($unitCount=1;$unitCount<=5;$unitCount++) {
                        $unitName = $UNITS[$unitCount-1]->unitName;
                        $percentage = round($UNIT_FINAL_MARKS[$unitCount],2);
                        $colour = "grey";
                        if($percentage < 3.33) {
                            $colour = "red";
                        }else if($percentage < 6.66) {
                            $colour = "orange";
                        }else if($percentage != 10){
                            $colour = "green";
                        }else {
                            $colour = "blue";
                        }
                        $unitTestMark = round($unitMarks[$courseCode."".$unitCount],2);
                        $cloTheoryMarks = round($S_THEORY_MARKS[$unitCount]/200,2);
                        $cloPracticalMarks = round($S_PRACTICAL_MARKS[$unitCount]/200,2);
                        $cloTotalMarks = 3*$ONE_SESSION_THEORY_COUNT;
                        $cloPracticalTotalMarks = 3*$ONE_SESSION_PRACTICAL_COUNT;
                        echo "<li>
                                <div class=\"collapsible-header\">
                                    <b>Unit $unitCount -&nbsp;</b> $unitName  <a class='secondary-content'>
                                    <span class='badge $colour new' data-badge-caption=\"/10\">$percentage</span></a>
                                    <div class='clearfix'> </div>
                                </div>
                                <div class=\"collapsible-body\">
                                    <div class='flex-center grey lighten-1'>
                                    <div class='chip'>Unit Test Score: $unitTestMark/100</div>
                                    <div class='chip'>Theory Score: $cloTheoryMarks/$cloTotalMarks</div>";
                        if($P>0){
                            echo "<div class='chip'>Practical: $cloPracticalMarks/$cloPracticalTotalMarks</div>";
                        }
                        echo "</div>
                                    <table class='bordered centered highlight'>
                                        <thead>
                                            <tr>
                                                <th>Session</th>
                                                <th>SLO - 1</th>
                                                <th>SLO - 2 </th>
                                            </tr>
                                        </thead>
                                        <tbody>";



                        for($sessionCount = 1;$sessionCount<=$SESSION_FINAL_COUNT;$sessionCount++) {
                            $sessionID = 100*$unitCount + $sessionCount;
                            echo "<tr><td>Session $sessionCount</td>";
                            $ID = $courseCode."".(100*$unitCount + $sessionCount)."1";
                            if($sessionData[$ID] == 111) {
                                echo "<td><i class=\"fa fa-check-circle light-green-text text-darken-3 big\"></i> </td>";
                            }else if($sessionData[$ID] != 0){
                                echo "<td><i class=\"fa fa-minus-circle yellow-text text-darken-2 big\"></i> </td>";
                            }else{
                                echo "<td><i class=\"fa fa-times-circle red-text text-lighten-1  big\"></i> </td>";
                            }


                            $ID = $courseCode."".(100*$unitCount + $sessionCount)."2";
                            if($sessionData[$ID] == 111) {
                                echo "<td><i class=\"fa fa-check-circle light-green-text text-darken-3 big\"></i> </td>";
                            }else if($sessionData[$ID] != 0){
                                echo "<td><i class=\"fa fa-minus-circle yellow-text text-darken-2 big\"></i> </td>";
                            }else{
                                echo "<td><i class=\"fa fa-times-circle red-text text-lighten-1 big\"></i> </td>";
                            }
                        }
                        /*
                                                $unitTest = $unitMarks[$courseCode.$unitCount];
                                                echo "<tr><td><b>Unit Test</b></td><td>$unitTest</td></tr></tbody>
                                                                <tr><td><b>CLO Theory</b></td><td>TODO</td></tr></tbody>";

                                                if($P != 0) {
                                                    echo "<tr><td><b>CLO Practical</b></td><td>TODO</td></tr>
                                                    <tr><td><b>Learning Resources</b></td><td>LINK</td></tr>";
                                                }
                        */
                        echo "</tbody>
                                    </table>";


                        echo "</li>";
                    }
echo "</ul>";




exit;
?>