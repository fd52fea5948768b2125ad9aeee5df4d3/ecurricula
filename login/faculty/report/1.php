<?php
session_start();

include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}
$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_POST['courseID'];

$finalResult['error'] = 0;

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['facultyID' => $SESSION_USERNAME, 'courseID' => $COURSE_CODE, 'status' => 1];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();

$html = "<select id=\"studentSelect\">";

$count = 0;
foreach ($rows as $data) {
    $studentID = $data->studentID;
    $studentName = $data->stdName;
    $count++;

    $html = $html."<option value='$studentID'>$studentID - $studentName</value>";
}


$html = $html."</select>
                            <label for=\"unitSelectDiv\">Select Student</label>";
$finalResult['html'] = $html;
echo json_encode($finalResult);
exit;


?>