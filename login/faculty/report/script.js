$(document).ready(function() {
    $('select').material_select();
    var goBtn = $('#goBtn');
    goBtn.hide();
    var contentDiv = $('#tableDiv');

    $('#courseSelectDiv').on('change',function () {
        getStudentList();
    });

    function getStudentList() {
        var courseCode = $('#courseSelectDiv').val();
        if(courseCode == -1) {
            window.alert("Select a valid course");
        }
        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    $('#studentSelectDiv').html(phpdata['html']);
                    $('select').material_select();
                    goBtn.show();
                }
            }
        });
    }

    goBtn.click(function(){
        var courseCode = $('#courseSelectDiv').val();
        var studentID = $('#studentSelect').val();

        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode,
                'studentID': studentID
            },
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                contentDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                contentDiv.html(phpdata);
                $('.collapsible').collapsible();
            }
        });
    });



    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';
});