<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['sender' => $SESSION_USERNAME];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.NOTICE_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) == 0) {
    die("<h5 class='center red-text'>No Notice Posted</h5>");
}
echo "<table class='striped bordered centered'><thead><tr><th class='center'>To</th><th class='center'>Course</th><th class='center'>Message</th><th class='center'>Action</th></tr></thead><tbody>";

foreach ($rows as $message) {
    $ID = $message->_id;
    $SENDER = ($message->receiver == "*")?"ALL":$message->receiver;
    $COURSE = $message->course;
    $MESSAGE = $message->message;

    echo "<tr><td>$SENDER</td><td>$COURSE</td><td>$MESSAGE</td><td><p class='center'><a data-mid='$ID' class='btn red deleteBtn'>DELETE</a> </p></td></tr>";
}

echo "</tbody></table>";

exit;
?>