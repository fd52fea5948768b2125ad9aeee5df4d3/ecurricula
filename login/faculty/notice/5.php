<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}

$ID = $_POST['id'];
$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->delete(["_id" => new MongoDB\BSON\ObjectID($ID)]);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".NOTICE_TABLE", $bulkWrite, $writeConcern);

echo "done";

exit;
?>