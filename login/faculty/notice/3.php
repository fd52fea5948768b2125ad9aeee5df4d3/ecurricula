<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'F') {
    displayError("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['receiver' => $SESSION_USERNAME];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.NOTICE_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) == 0) {
    die("<h5 class='center green-text'>No Pending Messages</h5>");
}
echo "<table class='striped bordered centered'><thead><tr><th class='center'>Sender</th><th class='center'>Message</th><th class='center'>Action</th></tr></thead><tbody>";

foreach ($rows as $message) {
    $ID = $message->_id;
    $SENDER = $message->sender;
    $MESSAGE = $message->message;

    echo "<tr><td>$SENDER</td><td>$MESSAGE</td><td><p class='center'><a data-mid='$ID' class='btn indigo seenBtn'>SEEN</a> </p></td></tr>";
}

echo "</tbody></table>";

exit;
?>