$(document).ready(function() {
    $('select').material_select();
    var contentDiv = $('#tableDiv');

    $('#sendBtn').click(function () {
        var courseCode = $('#courseSelectDiv').val();
        var studentSelect = $('#studentSelectDiv').val();
        var message = $('#messageInput').val();
        if(!courseCode || !studentSelect || !message) {
            window.alert("Select a Valid Course and Subject, and Enter a Valid Message!!!");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode,
                'studentID': studentSelect,
                'message': message,
                'type': "GET"
            },
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] !== 200) {
                    var c = window.confirm("A Notice is already in circulation. Do you want to replace it?");
                    if(c) {
                        $.ajax({
                            'method': 'POST',
                            'data': {
                                'courseID': courseCode,
                                'studentID': studentSelect,
                                'message': message,
                                'type': "FORCE"
                            },
                            'url': './1.php',
                            'dataType': 'JSON',
                            'error': function () {
                                window.alert("Something Wrong!");
                            },
                            'success': function (phpdata) {
                                if(phpdata['error'] === 200) {
                                    window.alert("Notice Successfully circulated.");
                                    initialize();
                                }else {
                                    window.alert("Oops. There is some here!!!");
                                }
                            }
                        });
                    }
                }else {
                    initialize();
                    window.alert("Notice Successfully circulated.");
                }
            }
        });
    });

    $('#courseSelectDiv').on('change',function () {
        var courseCode = $('#courseSelectDiv').val();
        if(!courseCode) return;

        $.ajax({
            'method': 'POST',
            'data': {
                'courseID': courseCode
            },
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                $('#studentSelectDiv').html(phpdata);

                $('select').material_select();
            }
        });
    });


    function initialize() {
        var pendingDiv = $('#pendingNotificationDiv');
        $.ajax({
            'method': 'POST',
            'url': './3.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                pendingDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                pendingDiv.html(phpdata);
                seeMSG();
            }
        });

        var postedDiv = $("#postedNotificationDiv");
        $.ajax({
            'method': 'POST',
            'url': './4.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'beforeSend': function () {
                postedDiv.html(PRELOADER);
            },
            'success': function (phpdata) {
                postedDiv.html(phpdata);
                deleteMSG();
            }
        });
    }

    function deleteMSG() {
        $('.deleteBtn').click(function () {
            var id = $(this).attr('data-mid');
            $.ajax({
                'method': 'POST',
                'data': {
                    'id': id,
                },
                'url': './5.php',
                'error': function () {
                    window.alert("Something Wrong!");
                },
                'success': function (phpdata) {

                    initialize();
                }
            });
        });
    }

    function seeMSG() {
        $('.seenBtn').click(function () {
            var id = $(this).attr('data-mid');
            $.ajax({
                'method': 'POST',
                'data': {
                    'id': id,
                },
                'url': './5.php',
                'error': function () {
                    window.alert("Something Wrong!");
                },
                'success': function (phpdata) {
                    initialize();
                }
            });
        });
    }


    var PRELOADER = '<div class="block center preloader-wrapper small active">'+
        '                                        <div class="spinner-layer spinner-green-only">'+
        '                                            <div class="circle-clipper left">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="gap-patch">'+
        '                                                <div class="circle"></div>'+
        '                                            </div><div class="circle-clipper right">'+
        '                                                <div class="circle"></div>'+
        '                                            </div>'+
        '                                        </div>';

    initialize();
});