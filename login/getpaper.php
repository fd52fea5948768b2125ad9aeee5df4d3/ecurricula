<?php
$client = new MongoDB\Driver\Manager($MONGO_URL);

$query = ['_id' => $COURSE_CODE];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
$courseName = $rows[0]->courseName;

$query = ['_id' => $STUDENT_ID.$UNIT_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.UNIT_QUESTIONS_TABLE",$query);
$rows = $rows->toArray();
$qAIDS = $rows[0]->qAIDs;
$sAIDS = $rows[0]->sAIDs;
$lAIDS = $rows[0]->lAIDs;

$query = ['_id' => array("\$in" => $qAIDS)];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$qARows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);
$qARows = $qARows->toArray();

$query = ['_id' => array("\$in" => $sAIDS)];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$sARows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
$sARows = $sARows->toArray();

$query = ['_id' => array("\$in" => $lAIDS)];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$lARows = $client->executeQuery("$DB_NAME.LONG_QUESTION_TABLE",$query);
$lARows = $lARows->toArray();

?>
<html>

<head>
    <title>Question Paper</title>
    <style>
        @media print {
            .container {
                padding: 10px;
                margin: 0;
                font-weight: 200;
                font-family: Serif, serif;
            }


            .center {
                text-align: center;
            }

            .h1 {
                font-size: 20px;
            }

            .h2 {
                font-size: 15px;
            }

            h3, a.h3 {
                font-size: 14px;
            }

            p {
                font-size: 12px;
                padding: 0;
                margin: 0 0 5px 0;
            }
            .bold {
                font-weight:bold;
            }

            .questions {
                padding: 0 0 0 10px;
                margin: 0 0 5px 0;
                font-family: sans-serif;
            }

            .options {
                padding: 0 0 5px 40px;
                margin: 0 0 5px 0;
                font-family: sans-serif;
            }
            .right {
                float: right;
            }
        }

        @media screen {
            body {
                width: 70%;
                margin: auto;

            }
            .container {
                border: 1px solid black;
                padding: 15px 15px 15px 15px;
                margin: 10px;
                font-weight: 200;
                font-family: Serif, serif;
            }
            .center {
                text-align: center;
            }

            .h1 {
                font-size: 1.7em;
            }

            .h2 {
                font-size: 1.4em;
            }

            .h3, a.h3 {
                font-size: 1.1em;
            }

            p {
                font-size: 1em;
                padding: 0;
                margin: 0 0 10px 0;
            }
            .bold {
                font-weight:bold;
            }

            .questions {
                padding: 0 0 0 30px;
                margin: 0 0 15px 0;
                font-family: sans-serif;
            }

            .options {
                padding: 0 0 8px 50px;
                margin: 0 0 8px 0;
                font-family: sans-serif;
            }
            .right {
                float: right;
            }

            .clearfix {
                clear: both;
            }
        }



    </style>
</head>
<body>
    <div class="container">
    <p class="h1 center"><b>UNIT TEST QUESTION PAPER, <?php echo strtoupper(date('F Y')); ?></b></p>
    <p class="h3 center">Unit <?php echo $UNIT_SUB_ID; ?></p>
    <p class="h2 center"><?php echo strtoupper($COURSE_CODE." - ".$courseName);?></p>
    <br>
        <a class="h3">Reg. No: <?php echo $STUDENT_ID; ?></a><br>
        <a class="h3 right">Max Marks: 100</a>
        <p class="clearfix"> </p>
        <hr>
    <p class="h2 bold center">Part A - MCQ</p><br>
    <?php
    $levelCount = 1;
        $count = 0;
        foreach ($qARows as $data) {
            if($count%2 == 0) {
                echo "<h4>Level $levelCount</h4>";
                $levelCount++;
            }
            $count++;
            $qName = nl2br($data->name);
            echo "<h3 class='questions'> ".$count.". $qName</h3>";

            echo getImages($data->sessionID,$COURSE_CODE,$data->_id,'quiz',$NODE_IP_ARR,$NODE_URL);
            $options = $data->options;
            $ch = 'A';
            foreach ($options as $option) {
                $option = nl2br($option);
                echo "<p class='options'>$ch. ".$option."</br></p>";
                $ch++;
            }
            echo "<br>";
        }
    ?>
<br>
    <p class="h2 bold center">Part B - Short Answer Question</p>
    <?php
    $count = 0;
    foreach ($sARows as $data) {
        $count++;
        $qName = nl2br($data->name);
        echo "<h3 class='questions'>Level ".$count.". $qName</h3>";

        echo getImages($data->sessionID,$COURSE_CODE,$data->_id,'sa',$NODE_IP_ARR,$NODE_URL);
    }
    ?>
<br>
    <p class="h2 bold center">Part C - Long Answer Question</p>
    <?php
    $count = 0;
    foreach ($lARows as $data) {
        $count++;
        $qName = nl2br($data->name);
        echo "<h3 class='questions'>Level ".$count.". $qName</h3>";

        echo getImages($data->sessionID,$COURSE_CODE,$data->_id,'la',$NODE_IP_ARR,$NODE_URL);

    }
    ?>
    </div>

</body>
</html>

