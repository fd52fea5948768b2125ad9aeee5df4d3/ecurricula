<?php
require_once (__DIR__."./../../../includes/general.config.php");
require_once (__DIR__."./../../../includes/node.mongo.config.php");

$url = $NODE_URL."/unit/upload/material";

session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired!!!");
}


if(!isset($_SESSION['unit_id'])) {
    echo "Unit not found";
    exit;
}
$COURSE_CODE = $_SESSION['courseCode'];


echo "Session Id: ".$_SESSION['unit_id']."\n";


$tmpfile = $_FILES['file']['tmp_name'];
$filename = basename($_FILES['file']['name']);

$data = array(
    'unitID' => $_SESSION['unit_id'],
    'courseID' => $COURSE_CODE
);
  $data['file'] = new CurlFile($_FILES['file']['tmp_name'],'file/exgd',$filename);



$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = curl_exec($ch);

//close connection
curl_close($ch);
?>