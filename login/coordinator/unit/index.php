<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

session_start();

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}


if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


if(isset($_GET['u'])) {
    $UNIT_SUB_ID = $_GET['u'];
}else{
    $UNIT_SUB_ID = $_SESSION['unit_sub_id'];
}

$COURSE_CODE = $_SESSION['courseCode'];
$_SESSION['unit_sub_id'] = $UNIT_SUB_ID;
$_SESSION['unit_id'] = $COURSE_CODE.$UNIT_SUB_ID;
$UNIT_ID = $_SESSION['unit_id'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $UNIT_ID];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    displayError("Unit Details Not Entered");
    return;
}

$UNIT_NAME = $rows[0]->unitName;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Unit Learning Resources</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        .deleteBtn, .downloadBtn {
            cursor: pointer;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"/>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-graduation-cap"></i> Learning Resources
                <span class="right"><div class="chip">Unit <?php echo $UNIT_SUB_ID." - ".$UNIT_NAME; ?></div></span><div class="clearfix"> </div>
            </h5>
            <hr>

            <br>
            <h5>ADD MATERIAL</h5>
            <div class="row row-fix">
                <div class="col s12">
                    <div class="file-field input-field">
                        <div class="btn red">
                            <span>ADD</span>
                            <input id="inputField" type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
            </div>




            <div class="row">
                <br>
                <a id="updateBtn" class="blue right btn waves-ripple ">UPLOAD</a>
            </div>

            <h5>AVAILABLE MATERIAL</h5><br>
            <div id="availableQuestionDiv">


        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
