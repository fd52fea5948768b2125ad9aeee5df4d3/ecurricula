<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
/*
$query = [];
$query = new MongoDB\Driver\Query($query,[]);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$COURSES = $rows->toArray();
*/
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Coordinator Question Paper Generation</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down"><img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>

        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Coordinator Question Paper Generation for <?php echo $_SESSION['courseCode']; ?></span><br>

            <h5>Question Paper Generation </h5>
            <hr>
            <br>
            <!-- <div class="row">
                <div class="col m6 s12">
                    <div class="row">
                        <div class="col s12 input-field">
                            <select id="courseSelect">
                                <?php
                                    foreach ($COURSES as $COURSE) {
                                        $code = $COURSE->courseCode;
                                        $name = $COURSE->courseName;
                                        echo "<option value='$code'>$name</option>";
                                    }

                                ?>
                            </select>
                            <label for="courseSelect">Select A Course</label>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="row">
                <div class="col s12">
                    <div class=" row">
                        <div class="col s12 m4 input-field">
                            <input type="number" id="quizCountInput">
                            <label for="quizCountInput">Number of Quiz Questions</label>
                        </div>
                        <div class="col s12 m4 input-field">
                            <input type="number" id="sACountInput">
                            <label for="sACountInput">Number of Short Questions</label>
                        </div>
                        <div class="col s12 m4 input-field">
                            <input type="number" id="lACountInput">
                            <label for="lACountInput">Number of Long Questions</label>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col s12">
                    <a id="goBtn" class="btn right waves-effect blue">Go</a>
                </div>
            </div>



        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
