<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}
$QUESTION_DATA = ['Quiz','Short Answer','Long Answer'];
$QUESTION_KEY = ['q','sA','lA'];
$QUESTION_COUNT = [$_GET['a'],$_GET['b'],$_GET['c']];

$SESSION_USERNAME = $_SESSION['username'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
/*
$query = [];
$query = new MongoDB\Driver\Query($query,[]);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$COURSES = $rows->toArray();
*/

$PAPER_COUNTS = [1,2,5,10,15,20,25,50,100];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Coordinator Question Paper Generation</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down"><img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"></span>

        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Coordinator Question Paper Generation for <?php echo $_SESSION['courseCode']; ?></span><br>

            <?php
                $count = 0;
                foreach ($QUESTION_DATA as $topics) {
                    echo "<div class='row'><div class='col s12'><h5>$topics Questions</h5><hr>";
                    $key = $QUESTION_KEY[$count];
                    for($i=1;$i<=$QUESTION_COUNT[$count];$i++) {
                        echo "<br><h6>Question $i</h6><div class='row'>
                                <div class='col s12 m6 input-field'>
                                    <select class='unitSelect' id='unitSelect$key$i'>
                                        <option value='1'>Unit 1</option>
                                        <option value='2'>Unit 2</option>
                                        <option value='3'>Unit 3</option>
                                        <option value='4'>Unit 4</option>
                                        <option value='5'>Unit 5</option>                                
</select>  
<label for='unitSelect$key$i'>Select Unit</label>
</div><div class='col s12 m6 input-field'>
                                    <select class='levelSelect' id='levelSelect$key$i'>
                                        <option value='1'>Level 1</option>
                                        <option value='2'>Level 2</option>
                                        <option value='3'>Level 3</option>                                
</select>  
<label for='levelSelect$key$i'>Select Level</label>
</div>
</div>";
                    }
                    echo "</div></div>";
                    $count++;
                }

            ?>
            <br>
            <h5>Question Paper Count</h5>
            <hr><br>

            <div class="col m6 s12">
                <div class="row">
                    <div class="col s12 input-field">
                        <select id="countSelect">
                            <?php
                                foreach ($PAPER_COUNTS as $nos) {
                                    echo "<option value='$nos'>$nos Papers</option>";
                                }

                            ?>
                        </select>
                        <label for="countSelect">Number of Question Paper</label>
                    </div>
                </div>
            </div>

            <br><h5>Marks Distribution</h5>
            <hr><br>
            <div class="row">
                <div class="col s12 m6 l3">
                    <div class="row">
                        <div class="col s12 input-field">
                            <input type="text" id="quizTotalMarks">
                            <label for="quizTotalMarks">Quiz Total Marks</label>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l3">
                    <div class="row">
                        <div class="col s12 input-field">
                            <input type="text" id="sATotalMarks">
                            <label for="sATotalMarks">Short Question Marks</label>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l3">
                    <div class="row">
                        <div class="col s12 input-field">
                            <input type="text" id="lATotalMarks">
                            <label for="lATotalMarks">Long Question Marks</label>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l3">
                    <div class="row">
                        <div class="col s12 input-field">
                            <input type="text" id="totalMarks">
                            <label for="totalMarks">Total Marks</label>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col s12">
                    <a id="goBtn" class="btn right waves-effect blue">Go</a>
                </div>
            </div>

            <div hidden="hidden">
                <a id="quizCount"><?php echo $_GET['a'];?></a>
                <a id="sACount"><?php echo $_GET['b'];?></a>
                <a id="lACount"><?php echo $_GET['c'];?></a>
            </div>


        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
