$(document).ready(function(){
    $('select').material_select();
    $('#goBtn').click(function () {
        var unitData = {};

        var levelData = {};
        $('.levelSelect').each(function (d) {
            levelData[$(this).attr('id')] = $(this).val();
        });

        var count = $('#countSelect').val();
        var units = [false,false,false,false,false];


        $('.unitSelect').each(function (d) {
            unitData[$(this).attr('id')] = $(this).val();
            units[parseInt($(this).val()) - 1] = true;
        });

        $.ajax({
            'method': 'POST',
            'url': './1.php',
            'data': {
                'units': unitData,
                'level': levelData,
                'count': count,
                'uniqueUnits': units,
                'qCount': $('#quizCount').html(),
                'sACount': $('#sACount').html(),
                'lACount': $('#lACount').html(),
                'qMArks': $('#quizTotalMarks').val(),
                'sAMarks': $('#sATotalMarks').val(),
                'lAMarks': $('#lATotalMarks').val(),
                'tMarks': $('#totalMarks').val()
            },
            'beforeSend': function () {
                window.alert("Generating Paper.. Please Don't refresh the Page!!!");
            },
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                window.open("./download","download");
            }
        });
    });
});