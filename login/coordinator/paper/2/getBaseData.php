<?php
include_once(__DIR__."/../../../../includes/general.config.php");

function getOutcome($plo) {
    global $A_K_MAPPING;
    $MAPPING_DATA = $A_K_MAPPING;

    $returnSTR = "";

    $str = explode(',',$plo);
    for($i=0;$i<=14;$i++) {
        if($str[$i] == 1) {
            $code = $MAPPING_DATA[$i][1];
            $returnSTR = $returnSTR."$code, ";
        }
    }

    return $returnSTR;
}
function getBaseData($UNIQUE_UNITS,$CLOS,$Q_ROWS,$SA_ROWS,$LA_ROWS,$Q_MARKS,$S_MARKS,$L_MARKS) {
    $COUNT = 1;
    $Q_STR = "";
    foreach ($Q_ROWS as $data) {
        $UNIT_ID = substr($data->unitID,-1);
        $OUTCOME = getOutcome($data->plo);
        $Q_STR = $Q_STR."<tr><td>Question $COUNT</td>
        <td>IO$UNIT_ID</td><td>$OUTCOME</td><td>$Q_MARKS</td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

        $COUNT++;
    }

    $SA_STR = "";
    foreach ($SA_ROWS as $data) {
        $UNIT_ID = substr($data->unitID,-1);
        $OUTCOME = getOutcome($data->plo);

        $SA_STR = $SA_STR."<tr><td>Question $COUNT</td>
        <td>IO$UNIT_ID</td><td>$OUTCOME</td><td>$S_MARKS</td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

        $COUNT++;
    }

    $LA_STR = "";
    foreach ($LA_ROWS as $data) {
        $UNIT_ID = substr($data->unitID,-1);
        $OUTCOME = getOutcome($data->plo);
        $LA_STR = $LA_STR."<tr><td>Question $COUNT</td>
        <td>IO$UNIT_ID</td><td>$OUTCOME</td><td>$L_MARKS</td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

        $COUNT++;
    }

    $htmlSTR = "";
    $MAPPING_DATA = $A_K_MAPPING;

    $ioString = "";
    $count = 1;
    for($i=0;$i<5;$i++) {
        if($UNIQUE_UNITS[$i] == 'true') {
            $text2 = json_encode($UNIQUE_UNITS);
            $text = $CLOS[$i]->cloName;
            $ioString = $ioString . "IO" . $count . " - " . $text . "<br>";
            $count++;
        }
    }

    $outcomeString = "";
    foreach ($MAPPING_DATA as $data) {
        $code = $data[1];
        $name = $data[0];

        $outcomeString = $outcomeString. "$code: ".$name."<br>";
    }


    $htmlSTR = $htmlSTR."<br><p class='h2'><b>Instructional Objectives: </b></p>";
    $htmlSTR = $htmlSTR."<p class='questions'>$ioString</p>";

    $htmlSTR = $htmlSTR."<br><p class='h2'><b>Outcomes: </b></p>";
    $htmlSTR = $htmlSTR."<p class='questions'>$outcomeString</p><hr>";


    $htmlSTR = $htmlSTR."<br><p class='h2'><b>Outcomes Analysis: </b></p>";
    $htmlSTR = $htmlSTR."<table>
                        <tr>
                            <td><b>Question</b></td>
                            <td><b>Instructional Objective</b></td>
                            <td><b>Outcome</b></td>
                            <td><b>Maximum Marks</b></td>
                            <td><b>Marks Obtained</b></td>
                        </tr>
                    
                        $Q_STR $SA_STR $LA_STR
                        
                        <tr><td colspan='4'>Total </td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>

                  </table><br>";





    return $htmlSTR;

}