<?php
function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$courseCode = $_SESSION['courseCode'];

if(file_exists("./../../../../../temp/$courseCode.zip")) {
    header('Content-Type: application/zip');
    header('Content-disposition: attachment; filename='.$courseCode."-Papers.zip");
    header('Content-Length: ' . filesize("./../../../../../temp/$courseCode.zip"));
    readfile("./../../../../../temp/$courseCode.zip");
}else {
    displayError("No File Present for Download");
}

?>

