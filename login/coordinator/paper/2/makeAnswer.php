<?php
include_once(__DIR__."/getImages.php");
include_once(__DIR__."/getBaseData.php");
include_once(__DIR__."/getEquations.php");
date_default_timezone_set('Asia/Kolkata');
function makeAnswer($quizData,$saData,$laData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$S_MATKS,$L_MARKS,$T_MARKS,$UNIQUE_UNITS) {
    $query = ['_id' => $courseCode];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
    $rows = $rows->toArray();
    $courseName = $rows[0]->courseName;
    $CLOS = $rows[0]->clo;

    $topSTR = getBaseData($UNIQUE_UNITS,$CLOS,$quizData,$saData,$laData,$Q_MARKS,$S_MATKS,$L_MARKS);

    $CSS_KATEX_URL = $NODE_IP_ARR."/static/css/katex.min.css";
    $JS_KATEX_URL = $NODE_IP_ARR."/static/js/katex.min.js";
    $JQUERY_URL =  $NODE_IP_ARR."/static/js/jquery-3.1.1.min.js";

    $htmlSTR = "";
    //Print base HTML
    $htmlSTR = $htmlSTR."
<!doctype html>
<html lang=\"en\"><head>
        
    <link rel=\"stylesheet\" type=\"text/css\" href=\"$CSS_KATEX_URL\">
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Answer Paper</title>
    <style>
        @media print {
            .container {
                padding: 10px;
                margin: 0;
                font-weight: 200;
                font-family: Serif, serif;
            }


            .center {
                text-align: center;
            }

            .h1 {
                font-size: 20px;
            }

            .h2 {
                font-size: 15px;
            }

            h3, a.h3 {
                font-size: 14px;
            }

            p {
                font-size: 12px;
                padding: 0;
                margin: 0 0 5px 0;
            }
            .bold {
                font-weight:bold;
            }

            .questions {
                padding: 0 0 0 10px;
                margin: 0 0 5px 0;
                font-family: sans-serif;
            }

            .options {
                padding: 0 0 5px 40px;
                margin: 0 0 5px 0;
                font-family: sans-serif;
            }
            .right {
                float: right;
            }
            
 table {
    border-collapse: collapse;
    width: 100%;
}

table, th, td {
    border: 1px solid black;
}
        }

        @media screen {
            body {
                width: 70%;
                margin: auto;

            }
            .container {
                border: 1px solid black;
                padding: 15px 15px 15px 15px;
                margin: 10px;
                font-weight: 200;
                font-family: Serif, serif;
            }
            .center {
                text-align: center;
            }

            .h1 {
                font-size: 1.7em;
            }

            .h2 {
                font-size: 1.4em;
            }

            .h3, a.h3 {
                font-size: 1.1em;
            }

            p {
                font-size: 1em;
                padding: 0;
                margin: 0 0 10px 0;
            }
            .bold {
                font-weight:bold;
            }

            .questions {
                padding: 0 0 0 30px;
                margin: 0 0 15px 0;
                font-family: sans-serif;
            }

            .options {
                padding: 0 0 8px 50px;
                margin: 0 0 8px 0;
                font-family: sans-serif;
            }
            .right {
                float: right;
            }

            .clearfix {
                clear: both;
            }
            
            table {
    border-collapse: collapse;
    width: 100%;
}

table, th, td {
    border: 1px solid black;
}
        }
    </style>
</head>
<body>
<div class=\"container\">
    <p class=\"h1 center\"><b>UNIT TEST QUESTION PAPER, ".strtoupper(date('F Y'))."</b></p>
    <p class=\"h2 center\">$courseCode - $courseName</p>
    <br>
    <a class=\"h3 right\">Max Marks: $T_MARKS</a><br>
    <p class=\"clearfix\"> </p>
    <hr>";

    $htmlSTR = $htmlSTR.$topSTR;

    $Q_COUNT = sizeof($quizData);
    $Q_TMARKS = intval($Q_MARKS)*$Q_COUNT;
    //Print MCQ
    $htmlSTR = $htmlSTR."<p class=\"h2 bold center\">Part A - MCQ ($Q_MARKS x $Q_COUNT = $Q_TMARKS marks)</p><br>";
    $count = 0;
    foreach ($quizData as $data) {
        $count++;
        $qName = nl2br($data->name);
        $htmlSTR = $htmlSTR."<h3 class='questions'> ".$count.". $qName</h3>";
        $htmlSTR = $htmlSTR.makeEquationHTML($data->equation,"ANSQuizQ".$data->_id);
        $htmlSTR = $htmlSTR.getImages($data->sessionID,$courseCode,$data->_id,'quiz',$NODE_IP_ARR,$NODE_URL);
        $options = $data->options;
        $ch = 'A';
        $optionCount = 1;
        foreach ($options as $option) {
            $option = nl2br($option);
            if($data->correctOption == $optionCount)
                $htmlSTR = $htmlSTR."<p class='options'>&#x2713; $ch. ".$option."</br></p>";
            else
                $htmlSTR = $htmlSTR."<p class='options'>&nbsp;&nbsp;&nbsp;&nbsp;$ch. ".$option."</br></p>";
            $ch++;
            $optionCount++;
        }
        echo "<br>";
    }


    $S_COUNT = sizeof($saData);
    $S_TMARKS = intval($S_MATKS)*$S_COUNT;
    //Print SAs
    $htmlSTR = $htmlSTR."<br><p class=\"h2 bold center\">Part B - Short Answer Question ($S_MATKS x $S_COUNT = $S_TMARKS marks)</p>";
    $count = 0;
    foreach ($saData as $data) {
        $count++;
        $qName = nl2br($data->name);
        $qAns = nl2br($data->answer);
        $htmlSTR = $htmlSTR."<h3 class='questions'>".$count.". $qName</h3>";
        $htmlSTR = $htmlSTR.makeEquationHTML($data->qEquation,"ANSSQQ".$data->_id);
        $htmlSTR = $htmlSTR.getImages($data->sessionID,$courseCode,$data->_id,'sa',$NODE_IP_ARR,$NODE_URL);
        $htmlSTR = $htmlSTR."<p class='questions'>Ans. $qAns</p>";
        $htmlSTR = $htmlSTR.makeEquationHTML($data->aEquation,"ANSSQA".$data->_id);
        $htmlSTR = $htmlSTR.getAnswerImages($data->sessionID,$courseCode,$data->_id,'la',$NODE_IP_ARR,$NODE_URL);
    }


    $L_COUNT = sizeof($laData);
    $L_TMARKS = intval($L_MARKS)*$L_COUNT;
    //Print LAs
    $htmlSTR = $htmlSTR."<br>
    <p class=\"h2 bold center\">Part C - Long Answer Question ($L_MARKS x $L_COUNT = $L_TMARKS marks)</p>";
    $count = 0;
    foreach ($laData as $data) {
        $count++;
        $qName = nl2br($data->name);
        $qAns = nl2br($data->answer);
        $htmlSTR=$htmlSTR."<h3 class='questions'>".$count.". $qName</h3>";
        $htmlSTR = $htmlSTR.makeEquationHTML($data->qEquation,"ANSLQQ".$data->_id);
        $htmlSTR = $htmlSTR.getImages($data->sessionID,$courseCode,$data->_id,'la',$NODE_IP_ARR,$NODE_URL);
        $htmlSTR = $htmlSTR."<p class='questions'>Ans. $qAns</p>";
        $htmlSTR = $htmlSTR.makeEquationHTML($data->aEquation,"ANSLQA".$data->_id);
        $htmlSTR = $htmlSTR.getAnswerImages($data->sessionID,$courseCode,$data->_id,'la',$NODE_IP_ARR,$NODE_URL);

    }


    //Print end Strings
    $htmlSTR = $htmlSTR."
</div>


<script type=\"text/javascript\" src=\"$JS_KATEX_URL\"></script>

<script type=\"text/javascript\" src=\"$JQUERY_URL\"></script>
</body>
<script>
    function initKatex() {
        $('.equationField').each(function() {
            //console.log($(this)[0].id);
            katex.render($(this).html(),document.getElementById($(this)[0].id));
        });
    }
    
    initKatex();
</script>
</html>
";
    return $htmlSTR;
}
?>
