<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
include_once(__DIR__."/makePaper.php");
include_once(__DIR__."/makeAnswer.php");


$client = new MongoDB\Driver\Manager($MONGO_URL);
function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$courseCode = $_SESSION['courseCode'];
$UNIT_DATA = $_POST['units'];
$LEVEL_DATA = $_POST['level'];
$COUNT = $_POST['count'];

$QUIZ_COUNT = $_POST['qCount'];
$SA_COUNT = $_POST['sACount'];
$LA_COUNT = $_POST['lACount'];

$Q_MARKS = $_POST['qMArks'];
$SA_MARKS = $_POST['sAMarks'];
$LA_MARKS = $_POST['lAMarks'];
$TOTAL_MARKS = $_POST['tMarks'];

$UNIQUE_UNITS = $_POST['uniqueUnits'];

$client = new MongoDB\Driver\Manager($MONGO_URL);

delete_directory("./../../../../temp/$courseCode/");
mkdir("./../../../../temp/$courseCode",07777,true);

function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

function getRows($unitID,$levelID,$client,$tableName,$DB_NAME,$courseCode) {
    $query = ['unitID' => "$courseCode$unitID", "level" => "".$levelID];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.$tableName",$query);
    $rows = $rows->toArray();

    $num  = UniqueRandomNumbersWithinRange(0,sizeof($rows)-1,1);
    return $rows[$num[0]];
}

for($currentPaper=1;$currentPaper<=$COUNT;$currentPaper++) {

    $quizData = array();
    for($i=1;$i<=$QUIZ_COUNT;$i++) {
        $unitID = $UNIT_DATA["unitSelectq$i"];
        $level = $LEVEL_DATA["levelSelectq$i"];
        array_push($quizData,getRows($unitID,$level,$client,"QUIZ_TABLE",$DB_NAME,$courseCode));
    }

    $sAData = array();
    for($i=1;$i<=$SA_COUNT;$i++) {
        $unitID = $UNIT_DATA["unitSelectsA$i"];
        $level = $LEVEL_DATA["levelSelectsA$i"];
        array_push($sAData,getRows($unitID,$level,$client,"SHORT_QUESTION_TABLE",$DB_NAME,$courseCode));
    }


    $lAData = array();
    for($i=1;$i<=$LA_COUNT;$i++) {
        $unitID = $UNIT_DATA["unitSelectlA$i"];
        $level = $LEVEL_DATA["levelSelectlA$i"];
        array_push($lAData,getRows($unitID,$level,$client,"LONG_QUESTION_TABLE",$DB_NAME,$courseCode));
    }

    $str = html_entity_decode(generatePaper($quizData,$sAData,$lAData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$SA_MARKS,$LA_MARKS,$TOTAL_MARKS,$UNIQUE_UNITS));


    $str = str_replace('\r\n' , ' ', $str);
    $str = str_replace('\n' , ' ', $str);
    $str = str_replace('\r' , ' ', $str);

    //$info = html_entities($info);

    //Save all files to a directory
    file_put_contents("./../../../../temp/$courseCode/paper-$currentPaper.html", $str);

    $str = html_entity_decode(generateAnswer($quizData,$sAData,$lAData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$SA_MARKS,$LA_MARKS,$TOTAL_MARKS,$UNIQUE_UNITS));


    $str = str_replace('\r\n' , ' ', $str);
    $str = str_replace('\n' , ' ', $str);
    $str = str_replace('\r' , ' ', $str);

    //$info = html_entities($info);

    //Save all files to a directory
    file_put_contents("./../../../../temp/$courseCode/answer-$currentPaper.html", $str);
}

/*********** ZIP THE FILES ************/
$dir = "./../../../../temp/$courseCode";
$zip_file = "./../../../../temp/$courseCode.zip";

$rootPath = realpath($dir);

$zip = new ZipArchive();
$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip->close();

delete_directory("./../../../../temp/$courseCode/");
/*********** ZIP THE FILES ***********/

$msg = ['error' => 0];
echo json_encode($msg);

function delete_directory($dir)
{
    if ($handle = opendir($dir))
    {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {

                if(is_dir($dir.$file))
                {
                    if(!@rmdir($dir.$file)) // Empty directory? Remove it
                    {
                        delete_directory($dir.$file.'/'); // Not empty? Delete the files inside it
                    }
                }
                else
                {
                    @unlink($dir.$file);
                }
            }
        }
        closedir($handle);

        @rmdir($dir);
    }

}



function generatePaper($quizData,$sAData,$lAData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$S_MATKS,$L_MARKS,$T_MARKS,$UNIQUE_UNITS) {
    return makePaper($quizData,$sAData,$lAData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$S_MATKS,$L_MARKS,$T_MARKS,$UNIQUE_UNITS);
}
function generateAnswer($quizData,$sAData,$lAData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$S_MATKS,$L_MARKS,$T_MARKS,$UNIQUE_UNITS) {
    return makeAnswer($quizData,$sAData,$lAData,$client,$courseCode,$DB_NAME,$NODE_IP_ARR,$NODE_URL,$Q_MARKS,$S_MATKS,$L_MARKS,$T_MARKS,$UNIQUE_UNITS);
}

?>