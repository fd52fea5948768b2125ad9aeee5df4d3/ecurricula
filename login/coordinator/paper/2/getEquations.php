<?php
session_start();

function makeEquationHTML($equation,$uniqueID) {

    $qEquations = explode('\\\\',$equation);
    $qcount= 0;
    $qHtml = "";
    foreach ($qEquations as $equation) {
        if($equation == '') continue;
        $qHtml = $qHtml."<div class='equationField' id='Q$uniqueID$qcount'>$equation</div>";
        $qcount++;
    }

    return "<div>".$qHtml."</div>";
}
?>