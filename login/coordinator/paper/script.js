$(document).ready(function(){
    $('select').material_select();
    $('#goBtn').click(function () {
        var quizCount = $('#quizCountInput').val();
        var sACount = $('#sACountInput').val();
        var lACount = $('#lACountInput').val();

        if(!quizCount || !sACount || !lACount) {
            window.alert("Enter Valid Data!!!");
            return;
        }

        if(isNaN(quizCount) || isNaN(sACount) || isNaN(lACount)) {
            window.alert("Only Integral values are allowed");
            return;
        }

        window.location.href = "./2?a="+quizCount+"&b="+sACount+"&c="+lACount;
    });
});