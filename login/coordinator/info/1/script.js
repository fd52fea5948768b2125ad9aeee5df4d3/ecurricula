$(document).ready(function() {
    var CLR_TEXT = "The purpose of learning this course is to ";
    $('select').material_select();
    $('#updateBtn').click(function () {
        c = window.confirm("Are you sure, you want to proceed?");
        if(c == false) return;
        var courseName = $('#courseName').val();
        var courseCategoryCode = $('#typeSelect').val();
        var courseCategoryName = $('#typeSelect').find('option:selected').html();
        var lecture = $('#lectureSelect').val();
        var theory = $('#theorySelect').val();
        var practical = $('#practicalSelect').val();
        var totalCredit = $('#totalCreditSelect').val();
        var pre = $('#preRequisiteCourseInput').val();
        var co = $('#coRequisiteCourseInput').val();
        var pro = $('#progressiveCourseInput').val();
        var dept = $('#offeringDepartmentSelect').val();
        var databook = $("#dataBookInput").val();
        var clr = $('#clrInput').val();


        var data = {
            'name': courseName,
            'categoryName': courseCategoryName,
            'categoryCode': courseCategoryCode,
            'l': lecture,
            't': theory,
            'p': practical,
            'c': totalCredit,
            'pre': pre,
            'co': co,
            'pro': pro,
            'dept': dept,
            'databook': databook,
            'clr': clr
        };

        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './1.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Update Successful");
                }
            }
        });
    });



    function makeInitialTextReadOnly(input) {
        var readOnlyLength = CLR_TEXT.length;
        input.addEventListener('keydown', function(event) {
            var which = event.which;
            if (((which == 8) && (input.selectionStart <= readOnlyLength))
                || ((which == 46) && (input.selectionStart < readOnlyLength))) {
                event.preventDefault();
            }
        });
        input.addEventListener('keypress', function(event) {
            var which = event.which;
            if ((event.which != 0) && (input.selectionStart < readOnlyLength)) {
                event.preventDefault();
            }
        });
        input.addEventListener('cut', function(event) {
            if (input.selectionStart < readOnlyLength) {
                event.preventDefault();
            }
        });
        input.addEventListener('paste', function(event) {
            if (input.selectionStart < readOnlyLength) {
                event.preventDefault();
            }
        });
    }

    makeInitialTextReadOnly(document.getElementById("clrInput"));
});