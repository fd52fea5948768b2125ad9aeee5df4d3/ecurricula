<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
$result = Array();
$result['error'] = 0;

function yellError($errorMsg) {
    $result = ['error' => 2,'errorMsg' => $errorMsg];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    yellError("Session Expired");
    exit;
}

function checkCourse($input) {
    if(strpos($input, " ") === false)
    {
        $input = explode(',',$input);
        foreach ($input as $str) {
            if(ctype_alnum($str) === false) {
                yellError("Any Alpha-Numeric letters, comma separated values are allowed");
            }
        }
        return $input;
    }else {
        yellError("Spaces Are Not Allowed");
    }
}

$COURSE_CODE = strtoupper($_SESSION['courseCode']);
$COURSE_NAME = $_POST['name'];
$CATEGORY_NAME = $_POST['categoryName'];
$CATEGORY_CODE = $_POST['categoryCode'];
$L = $_POST['l'];
$T = $_POST['t'];
$P = $_POST['p'];
$C = $_POST['c'];

$PRE = strtoupper(trim($_POST['pre']));
if($PRE != "")
    $PRE = checkCourse($PRE);
$CO = strtoupper(trim($_POST['co']));
if($CO != "")
    $CO = checkCourse($CO);
$PRO = strtoupper(trim($_POST['pro']));
if($PRO != "")
    $PRO = checkCourse($PRO);

$DEPT = $_POST['dept'];
$DATABOOK = $_POST['databook'];
$CLR = $_POST['clr'];

$prefix = "The purpose of learning this course is to ";
$CLR = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $CLR);

$requisite = ['pre'=> $PRE,'co' => $CO];
$courseCategory = ['categoryCode' => $CATEGORY_CODE,'categoryName' => $CATEGORY_NAME, 'l' => $L,'t' => $T,'p' => $P,'c' => $C];
$dbChange =  ['courseName' => $COURSE_NAME,
        'department' => $DEPT,
        'progressiveCourse' => $PRO,
        'requisite' => $requisite,
        'standards' => $DATABOOK,
        'clr' => $CLR,
        'courseCategory' => $courseCategory];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$bulk = new MongoDB\Driver\BulkWrite;
$bulk->update(
    ['_id' => $COURSE_CODE],
    ['$set' => $dbChange],
    ['multi' => false, 'upsert' => false]
);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".COURSE_TABLE", $bulk, $writeConcern);

$result['errorMsg'] = "jifwf";

echo json_encode($result);

?>