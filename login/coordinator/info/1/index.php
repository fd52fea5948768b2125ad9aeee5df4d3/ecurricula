<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$DEPARTMENTS = ["Computer Science and Eng.",'Information Technology','Software Engineering',
                'Civil Engineering','Mechanical engineering', 'Automobile Engineering',
                'Aerospace Engineering','Mechatronics','Electronics and Comm.','Telecommunication',
                'Electrical & Electronics','Electronics and Instru.','Instru. & Ctrl Eng',
                'Chemical Engineering','Biotechnology','Biomedical Engineering','Genetic Engineering',
                'Food Process Engineering','Nanotechnology','Nuclear Engineering','Chemistry'];


$FIXED_CATEGORIES = [['code' => 'BS' , 'name' => 'Basic Sciences'],
    ['code' => 'HS' , 'name' => 'Humanities &amp; Social Science (incl. Arts Courses)'],
    ['code' => 'ES' , 'name' => 'Engineering Sciences'],
    ['code' => 'PC' , 'name' => 'Professional Core'],
    ['code' => 'PE' , 'name' => 'Professional Elective'],
    ['code' => 'OE' , 'name' => 'Open Elective']];

$COURSE_CODE = $_SESSION['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = [];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$departments = $client->executeQuery("$DB_NAME.DEPARTMENT_TABLE",$query);
$departments = $departments->toArray();

$query = ['_id' => $COURSE_CODE];

$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) != 1) {
    displayError("Can't get course data");
}
$BASE_CLR_TEXT = "The purpose of learning this course is to ";
$COURSE_NAME = isset($rows[0]->courseName)?$rows[0]->courseName:"";
$DEPT = isset($rows[0]->department)?$rows[0]->department:"";
$COURSE_CATEGORY = isset($rows[0]->courseCategory->categoryCode)?$rows[0]->courseCategory->categoryCode:"";
$L = isset($rows[0]->courseCategory->l)?$rows[0]->courseCategory->l:"";
$T = isset($rows[0]->courseCategory->t)?$rows[0]->courseCategory->t:"";
$P = isset($rows[0]->courseCategory->p)?$rows[0]->courseCategory->p:"";
$C = isset($rows[0]->courseCategory->c)?$rows[0]->courseCategory->c:"";
$STANDARD = isset($rows[0]->standards)?$rows[0]->standards:"";
$CLR = isset($rows[0]->clr)?$BASE_CLR_TEXT.$rows[0]->clr:$BASE_CLR_TEXT;
$PRE = isset($rows[0]->requisite->pre)?(($rows[0]->requisite->pre=="")?"":join(",",$rows[0]->requisite->pre)):"";
$CO = isset($rows[0]->requisite->co)?(($rows[0]->requisite->co=="")?"":join(",",$rows[0]->requisite->co)):"";
$PRO = isset($rows[0]->progressiveCourse)?(($rows[0]->progressiveCourse=="")?"":join(",",$rows[0]->progressiveCourse)):"";





?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Course Learning Content</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .grey-bar {
            width: 100%;
            height: 2px;
        }

        .breadcrumb:before {
            content: '\25BA';
            color: rgba(255, 255, 255, 0.7);
            vertical-align: top;
            display: inline-block;
            font-weight: normal;
            font-style: normal;
            font-size: 20px;
            margin: 0 10px 0 8px;
            -webkit-font-smoothing: antialiased;
        }

        nav .nav-wrapper {
            padding-left: 15px !important;
        }


    </style>
</head>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
        <br><br><br>
    <nav>
        <div class="nav-wrapper green darken-2">
            <div class="col s12">
                <a class="breadcrumb">CLC</a>
                <!-- <a class="breadcrumb">CLP</a>
                <a class="breadcrumb">Unit 1</a>
                <a class="breadcrumb">Unit 2</a>
                <a class="breadcrumb">Unit 3</a>
                <a class="breadcrumb">Unit 4</a>
                <a class="breadcrumb">Unit 5</a> -->
            </div>
        </div>
    </nav>

    <div class="card">
        <div class="card-content">
            <h5>COURSE LEARNING CONTENT (CLC)</h5>
            <hr>
            <br><br>
            <div class="content-box">
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s12 m4">
                                <input disabled value="<?php echo $COURSE_CODE; ?>" id="courseCode" type="text">
                                <label for="courseCode">Course Code</label>
                            </div>
                            <div class="input-field col s12 m8">
                                <input id="courseName" value="<?php echo $COURSE_NAME; ?>" type="text">
                                <label for="courseName">Course Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m4">
                                <select id="typeSelect">
                                    <?php

                                    foreach ($FIXED_CATEGORIES as $cat) {
                                        $code = $cat['code'];
                                        $name = $cat['name'];
                                        if($COURSE_CATEGORY == $cat['code'])
                                            echo "<option selected value=\"$code\">".$name."</option>";
                                        else
                                            echo "<option value=\"$code\">".$name."</option>";
                                    }

                                    ?>
                                </select>
                                <label>Course Category</label>
                            </div>
                            <div class="input-field col s12 m2">
                                <select id="lectureSelect">
                                    <?php
                                        for($i=0;$i<5;$i++) {
                                            if($i == $L)
                                                echo "<option selected value=\"$i\">$i</option>";
                                            else
                                                echo "<option value=\"$i\">$i</option>";
                                        }
                                    ?>
                                </select>
                                <label>Lecture (L)</label>
                            </div>
                            <div class="input-field col s12 m2">
                                <select id="theorySelect">
                                    <?php
                                    for($i=0;$i<5;$i++) {
                                        if($i == $T)
                                            echo "<option selected value=\"$i\">$i</option>";
                                        else
                                            echo "<option value=\"$i\">$i</option>";
                                    }
                                    ?>
                                </select>
                                <label>Tutorial (T)</label>
                            </div>
                            <div class="input-field col s12 m2">
                                <select id="practicalSelect">
                                    <?php
                                    for($i=0;$i<26;$i++) {
                                        if($i == $P)
                                            echo "<option selected value=\"$i\">$i</option>";
                                        else
                                            echo "<option value=\"$i\">$i</option>";
                                    }
                                    ?>
                                </select>
                                <label>Practical (P)</label>
                            </div>
                            <div class="input-field col s12 m2">
                                <select id="totalCreditSelect">
                                    <?php
                                    for($i=0;$i<26;$i++) {
                                        if($i == $C)
                                            echo "<option selected value=\"$i\">$i</option>";
                                        else
                                            echo "<option value=\"$i\">$i</option>";
                                    }
                                    ?>
                                </select>
                                <label>Credit (C)</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m4">
                                <input value="<?php echo $PRE; ?>" id="preRequisiteCourseInput" type="text">
                                <label for="preRequisiteCourseInput">Pre-Requisite Course</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <input value="<?php echo $CO; ?>" id="coRequisiteCourseInput" type="text">
                                <label for="coRequisiteCourseInput">Co-Requisite Course</label>
                            </div>
                            <div class="input-field col s12 m4">
                                <input value="<?php echo $PRO; ?>" id="progressiveCourseInput" type="text">
                                <label for="progressiveCourseInput">Progressive Course</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6">
                                <select id="offeringDepartmentSelect">
                                    <option value="" disabled>Choose your option</option>
                                    <?php
                                        $COUNT = 0;
                                        foreach ($DEPARTMENTS as $values) {
                                            if($DEPT == $COUNT)
                                                echo "<option value=\"$COUNT\" selected>$values</option>";
                                            else
                                                echo "<option value=\"$COUNT\">$values</option>";
                                            $COUNT++;
                                        }
                                    ?>
                                </select>
                                <label for="offeringDepartmentSelect">Course Offering Department</label>
                            </div>
                            <div class="input-field col s12 m6">
                                <input id="dataBookInput" value="<?php echo $STANDARD; ?>" type="text">
                                <label for="dataBookInput">Data Book/Codes/Standards</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="<?php echo $CLR?>" id="clrInput" type="text">
                        <label for="clrInput">Course Learning Rationale</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <a id="updateBtn" class="btn right red btn-block">Update&nbsp;&nbsp; <i class="fa fa-retweet"></i> </a>
                        <div class="clearfix"> </div>
                    </div>
                </div><div class="grey-bar grey lighten-2 col s12"> </div><br>

                <div class="row">
                    <div class="col s12">
                        <a class="btn disabled"><i class="fa fa-caret-left"></i>&nbsp;&nbsp; Previous </a><span class="hide-on-med-and-up"><br></span>
                        <a href="./../2" class="btn right blue">Next&nbsp;&nbsp; <i class="fa fa-caret-right"></i> </a>
                        <div class="clearfix"> </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
