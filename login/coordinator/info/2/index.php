<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


$MAPPING_DATA = $A_K_MAPPING;

$COURSE_CODE = $_SESSION['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$option = [];
$query = ['_id' => $COURSE_CODE];

$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) != 1) {
    displayError("Can't get course data");
}
$COURSE_NAME = isset($rows[0]->courseName)?$rows[0]->courseName:"";
$CLOS = isset($rows[0]->clo)?$rows[0]->clo:"";


$MAPPING_TEXT = "";

foreach ($MAPPING_DATA as $DATUM) {
    $MTEXT = $DATUM[0];
    $MCODE = $DATUM[1];
    $MAPPING_TEXT = $MAPPING_TEXT."<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Course Learning Plan</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .grey-bar {
            width: 100%;
            height: 2px;
        }

        .grey-bar-small {
            width: 100px;
            height: 2px;
            margin-bottom: 25px;
        }

        .breadcrumb:before {
            content: '\25BA';
            color: rgba(255, 255, 255, 0.7);
            vertical-align: top;
            display: inline-block;
            font-weight: normal;
            font-style: normal;
            font-size: 20px;
            margin: 0 10px 0 8px;
            -webkit-font-smoothing: antialiased;
        }

        nav .nav-wrapper {
            padding-left: 15px !important;
        }

        .no-margin-bottom {
            margin-bottom: 0;
        }

        [type="checkbox"].filled-in:checked+label:after {
            border-color: #7c4dff !important;
            background-color: #7c4dff !important;
        }


    </style>
</head>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
        <br><br><br>
    <nav>
        <div class="nav-wrapper green darken-2">
            <div class="col s12">
                <a href="./../1" class="breadcrumb">CLC</a>
                <a class="breadcrumb">CLP</a>
                <!-- <a class="breadcrumb">Unit 1</a>
                <a class="breadcrumb">Unit 2</a>
                <a class="breadcrumb">Unit 3</a>
                <a class="breadcrumb">Unit 4</a>
                <a class="breadcrumb">Unit 5</a> -->
            </div>
        </div>
    </nav>

    <div class="card">
        <div class="card-content">
            <h5>COURSE LEARNING PLAN (CLP)</h5>
            <hr>
            <br><br>
            <div class="content-box">
                <div class="row no-margin-bottom">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s12 m4">
                                <input disabled value="<?php echo $COURSE_CODE; ?>" id="courseCode" type="text">
                                <label for="courseCode">Course Code</label>
                            </div>
                            <div class="input-field col s12 m8">
                                <input disabled id="courseName" value="<?php echo $COURSE_NAME; ?>" type="text">
                                <label for="courseName">Course Name</label>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                for($i=1;$i<=5;$i++) {
                    if($CLOS != "")
                        $CLO_NAME = isset($CLOS[$i-1]->cloName)?$CLOS[$i-1]->cloName:"";
                    else
                        $CLO_NAME = "";

                    $inputID = "cloNameInput".$i;
                    echo "<br><h5 class='heading'>CLO-$i</h5><div class=\"grey-bar-small blue lighten-2 col s12\"> </div>";
                    echo '<div class="row"><div class="input-field col s12">';
                    echo "<input type='text' value='$CLO_NAME' id='$inputID'><label for='$inputID'>CLO Name</label> ";
                    echo "<h6><b>Programm Learning Outcomes</b></h6><table class=\"centered\">
            <thead>
            <tr>
                $MAPPING_TEXT
            </tr>
            </thead><tbody>
            <tr>";
                    for($j=0;$j<=14;$j++) {
                            if($CLOS != "")
                                $PLOS = isset($CLOS[$i-1]->plo)?explode(",",$CLOS[$i-1]->plo):[];
                            if(isset($PLOS[$j]) && $PLOS[$j] == 1)
                                $CHECKED = true;
                            else
                                $CHECKED = false;

                            $checkBoxID = "checkBox".$i.$j;
                            if($CHECKED)
                                echo "<td><input type=\"checkbox\" class=\"filled-in\" id=\"$checkBoxID\" checked=\"checked\" /><label for=\"$checkBoxID\"></label></td>";
                            else
                                echo "<td><input type=\"checkbox\" class=\"filled-in\" id=\"$checkBoxID\" /><label for=\"$checkBoxID\"></label></td>";
                    }

                    echo '</tr>
            </tbody></table></div></div>';
                }


                ?>

                <div class="row">
                    <div class="col s12">
                        <a id="updateBtn" class="btn right red btn-block">Update&nbsp;&nbsp; <i class="fa fa-retweet"></i> </a>
                        <div class="clearfix"> </div>
                    </div>
                </div><div class="grey-bar grey lighten-2 col s12"> </div><br>

                <div class="row">
                    <div class="col s12">
                        <a href="./../1" class="btn blue"><i class="fa fa-caret-left"></i>&nbsp;&nbsp; Previous </a>
                        <span class="hide-on-med-and-up"><br></span>
                        <a href="./../3" class="btn right blue">Next&nbsp;&nbsp; <i class="fa fa-caret-right"></i> </a>
                        <div class="clearfix"> </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
