$(document).ready(function() {
    $('select').material_select();
    $('#updateBtn').click(function () {
        c = window.confirm("Are you sure, you want to proceed?");
        if(c == false) return;

        var cloNames = {};
        var checkBoxData = {};
        for(var i=1;i<=5;i++) {
            var inputID = "cloNameInput"+i;
            cloNames[inputID] = $("#"+inputID).val();
            console.log($(inputID).val());
            for(var j=0;j<=14;j++) {
                var checkBoxID = ("checkBox" + i)+j;
                checkBoxData[checkBoxID] = $("#"+checkBoxID).is(':checked')===true?1:0;
            }
        }

        var data = {
            'cloNames': cloNames,
            'checkBoxName': checkBoxData
        };

        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './2.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Update Successful");
                }
            }
        });
    });
});