<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
$result = Array();
$result['error'] = 0;

$COURSE_CODE = $_SESSION['courseCode'];

function yellError($errorMsg) {
    $result = ['error' => 2,'errorMsg' => $errorMsg];
    echo json_encode($result);
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    yellError("Session Expired");
    exit;
}

$CLONAMES = $_POST['cloNames'];
$PLO = $_POST['checkBoxName'];

$clo = Array();
for($i=1;$i<=5;$i++) {
    $cloID = $COURSE_CODE."CLO-".$i;
    $name = $CLONAMES["cloNameInput".$i];
    $plo = Array();
    for($j=0;$j<=14;$j++) {
        $plo[$j] = $PLO["checkBox".$i."".$j];
    }
    $plo = join(",",$plo);
    $clo[$i] = ['cloName' => $name, 'plo' => $plo,'cloID' => $cloID];
}


$client = new MongoDB\Driver\Manager($MONGO_URL);
$bulk = new MongoDB\Driver\BulkWrite;
$bulk->update(
    ['_id' => $COURSE_CODE],
    ['$set' => ['clo' => [$clo[1],$clo[2],$clo[3],$clo[4],$clo[5]] ]],
    ['multi' => false, 'upsert' => false]
);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".COURSE_TABLE", $bulk, $writeConcern);

$result['errorMsg'] = "jifwf";

echo json_encode($result);

?>