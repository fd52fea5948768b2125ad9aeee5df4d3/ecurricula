<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
$UNIT_NOS = 1;
function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}
$COURSE_CODE = $_SESSION['courseCode'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
$option = [];
$query = ['_id' => $COURSE_CODE];

$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) != 1) {
    displayError("Error: Multiple Course Present");
}

$COURSE_NAME = isset($rows[0]->courseName)?$rows[0]->courseName:"";
if(!isset($rows[0]->courseCategory)) displayError("CLC Data Missing");
$L = $rows[0]->courseCategory->l;
$T = $rows[0]->courseCategory->t;
$P = $rows[0]->courseCategory->p;

$TOTAL_SESSION_IN_WEEK = $L+$T+$L;


$option = [];
$query = ['_id' => $COURSE_CODE."".$UNIT_NOS];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) != 1) {
    $UNIT_NAME = "";
    $datapresent = false;

    $SRO_NAME = Array();
    $SLO_NAME = Array();


}else {
    $UNIT_NAME = $rows[0]->unitName;


    $regex = new MongoDB\BSON\Regex("$COURSE_CODE".$UNIT_NOS);
    $query = ['_id' => $regex];
    $SESSION_TABLE_NAME = $COURSE_CODE."_SESSION_TABLE";
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery($DB_NAME.".".$SESSION_TABLE_NAME,$query);
    $rows = $rows->toArray();
    $datapresent = false;
    if(sizeof($rows) != 0) {
        $datapresent = true;
    }
    $SRO_NAME = Array();
    $SLO_NAME = Array();

    foreach ($rows as $data) {
        $UNIT_ID = $data->sessionID;
        $SRO_NAME[$UNIT_ID."1"] = $data->sro[0];
        $SRO_NAME[$UNIT_ID."2"] = $data->sro[1];

        $SLO_NAME[$UNIT_ID."1"] = $data->slo[0]->name;
        $SLO_NAME[$UNIT_ID."2"] = $data->slo[1]->name;
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Unit 1 Details</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .grey-bar-small {
            width: 100% !important;
            max-width: 100vw;
            height: 2px;
            margin-bottom: 25px;
        }


        .grey-bar {
            width: 100%;
            height: 2px;
        }

        .breadcrumb:before {
            content: '\25BA';
            color: rgba(255, 255, 255, 0.7);
            vertical-align: top;
            display: inline-block;
            font-weight: normal;
            font-style: normal;
            font-size: 20px;
            margin: 0 10px 0 8px;
            -webkit-font-smoothing: antialiased;
        }

        nav .nav-wrapper {
            padding-left: 15px !important;
        }

        .heading {
            line-height: 35px;
            color: black;
            font-size: 1.2em;
            text-transform: uppercase;
        }


    </style>
</head>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down"><img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"></span>

        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
        <br><br><br>
    <nav>
        <div class="nav-wrapper green darken-2">
            <div class="col s12">
                <a href="./../1" class="breadcrumb">CLC</a>
                <a href="./../2" class="breadcrumb">CLP</a>
                <a class="breadcrumb">Unit 1</a>
                <!-- <a class="breadcrumb">Unit 2</a>
                <a class="breadcrumb">Unit 3</a>
                <a class="breadcrumb">Unit 4</a>
                <a class="breadcrumb">Unit 5</a> -->
            </div>
        </div>
    </nav>

    <div class="card">
        <div class="hide" hidden="hidden">
            <span id="UNIT_NOS"><?php echo $UNIT_NOS; ?></span>
            <span id="L"><?php echo $L; ?></span>
            <span id="T"><?php echo $T; ?></span>
            <span id="P"><?php echo $P; ?></span>
        </div>
        <div class="card-content">
            <h5>Unit <span id="unitID"><?php echo $UNIT_NOS; ?></span></h5>
            <hr>
            <br><br>
            <div class="content-box">
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="input-field col s12 m4">
                                <input disabled value="<?php echo $COURSE_CODE; ?>" id="courseCode" type="text">
                                <label for="courseCode">Course Code</label>
                            </div>
                            <div class="input-field col s12 m8">
                                <input disabled id="courseName" value="<?php echo $COURSE_NAME; ?>" type="text">
                                <label for="courseName">Course Name</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12 m6">
                                <input value="<?php echo $UNIT_NAME; ?>" id="unitName" type="text">
                                <label for="unitName">Unit Name</label>
                            </div><br><br>
                        </div>

                    </div>

                    <?php
                    function generateSessionBlock($sessionID,$TYPE,$weekCount,$unit,$dataPresent,$SLOS,$SROS) {
                        $SRO_ID_1 = "sro".($unit*100 + $sessionID)."Input1";
                        $SRO_ID_2 = "sro".($unit*100 + $sessionID)."Input2";

                        $SLO_ID_1 = "slo".($unit*100 + $sessionID)."Input1";
                        $SLO_ID_2 = "slo".($unit*100 + $sessionID)."Input2";

                        $SRO_1_NAME = $dataPresent?$SROS["".($unit*100 + $sessionID)."1"]:"";
                        $SRO_2_NAME = $dataPresent?$SROS["".($unit*100 + $sessionID)."2"]:"";
                        $SLO_1_NAME = $dataPresent?$SLOS["".($unit*100 + $sessionID)."1"]:"";
                        $SLO_2_NAME = $dataPresent?$SLOS["".($unit*100 + $sessionID)."2"]:"";



                        echo "<div class=\"row\">
                        <div class=\"col s12\">
                            <div class=\"row\">
                                <div class=\"col s12\">
                                    <a class=\"heading\">Session $sessionID </a><div class=\"right\"><div class=\"chip\">Week $weekCount</div> <div class=\"chip\">$TYPE</div></div>
                                    <div class=\"grey-bar-small blue lighten-2 col s12\"></div>
                                    <div class=\"clearfix\"> </div>
                                </div>
                            </div>
                            <h6><b>Session Rationale &amp; Outcomes</b> ( Purpose of learning this session is to )</h6>
                            <div class=\"row\">
                                <div class=\"input-field col s12 m6\">
                                    <input value='$SRO_1_NAME' type=\"text\" id=\"$SRO_ID_1\">
                                    <label for=\"$SRO_ID_1\">SRO 1</label>
                                </div>
                                <div class=\"input-field col s12 m6\">
                                    <input value='$SRO_2_NAME' type=\"text\" id=\"$SRO_ID_2\">
                                    <label for=\"$SRO_ID_2\">SRO 2</label>
                                </div>
                            </div>
                            <h6><b>Session Topic Name</b></h6>
                            <div class=\"row\">
                                <div class=\"input-field col s12 m6\">
                                    <input value='$SLO_1_NAME' type=\"text\" id=\"$SLO_ID_1\">
                                    <label for=\"$SLO_ID_1\">SLO 1 Topic Name</label>
                                </div>
                                <div class=\"input-field col s12 m6\">
                                    <input value='$SLO_2_NAME' type=\"text\" id=\"$SLO_ID_2\">
                                    <label for=\"$SLO_ID_2\">SLO 2 Topic Name</label>
                                </div>
                            </div>
                        </div>
                    </div><br><br><br>";
                    }
                    $effectiveSession = 1;
                    for($week=1;$week<4;$week++) {
                        for($i=0;$i<$L;$i++) {
                            generateSessionBlock($effectiveSession,"Lecture",$week,$UNIT_NOS,$datapresent,$SLO_NAME,$SRO_NAME);
                            $effectiveSession++;
                        }
                        for($i=0;$i<$T;$i++) {
                            generateSessionBlock($effectiveSession,"Theory",$week,$UNIT_NOS,$datapresent,$SLO_NAME,$SRO_NAME);
                            $effectiveSession++;
                        }
                        for($i=0;$i<$P;$i++) {
                            generateSessionBlock($effectiveSession,"Practical",$week,$UNIT_NOS,$datapresent,$SLO_NAME,$SRO_NAME);
                            $effectiveSession++;
                        }
                    }

                    ?>




                </div>

                <div class="row">
                    <div class="col s12">
                        <a id="updateBtn" class="btn right red btn-block">Update&nbsp;&nbsp; <i class="fa fa-retweet"></i> </a>
                        <div class="clearfix"> </div>
                    </div>
                </div><div class="grey-bar grey lighten-2 col s12"> </div><br>

                <div class="row">
                    <div class="col s12">
                        <a href="./../2" class="btn blue"><i class="fa fa-caret-left"></i>&nbsp;&nbsp; Previous </a>
                        <span class="hide-on-med-and-up"><br><br></span>
                        <a href="./../4" class="btn right blue">Next&nbsp;&nbsp; <i class="fa fa-caret-right"></i> </a>
                        <div class="clearfix"> </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
