$(document).ready(function() {
    $('select').material_select();
    $('#updateBtn').click(function () {
        c = window.confirm("Are you sure, you want to proceed?");
        if(c == false) return;

        var unitName = $("#unitName").val();
        var unitNumber = $("#UNIT_NOS").html();
        var l = parseInt($("#L").html());
        var t = parseInt($("#T").html());
        var p = parseInt($("#P").html());
        var sro = {};
        var slo = {};

        var count = 1;
        for(var i=0;i<3;i++) {
            for(var j=0;j<(l+t+p);j++) {
                var sroID = "sro" + (unitNumber * 100 + count) + "Input";
                sro[sroID + 1] = $("#" + sroID + '1').val();
                sro[sroID + 2] = $("#" + sroID + '2').val();
                var sloID = "slo" + (unitNumber * 100 + count) + "Input";
                slo[sloID + 1] = $("#" + sloID + '1').val();
                slo[sloID + 2] = $("#" + sloID + '2').val();
                count++;
            }
        }

        var data = {
            'unitName': unitName,
            'sros': sro,
            'slos': slo,
            'unit': unitNumber
        };

        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './5.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Update Successful");
                }
            }
        });
    });

});