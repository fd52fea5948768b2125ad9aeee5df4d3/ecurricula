<?php
session_start();
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
$client = new MongoDB\Driver\Manager($MONGO_URL);

$result = Array();
$result['error'] = 0;

function yellError($errorMsg) {
    $result = ['error' => 2,'errorMsg' => $errorMsg];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    yellError("Session Expired");
    exit;
}

$COURSE_CODE = $_SESSION['courseCode'];

$UNIT_NAME = $_POST['unitName'];
$UNIT_NOS = $_POST['unit'];
$SROS = $_POST['sros'];
$SLOS = $_POST['slos'];


$option = [];
$query = ['_id' => $COURSE_CODE];

$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
$L = $rows[0]->courseCategory->l;
$T = $rows[0]->courseCategory->t;
$P = $rows[0]->courseCategory->p;


$bulk = new MongoDB\Driver\BulkWrite;
$UNIT_ID = "$COURSE_CODE".$UNIT_NOS;
$bulk->update(
    ['_id' => $UNIT_ID],
    ['$set' => ['courseCode' => $COURSE_CODE,'unitID' => $UNIT_ID, 'unitName' => $UNIT_NAME, "link" => " "]],
    ['multi' => false, 'upsert' => true]
);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".UNITS_TABLE", $bulk, $writeConcern);


$bulk = new MongoDB\Driver\BulkWrite;
$effectiveSession = 1;
for($week=1;$week<4;$week++) {
    for($i=0;$i<($L+$T+$P);$i++) {
        $unitID = (100*$UNIT_NOS + $effectiveSession);
        $id = $COURSE_CODE.($UNIT_NOS*100 + $effectiveSession);
        $slo_id_1 = $id."1";
        $slo_id_2 = $id."2";
        $SRO_1 = $SROS["sro".$unitID."Input1"];
        $SRO_2 = $SROS["sro".$unitID."Input2"];

        $SLO_1 = ['name'=>$SLOS["slo".$unitID."Input1"], 'id'=>($id."1")];
        $SLO_2 = ['name'=>$SLOS["slo".$unitID."Input2"], 'id'=>($id."2")];

        $bulk->update(
            ['_id' => $id],
            ['$set' => ['courseID' => $COURSE_CODE,'sessionID' => $unitID, 'sro' => [$SRO_1,$SRO_2], "slo" => [$SLO_1,$SLO_2] ]],
            ['multi' => false, 'upsert' => true]
        );
        $effectiveSession++;
    }
}
$TABLE_NAME = $COURSE_CODE."_SESSION_TABLE";
$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".".$TABLE_NAME, $bulk, $writeConcern);



echo json_encode($result);

?>