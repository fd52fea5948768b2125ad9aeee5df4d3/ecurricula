<?php
    include_once(__DIR__."/../../../../includes/general.config.php");
    include_once(__DIR__."/../../../../includes/mongo.db.config.php");

    session_start();

    function displayError($str) {
        $errorHTML = "";
        $errorMsg = "$str";
        $BASE_PATH = "../../../..";
        include_once(__DIR__."/../../../../error.php");
        echo $errorHTML;
        exit;
    }


    if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
        displayError("Session Expired!!!");
        exit;
    }

    if(isset($_GET['u'])) {
        $UNIT_ID = $_GET['u'];
        $SESSION_ID = $_GET['s'];
        $INDEX_ID = $_GET['i'];
    }else{
        $UNIT_ID = $_SESSION['unit_id'];
        $SESSION_ID = $_SESSION['session_id'];
        $INDEX_ID = $_SESSION['index_id'];
    }



    $_SESSION['unit_id'] = $UNIT_ID;
    $_SESSION['session_id'] = $SESSION_ID;
    $_SESSION['index_id'] = $INDEX_ID;

    $COURSE_CODE = $_SESSION['courseCode'];
    $SLO_SUB_ID = (intval($UNIT_ID)*100) + intval($SESSION_ID);

    $FINAL_SLO_ID = $COURSE_CODE."".$SLO_SUB_ID."".$INDEX_ID;
    $_SESSION['slo_id'] = $FINAL_SLO_ID;
    $_SESSION['unit_id'] = $UNIT_ID;
    $_SESSION['session_id'] = $SESSION_ID;


    if($INDEX_ID > 2 || $INDEX_ID < 1) {
        displayError("Invalid SLO ID");
        return;
    }


    $_SESSION['SESSION_ID'] = "".$COURSE_CODE."".$SLO_SUB_ID;

    $SESSION_ID_FIXED = $_SESSION['SESSION_ID'];

    $TITLES = ['Recap: Yesterday\'s Topic', 'SLO-1 Content','SLO-1 Practice','SLO-2 Content','SLO-2 Practice','Summarize Today\'s Topic'];

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['_id' => ($COURSE_CODE."".$UNIT_ID)];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);

    $rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) {
        displayError("Unit Details Not Entered");
        return;
    }

    $UNIT_NAME = $rows[0]->unitName;


    $query = ['_id' => $SESSION_ID_FIXED];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.LEARNING_PLAN_TABLE",$query);
    $rows = $rows->toArray();


    $TIMES = [05,10,10,10,10,05];
    $C = [0,0,0,0,0,0];
    $D = [0,0,0,0,0,0];
    $I = [0,0,0,0,0,0];
    $O = [0,0,0,0,0,0];

    if(sizeof($rows) == 1) {
        $TIMES = $rows[0]->time;
        $C = $rows[0]->conceive;
        $D = $rows[0]->design;
        $I = $rows[0]->implement;
        $O = $rows[0]->operate;
    }


    /****SGET SESSION DETAILS ****/
    $query = ['_id' => $SESSION_ID_FIXED];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery($DB_NAME.".".$COURSE_CODE."_SESSION_TABLE",$query);
    $rows = $rows->toArray();
    if(intval($INDEX_ID) % 2 == 0) $SESSION_NAME = $rows[0]->slo[1]->name;
    else $SESSION_NAME = $rows[0]->slo[0]->name;
    /**** END OF GET SESSION ****/
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Learning Plan</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        body {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {
            body {
                padding-left: 0;
            }
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        .heading {
            font-size: 18px;
            font-weight: 500;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"> </img>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Details</a></li>
    <li class="grey lighten-2"><a>Learning Plan</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../2"?>">MCQ</a></li>
    <li><a class="waves-effect" href="<?php echo "../3"?>">Short Question</a></li>
    <li><a class="waves-effect" href="<?php echo "../4"?>">Long Question</a></li>
    <li><a class="waves-effect" href="<?php echo "../5"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6"?>">Learning Practice</a></li>
</ul>


<ul id="slide-out-small" class="hide-on-med-and-up side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Details</a></li>
    <li class="grey lighten-2"><a>Learning Plan</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../2"?>">MCQ</a></li>
    <li><a class="waves-effect" href="<?php echo "../3"?>">Short Question</a></li>
    <li><a class="waves-effect" href="<?php echo "../4"?>">Long Question</a></li>
    <li><a class="waves-effect" href="<?php echo "../5"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6"?>">Learning Practice</a></li>
</ul>

<div class="container">
        <div class="hide-on-small-only">
            <br><br><br>
        </div>
        <div class="card">
            <div class="card-content">
                <h5><i class="fa fa-graduation-cap"></i> Learning Plan - <?php echo "Session $SESSION_ID [$SESSION_NAME]"; ?>
                <span class="right"><div class="chip">Unit <?php echo $UNIT_ID." - ".$UNIT_NAME; ?></div></span><div class="clearfix"> </div>
                </h5>
                <hr>

                <div class="row row-fix">
                    <div class="col s3 m1">
                        <div class="row row-fix">
                            <div class="col s12">
                                <p class="heading center">Time</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s9 m5">
                        <div class="row row-fix">
                            <div class="col s12">
                                <p class="heading center">TITLE</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6">
                        <div class="row row-fix">
                            <div class="col s3">
                                <p class="heading">Conceive</p>
                            </div>
                            <div class="col s3">
                                <p class="heading">Design</p>
                            </div>
                            <div class="col s3">
                                <p class="heading">Implement</p>
                            </div>
                            <div class="col s3">
                                <p class="heading">Operate</p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php

                    for($z=1;$z<=6;$z++) {
                        $str = $TITLES[$z-1];
                        $thisTime = $TIMES[$z-1];

                        $cChecked = "";
                        $dChecked = "";
                        $iChecked = "";
                        $oChecked = "";

                        if($C[$z-1] == 1) $cChecked = "checked";
                        if($D[$z-1] == 1) $dChecked = "checked";
                        if($I[$z-1] == 1) $iChecked = "checked";
                        if($O[$z-1] == 1) $oChecked = "checked";


                        echo "<div class=\"row row-fix\">
                    <div class=\"col s3 m1\">
                        <div class=\"row row-fix\">
                            <div class=\"col s12 input-field\">
                                <input value='$thisTime' class='verifyTime' id=\"timeInput$z\" type=\"text\">
                                <label for=\"timeInput$z\"></label>
                            </div>
                        </div>
                    </div>
                    <div class=\"col s9 m5\">
                        <div class=\"row row-fix\">
                            <div class=\"col s12 input-field\">
                                <input disabled=\"disabled\" value=\"$str\" id=\"textField$z\" type=\"text\">
                                <label for=\"textField$z\"></label>
                            </div>
                        </div>
                    </div>
                      
                    <div class=\"col s12 m6\">
                        <div class=\"row row-fix\">
                            <div class=\"input-field col s3\">
                                <input $cChecked type=\"checkbox\" id=\"conceiveCheckbox1$z\" />
                                <label for=\"conceiveCheckbox1$z\"></label>
                            </div>
                            <div class=\"input-field col s3\">
                                <input $dChecked type=\"checkbox\" id=\"conceiveCheckbox2$z\" />
                                <label for=\"conceiveCheckbox2$z\"></label>
                            </div>
                            <div class=\"input-field col s3\">
                                <input $iChecked type=\"checkbox\" id=\"conceiveCheckbox3$z\" />
                                <label for=\"conceiveCheckbox3$z\"></label>
                            </div>
                            <div class=\"input-field col s3\">
                                <input $oChecked type=\"checkbox\" id=\"conceiveCheckbox4$z\" />
                                <label for=\"conceiveCheckbox4$z\"></label>
                            </div>
                        </div>
                    </div>
                </div>";
                    }


                ?>

                <div class="row">
                    <br>
                    <a id="updateBtn" class="right btn waves-ripple red">Update</a>
                </div>


            </div>
        </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
