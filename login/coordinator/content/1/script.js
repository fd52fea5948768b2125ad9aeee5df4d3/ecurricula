$(document).ready(function () {
    $(".button-collapse").sideNav();
    var updateButton = $('#updateBtn');

    function makeData() {
        var data = {};
        for(var i=1;i<=6;i++) {
            var timeFieldID = "timeInput"+i;
            for(var j=1;j<=4;j++) {
                var checkBoxID = "conceiveCheckbox"+j+"" + i;
                var temp = $("#"+checkBoxID+":checked").val();

                if(temp) {
                    data[checkBoxID] = 1;
                }else {
                    data[checkBoxID] = 0;
                }
            }
            var time = parseInt($("#"+timeFieldID).val());
            if(!time) {
                data[timeFieldID] = 0;
            }else {
                data[timeFieldID] = time;
            }

        }
        return data;
    }

    function verifyButton() {
        var sum = 0;
        for(var i=1;i<=6;i++) {
            var time = parseInt($("#timeInput"+i).val());
            if(!time) {
                displayErrorMessage(1);
                time = 0;
            }
            sum += time;
        }
        if(sum != 50) {
            displayErrorMessage(2);
            return false;
        }
        displayErrorMessage(0);
        return true;
    }

    function displayErrorMessage(e) {
        // 0 -> Clear Error
        // 1 -> Improper time
        // 2 -> Not Equal to 50

        if(e == 0) {
            updateButton.removeClass('disabled');
        }else if(e == 1) {
            updateButton.addClass('disabled');
        }else if(e == 2)  {
            updateButton.addClass('disabled');
        }

    }

    updateButton.click(function () {
        if(verifyButton()) {
            var data = makeData();
            $.ajax({
                'method': 'POST',
                'data': data,
                'url': './1.php',
                'dataType': 'JSON',
                'error': function () {
                    window.alert("Something Wrong!");
                },
                'success': function (phpdata) {
                    if(phpdata['error'] == 1) {
                        window.alert("Error!!!");
                    }if(phpdata['error'] == 2) {
                        window.alert("Error: " + phpdata['errorMsg']);
                    }else {
                        window.alert("Update Successful");
                    }
                }
            });
        }
    });

    $(".verifyTime").on('keyup',function () {
        verifyButton();
    });

    verifyButton();


});