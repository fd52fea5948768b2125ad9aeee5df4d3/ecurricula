<?php
    session_start();

    if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
        die("Session Expired");
    }


include_once(__DIR__."/../../../../includes/general.config.php");
    include_once(__DIR__."/../../../../includes/mongo.db.config.php");

    $SLO_ID = $_SESSION['slo_id'];
    $SESSION_ID = $_SESSION['SESSION_ID'];
    $COURSE_ID = $_SESSION['courseCode'];



    $C =    [$_POST['conceiveCheckbox11'],
            $_POST['conceiveCheckbox12'],
            $_POST['conceiveCheckbox13'],
            $_POST['conceiveCheckbox14'],
            $_POST['conceiveCheckbox15'],
            $_POST['conceiveCheckbox16']];
    $D =    [$_POST['conceiveCheckbox21'],
            $_POST['conceiveCheckbox22'],
            $_POST['conceiveCheckbox23'],
            $_POST['conceiveCheckbox24'],
            $_POST['conceiveCheckbox25'],
            $_POST['conceiveCheckbox26']];
    $I = [$_POST['conceiveCheckbox31'],
        $_POST['conceiveCheckbox32'],
        $_POST['conceiveCheckbox33'],
        $_POST['conceiveCheckbox34'],
        $_POST['conceiveCheckbox35'],
        $_POST['conceiveCheckbox36']];
    $O = [$_POST['conceiveCheckbox41'],
        $_POST['conceiveCheckbox42'],
        $_POST['conceiveCheckbox43'],
        $_POST['conceiveCheckbox44'],
        $_POST['conceiveCheckbox45'],
        $_POST['conceiveCheckbox46']];

    $TIME = [$_POST['timeInput1'],$_POST['timeInput2'],$_POST['timeInput3'],$_POST['timeInput4'],$_POST['timeInput5'],$_POST['timeInput6']];


    $dbChange =  ['sloID' => $SESSION_ID,
        'courseID' => $COURSE_ID,
        'time' => $TIME,'conceive' => $C,
        'design' => $D, 'implement' => $I, 'operate' => $O];

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->update(
        ['_id' => $SESSION_ID],
        ['$set' => $dbChange],
        ['multi' => false, 'upsert' => true]
    );


    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".LEARNING_PLAN_TABLE", $bulk, $writeConcern);

    $result = Array();
    $result['error'] = 0;

    echo json_encode($result);
    return;

?>