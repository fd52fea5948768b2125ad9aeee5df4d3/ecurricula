<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

$QUIZ_ID = $_SESSION['edit_slo_id'];
$SLO_ID = $_SESSION['slo_id'];
$COURSE_ID = $_SESSION['courseCode'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$UNIT_ID = $_SESSION['unit_id'];

$QUESTION_NAME = $_POST['name'];
$ANSWER = $_POST['answer'];
$LEVEL = $_POST['level'];


$A_EQUATION = $_POST['aEquation'];
$Q_EQUATION = $_POST['qEquation'];


$PLO_DATA = $_POST['plo'];
$PLO_DATA = implode(",",$PLO_DATA);

$dbChange = ["_id" => $QUIZ_ID,'sessionID' => $SLO_ID,
    'unitID' => $COURSE_ID."".$UNIT_ID,
    'name' => $QUESTION_NAME,
    'answer' => $ANSWER,
    'plo' => $PLO_DATA,
    'aEquation' => $A_EQUATION,
    'qEquation' => $Q_EQUATION,
    'level' => $LEVEL
];

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $QUIZ_ID],
    ['$set' => $dbChange],
    ['multi' => false, 'upsert' => false]
);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".SHORT_QUESTION_TABLE", $bulkWrite, $writeConcern);

$result = Array();
$result['error'] = 0;
$result['errorMsg'] = json_encode($dbChange);
echo json_encode($result);
return;

?>