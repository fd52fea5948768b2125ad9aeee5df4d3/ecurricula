<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired");
}

include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

$result = [];

$SLO_ID = $_SESSION['slo_id'];
$COURSE_ID = $_SESSION['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['sessionID' => $SLO_ID, 'level' => '1'];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
$result['1'] = sizeof($rows->toArray());

$query = ['sessionID' => $SLO_ID, 'level' => '2'];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
$result['2'] = sizeof($rows->toArray());

$query = ['sessionID' => $SLO_ID, 'level' => '3'];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.SHORT_QUESTION_TABLE",$query);
$result['3'] = sizeof($rows->toArray());


echo json_encode($result);

?>