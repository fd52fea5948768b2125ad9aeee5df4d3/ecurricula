<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/images/list/sa";


session_start();


function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$QUIZ_ID = $_GET['q'];
$SLO_ID =  $_SESSION['slo_id'];
$COURSE_CODE = $_SESSION['courseCode'];


$data = array(
    'sessionID' => $_SESSION['slo_id'],
    'courseID' => $COURSE_CODE,
    'qID' => $QUIZ_ID
);

$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = json_decode(curl_exec($ch));

//echo json_encode($result);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Question Images</title>
    <style>

        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteButton, .editButton {
            cursor: pointer;
        }
    </style>
</head>
<body>


<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-graduation-cap"></i>Question Images</h5><hr>
            <br>

                <div class="slider">
                    <ul class="slides">

                        <?php
                        foreach ($result as $res) {
                            if($res->type == 'answer') continue;
                            $hrefURL = $NODE_IP_ARR."/public?q=".$res->path;
                            echo "<li>
                            <img class=\"materialboxed\" src=\"$hrefURL\">
                        </li>";
                        }
                        ?>


                    </ul>
                </div>

            <h5><i class="fa fa-graduation-cap"></i>Answer Images</h5><hr>
            <br>

            <div class="slider">
                <ul class="slides">

                    <?php
                    foreach ($result as $res) {
                        if($res->type == 'question') continue;
                        $hrefURL = $NODE_IP_ARR."/public?q=".$res->path;
                        echo "<li>
                            <img class=\"materialboxed\" src=\"$hrefURL\">
                        </li>";
                    }
                    ?>


                </ul>
            </div>




            </div>


        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script>
    $(document).ready(function(){
        console.log("Working2");
        $('.slider').slider();
        $('.materialboxed').materialbox();
    });
</script>
</body>
</html>
