$(document).ready(function () {

    $('select').material_select();
    var addButton = $('#updateBtn');

    function initKetex() {
        var text = $("#questionEquationTextAreaInput").val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("questionEquationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationFieldQ"+i+"'></div><br>";
            }
        }
        $('#questionEquationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationFieldQ"+i));
            }catch(e) {
                $('#equationFieldQ'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }

        var text = $("#answerEquationTextAreaInput").val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("answerEquationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationFieldA"+i+"'></div><br>";
            }
        }
        $('#answerEquationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationFieldA"+i));
            }catch(e) {
                $('#equationFieldA'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    }

    initKetex();

    $('#questionEquationTextAreaInput').on('change keyup paste cut',function () {
        var text = $(this).val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("questionEquationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationFieldQ"+i+"'></div><br>";
            }
        }
        $('#questionEquationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationFieldQ"+i));
            }catch(e) {
                $('#equationFieldQ'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    });

    $('#answerEquationTextAreaInput').on('change keyup paste cut',function () {
        var text = $(this).val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("answerEquationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationFieldA"+i+"'></div><br>";
            }
        }
        $('#answerEquationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationFieldA"+i));
            }catch(e) {
                $('#equationFieldA'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    });


    addButton.click(function () {
        var questionName = $("#QuestionTextAreaInput").val();
        if(!questionName) {
            window.alert("Question can't be left blank!!!");
            return;
        }

        var answer = $('#AnswerTextAreaInput').val();
        if(!answer) {
            window.alert("Answer can't be left blank!!!");
            return;
        }

        var level = $("#levelSelect").val();

        var plos = [];
        for(var i=0;i<=14;i++) {
            var checkboxValue = $('#checkBox'+i).prop('checked');
            plos[i] = checkboxValue?1:0;
        }

        var qEquation = $('#questionEquationTextAreaInput').val();
        if(!qEquation) qEquation = "";
        var aEquation = $('#answerEquationTextAreaInput').val();
        if(!aEquation) aEquation = "";



        var data = {
            'name': questionName,
            'answer': answer,
            'level': level,
            'qEquation': qEquation,
            'aEquation': aEquation,
            'plo': plos
        };

        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './3d.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Update Successful");
                }

                window.close();
            }
        });
    });

});