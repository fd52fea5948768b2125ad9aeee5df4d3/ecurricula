$(document).ready(function () {
    $(".button-collapse").sideNav();
    var filePattern =  /\.([0-9a-z]+)(?:[\?#]|$)/i;
    var contentDiv = $('#mainContentDiv');

    function formatSizeUnits(bytes){
        if      (bytes>=1073741824) {bytes=(bytes/1073741824).toFixed(2)+' GB';}
        else if (bytes>=1048576)    {bytes=(bytes/1048576).toFixed(2)+' MB';}
        else if (bytes>=1024)       {bytes=(bytes/1024).toFixed(2)+' KB';}
        else if (bytes>1)           {bytes=bytes+' bytes';}
        else if (bytes==1)          {bytes=bytes+' byte';}
        else                        {bytes='0 byte';}
        return bytes;
    }


    function renderPage() {
        $.ajax({
            url: './6a.php',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if(data['error'] == 404) {
                    window.alert(data['errorMsg']);
                }else {
                    contentDiv.html(data['html']);
                    if(data['type'] == 0) {
                        //upload content page
                        var addButton = $('#updateBtn');

                        addButton.click(function () {
                            var nos = $('#inputField').get(0).files.length;
                            if(nos == 0) {
                                window.alert("Please select a file to upload");
                                return;
                            }else if(nos != 1) {
                                window.alert("Only File Can be Uploaded");
                                return;
                            }
                            var uploadFile = $('#inputField').get(0).files[0];
                            var size = formatSizeUnits(uploadFile.size);

                            var fileName = uploadFile.name;
                            var extension = fileName.match(filePattern);

                            if(!extension) {
                                window.alert("Not a Valid File.");
                                return;
                            }

                            if(uploadFile.size > 3145728) {
                                window.alert("File can't exceed more than 3 MB.")
                                return;
                            }


                            if( !(extension[0] == '.pdf' || extension[0] == '.doc' || extension[0] == '.docx' )) {
                                window.alert("Only PDF and Word Files Can be Uploaded");
                                return;
                            }

                            var bool = window.confirm("Do want to upload " + uploadFile.name + " (" + size + ") ?");

                            if(!bool) return;

                            var formData = new FormData();
                            formData.append('file', uploadFile);
                            $.ajax({
                                url: './6b.php',
                                data: formData,
                                type: 'POST',
                                success: function (e) {
                                    alert('Upload completed');
                                    renderPage();
                                },
                                error: function (e) {
                                    alert('error ' + e.message);
                                },
                                cache: false,
                                contentType: false,
                                processData: false
                            });
                        });

                    }else if(data['type'] == 1) {
                        //download content page

                        $('.deleteBtn').click(function () {
                            deleteFile($(this).attr('data-fileName'));
                        });

                        $('.downloadBtn').click(function () {
                            downloadBtnClicked();
                        });
                    }

                }
            },
            error: function (e) {
                alert('error ' + e.message);
            }
        });
    }

    renderPage();



    function deleteFile(filename) {
        $.ajax({
            url: './6c.php',
            type: 'POST',
            data: {
                fileName: filename
            },
            success: function (data) {
                window.alert("File Deleted!!!");
                renderPage();
            },
            error: function (e) {
                alert('error ' + e.message);
            }
        });
    }

    function downloadBtnClicked() {
        Materialize.toast('Downloading started in background, wait for few seconds to complete the download!!!', 8000);
    }

});