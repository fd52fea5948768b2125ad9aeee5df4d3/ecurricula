<?php
require_once (__DIR__."./../../../../includes/general.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/slo/list/practice";

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


if(!isset($_SESSION['slo_id'])) {
    echo "Session not found";
    exit;
}
$COURSE_CODE = $_SESSION['courseCode'];
$SLO_ID = $_SESSION['slo_id'];

$data = array(
    'sessionID' => $_SESSION['slo_id'],
    'courseID' => $COURSE_CODE
);


$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = json_decode(curl_exec($ch));

//close connection
curl_close($ch);

if(sizeof($result) == 0) {
    $html = "<h5>ADD MATERIAL</h5>
                <div class=\"row row-fix\">
                    <div class=\"col s12\">
                        <div class=\"file-field input-field\">
                            <div class=\"btn red\">
                                <span>ADD</span>
                                <input id=\"inputField\" type=\"file\">
                            </div>
                            <div class=\"file-path-wrapper\">
                                <input class=\"file-path validate\" type=\"text\">
                            </div>
                        </div>
                    </div>
                </div><div class=\"row\">
                    <br>
                    <a id=\"updateBtn\" class=\"blue right btn waves-ripple \">UPLOAD</a>
                </div>";

    $finalReslt = ['error'=> 200, 'html' => $html, 'type' => 0];
    echo json_encode($finalReslt);
    exit;
} else {
    $html = "<h5>AVAILABLE MATERIAL</h5><br>";
    foreach ($result as $d) {
        $data = $d->name;
        $isEmpty = false;
        $path = "data/coordinator/$COURSE_CODE/slc/$SLO_ID"."/".$data;
        $hrefURL = $NODE_IP_ARR."/public?q=".$d->path;
        $html = $html."<div class=\"card\">
                        <div class=\"card-content\">
                            File Name: $data
                        </div>
                        <div class=\"card-action\">
                            <a href='$hrefURL' class=\"blue-text downloadBtn text-darken-1\"><i class=\"fa fa-download\"></i>&nbsp; Download</a>
                            <a data-fileName='$data' class=\"red-text deleteBtn text-darken-1\"><i class=\"fa fa-trash\"></i>&nbsp; Delete</a>
                        </div>
                    </div>";
    }
    $finalReslt = ['error'=> 200, 'html' => $html, 'type' => 1];
    echo json_encode($finalReslt);
    exit;
}

?>

echo