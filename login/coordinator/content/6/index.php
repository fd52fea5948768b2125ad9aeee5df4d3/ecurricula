<?php
    include_once(__DIR__."/../../../../includes/general.config.php");
    include_once(__DIR__."/../../../../includes/mongo.db.config.php");

    session_start();

    function displayError($str) {
        $errorHTML = "";
        $errorMsg = "$str";
        $BASE_PATH = "../../../..";
        include_once(__DIR__."/../../../../error.php");
        echo $errorHTML;
        exit;
    }


    if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
        displayError("Session Expired!!!");
        exit;
    }

    $UNIT_ID = $_SESSION['unit_id'];
    $SESSION_ID = $_SESSION['session_id'];
    $INDEX_ID = $_SESSION['index_id'];

    if(!isset($_SESSION['unit_id'])) {
        displayError("Error: Login Again");
    }

    $COURSE_CODE = $_SESSION['courseCode'];
    $SLO_SUB_ID = (intval($UNIT_ID)*100) + intval($SESSION_ID);

    $FINAL_SLO_ID = $COURSE_CODE."".$SLO_SUB_ID."".$INDEX_ID;
    $_SESSION['slo_id'] = $FINAL_SLO_ID;
    $_SESSION['unit_id'] = $UNIT_ID;
    $_SESSION['session_id'] = $SESSION_ID;


    if($INDEX_ID > 2 || $INDEX_ID < 1) {
        displayError("Invalid SLO ID");
        return;
    }

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['_id' => ($COURSE_CODE."".$UNIT_ID)];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);

    $rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) {
        displayError("Unit Details Not Entered");
        return;
    }

    $UNIT_NAME = $rows[0]->unitName;

    /****SGET SESSION DETAILS ****/
    $SESSION_ID_FIXED = $_SESSION['SESSION_ID'];
    $query = ['_id' => $SESSION_ID_FIXED];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery($DB_NAME.".".$COURSE_CODE."_SESSION_TABLE",$query);
    $rows = $rows->toArray();
    if(intval($INDEX_ID) % 2 == 0) $SESSION_NAME = $rows[0]->slo[1]->name;
    else $SESSION_NAME = $rows[0]->slo[0]->name;
    /**** END OF GET SESSION ****/

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title>Learning Practice</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        body {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {
            body {
                padding-left: 0;
            }
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteBtn, .downloadBtn {
            cursor: pointer;
        }

        .strong {
            font-weight: bold;
            font-family: monospace;
        }
    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"> </img>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Details</a></li>
    <li><a href="<?php echo "../1"?>">Learning Plan</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Content</a></li>
    <li><a href="<?php echo "../2";?>" class="waves-effect">MCQ</a></li>
    <li><a href="<?php echo "../3"?>" class="waves-effect">Short Question</a></li>
    <li><a href="<?php echo "../4"?>" class="waves-effect">Long Question</a></li>
    <li><a href="<?php echo "../5"?>" class="waves-effect">Learning Content</a></li>
    <li class="grey lighten-2"><a class="waves-effect">Learning Practice</a></li>
</ul>


<ul id="slide-out-small" class="hide-on-med-and-up side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Details</a></li>
    <li><a href="<?php echo "../1"?>">Learning Plan</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Content</a></li>
    <li><a href="<?php echo "../2";?>" class="waves-effect">MCQ</a></li>
    <li><a href="<?php echo "../3"?>" class="waves-effect">Short Question</a></li>
    <li><a href="<?php echo "../4"?>" class="waves-effect">Long Question</a></li>
    <li><a href="<?php echo "../5"?>" class="waves-effect">Learning Content</a></li>
    <li class="grey lighten-2"><a class="waves-effect">Learning Practice</a></li>
</ul>

<div class="container">
        <div class="hide-on-small-only">
            <br><br><br>
        </div>
        <div class="card">
            <div class="card-content">
                <h5><i class="fa fa-file"> </i> Learning Practice - <?php echo "Session $SESSION_ID [$SESSION_NAME]"; ?>
                <span class="right"><div class="chip">Unit <?php echo $UNIT_ID." - ".$UNIT_NAME; ?></div></span><div class="clearfix"> </div>
                </h5>
                <hr><br>

                <div id="mainContentDiv">

                </div>

            </div>
        </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
