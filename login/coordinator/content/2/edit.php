<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
session_start();


function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


$MAPPING_DATA = $A_K_MAPPING;

$SLO_ID = $_GET['q'];
$_SESSION['edit_slo_id'] = $SLO_ID;

$COURSE_CODE = $_SESSION['courseCode'];
$UNIT_ID = $_SESSION['unit_id'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $COURSE_CODE];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$courseData = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$courseData = $courseData->toArray();

$query = ['_id' => $SLO_ID];

$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) == 0) {
    displayError("No Such Question Present");
    return;
}
$SELECTED_PLO = $rows[0]->plo;

//echo json_encode($SELECTED_PLO);

$QUESTION_NAME = $rows[0]->name;
$OPTION1 = $rows[0]->options[0];
$OPTION2 = $rows[0]->options[1];
$OPTION3 = $rows[0]->options[2];
$OPTION4 = $rows[0]->options[3];
$CORRECT_OPTION = $rows[0]->correctOption;
$LEVEL = $rows[0]->level;
$EQUATION = $rows[0]->equation;

$CHECKED1 = "";
$CHECKED2 = "";
$CHECKED3 = "";
$CHECKED4 = "";
if($CORRECT_OPTION == 1) $CHECKED1 = "checked";
if($CORRECT_OPTION == 2) $CHECKED2 = "checked";
if($CORRECT_OPTION == 3) $CHECKED3 = "checked";
if($CORRECT_OPTION == 4) $CHECKED4 = "checked";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/katex.min.css">
    <title>Session Rationale & Outcomes</title>
    <style>

        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteButton, .editButton {
            cursor: pointer;
        }
    </style>
</head>
<body>


<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-graduation-cap"></i>EDIT QUESTION</h5>

            <hr>
            <div class="row row-fix">
                <div class="col s12">
                    <div class="row row-fix">
                        <div class="input-field col s12">
                            <textarea id="quizQuestionTextAreaInput" class="materialize-textarea"><?php echo $QUESTION_NAME; ?></textarea>
                            <label for="quizQuestionTextAreaInput">Question</label>
                        </div>
                    </div><br>

                    <div class="row row-fix">
                        <div class="input-field col s12 m7">
                            <textarea id="quizEquationTextAreaInput" class="materialize-textarea"><?php echo $EQUATION; ?></textarea>
                            <label for="quizEquationTextAreaInput">Equations (if any)</label>
                        </div>
                        <div class="input-field col s12 m5 equationInput" id="equationRenderDivQuestion">

                        </div>
                    </div><br>

                    <div class="row row-fix">
                        <div class="col s2 m1 center-fix">
                            <input data-value="1" <?php echo $CHECKED1; ?> name="quizChoiceGroup" type="radio"  id="checkbox1" />
                            <label for="checkbox1"> </label>
                        </div>
                        <div class="input-field col s10 m5">
                            <textarea id="quizOptionInput1" class="materialize-textarea"><?php echo $OPTION1; ?></textarea>
                            <label for="quizOptionInput1">Option 1</label>
                        </div>


                        <div class="col s2 m1 center-fix">
                            <input <?php echo $CHECKED2; ?> data-value="2" name="quizChoiceGroup" type="radio"  id="checkbox2" />
                            <label for="checkbox2"> </label>
                        </div>
                        <div class="input-field col s10 m5 ">
                            <textarea id="quizOptionInput2" class="materialize-textarea"><?php echo $OPTION2; ?></textarea>
                            <label for="quizOptionInput2">Option 2</label>
                        </div>
                    </div>



                    <div class="row row-fix">
                        <div class="col s2 m1 center-fix">
                            <input <?php echo $CHECKED3; ?> data-value="3" name="quizChoiceGroup" type="radio"  id="checkbox3" />
                            <label for="checkbox3"> </label>
                        </div>
                        <div class="input-field col s10 m5">
                            <textarea id="quizOptionInput3" class="materialize-textarea"><?php echo $OPTION3; ?></textarea>
                            <label for="quizOptionInput3">Option 3</label>
                        </div>


                        <div class="col s2 m1 center-fix">
                            <input <?php echo $CHECKED4; ?> data-value="4" name="quizChoiceGroup" type="radio"  id="checkbox4" />
                            <label for="checkbox4"> </label>
                        </div>
                        <div class="input-field col s10 m5">
                            <textarea id="quizOptionInput4" class="materialize-textarea"><?php echo $OPTION4; ?></textarea>
                            <label for="quizOptionInput4">Option 4</label>
                        </div>
                    </div>


                    <br>

                    <div class="row row-fix">
                        <div class="col s12 m6 offset-m3 input-field">
                            <select id="levelSelect">
                                <?php
                                    for($i=1;$i<4;$i++) {
                                        if($i == $LEVEL) {
                                            echo "<option selected value=\"$i\">Level $i</option>";
                                        }else {
                                            echo "<option value=\"$i\">Level $i</option>";
                                        }
                                    }

                                ?>
                            </select>
                            <label for="levelSelect">Select Level</label>
                        </div>

                    </div>

                    <div class="row row-fix">
                        <div class="col s12">
                            <table class="centered">
                                <thead>
                                <tr>

                                    <?php
                                    foreach ($MAPPING_DATA as $DATUM) {
                                        $MTEXT = $DATUM[0];
                                        $MCODE = $DATUM[1];
                                        echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                                    }

                                    ?>
                                </tr>
                                </thead><tbody>

                                <?php
                                $PLOS = explode(",",$courseData[0]->clo[$UNIT_ID-1]->plo);
                                $SELECTED_PLO = explode(",",$SELECTED_PLO);
                                for($j=0;$j<=14;$j++) {
                                    if($PLOS[$j] == 1)
                                        $CHECKED = true;
                                    else
                                        $CHECKED = false;

                                    if($SELECTED_PLO[$j] == 1) {
                                        $SELECTED = "checked";
                                    }else {
                                        $SELECTED = "";
                                    }

                                    $checkBoxID = "checkBox".$j;
                                    if($CHECKED)
                                        echo "<td> <input type=\"checkbox\" class=\"filled-in\" id=\"$checkBoxID\" $SELECTED/><label for=\"$checkBoxID\"></label></td>";
                                    else
                                        echo "<td> <input type=\"checkbox\" disabled class=\"filled-in\" id=\"$checkBoxID\" /><label for=\"$checkBoxID\"></label></td>";
                                }
                                ?>

                                </tbody></table>
                        </div>
                    </div>


                    <div class="row row-fix">
                        <br>
                        <a id="updateBtn" class="right btn waves-ripple red">UPDATE</a>
                    </div>

            </div>





        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
    <script type="text/javascript" src="../../../../static/js/katex.min.js"></script>
<script type="text/javascript" src="edit.js"></script>

</body>
</html>
