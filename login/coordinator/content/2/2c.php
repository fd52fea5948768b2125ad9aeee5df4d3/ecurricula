<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired");
}

include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");


$url = $NODE_URL."/images/delete/quiz";

$COURSE_ID = $_SESSION['courseCode'];

$QUIZ_ID = $_POST['id'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$bulk = new MongoDB\Driver\BulkWrite;

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->delete(['_id' => $QUIZ_ID]);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".QUIZ_TABLE", $bulkWrite, $writeConcern);

$data = array(
    'qID' => $QUIZ_ID,
    'sessionID' => $_SESSION['slo_id'],
    'courseID' => $COURSE_ID,
);

$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = curl_exec($ch);

//close connection
curl_close($ch);


$result = Array();
$result['error'] = 0;



echo json_encode($result);
return;

?>