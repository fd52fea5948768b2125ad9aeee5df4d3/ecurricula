<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired");
}
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

$QUIZ_ID = $_SESSION['edit_slo_id'];
$SLO_ID = $_SESSION['slo_id'];
$COURSE_ID = $_SESSION['courseCode'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$UNIT_ID = $_SESSION['unit_id'];

$QUESTION_NAME = $_POST['name'];
$LEVEL = $_POST['level'];
$OPTIONS = [$_POST['option1'],$_POST['option2'],$_POST['option3'],$_POST['option4']];
$CORRECT_OPTION = $_POST['correctAnswer'];


$PLO_DATA = $_POST['plo'];
$PLO_DATA = implode(",",$PLO_DATA);

$EQUATIONS = $_POST['equation'];

$dbChange = ["_id" => $QUIZ_ID,'sessionID' => $SLO_ID,
    'unitID' => $COURSE_ID."".$UNIT_ID,
    'name' => $QUESTION_NAME,
    'options' => $OPTIONS,
    'level' => $LEVEL,
    'equation' => $EQUATIONS,
    'plo' => $PLO_DATA,
    'correctOption' => $CORRECT_OPTION
];

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(
    ['_id' => $QUIZ_ID],
    ['$set' => $dbChange],
    ['multi' => false, 'upsert' => false]
);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".QUIZ_TABLE", $bulkWrite, $writeConcern);

$result = Array();
$result['error'] = 0;
$result['errorMsg'] = json_encode($dbChange);
echo json_encode($result);
return;

?>