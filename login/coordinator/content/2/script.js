$(document).ready(function () {



    var filePattern =  /\.([0-9a-z]+)(?:[\?#]|$)/i;
    $('select').material_select();
    var PRE_LOADER = '<br><br><br><div class="center"><div class="preloader-wrapper big active">'+
        '    <div class="spinner-layer spinner-blue-only">'+
        '      <div class="circle-clipper left">'+
        '        <div class="circle"></div>'+
        '      </div><div class="gap-patch">'+
        '        <div class="circle"></div>'+
        '      </div><div class="circle-clipper right">'+
        '        <div class="circle"></div>'+
        '      </div>'+
        '    </div>'+
        '  </div></div> ';


    function formatSizeUnits(bytes){
        if      (bytes>=1073741824) {bytes=(bytes/1073741824).toFixed(2)+' GB';}
        else if (bytes>=1048576)    {bytes=(bytes/1048576).toFixed(2)+' MB';}
        else if (bytes>=1024)       {bytes=(bytes/1024).toFixed(2)+' KB';}
        else if (bytes>1)           {bytes=bytes+' bytes';}
        else if (bytes==1)          {bytes=bytes+' byte';}
        else                        {bytes='0 byte';}
        return bytes;
    }

    function initKatex() {
        $('.equationField').each(function() {
            //console.log($(this)[0].id);
            katex.render($(this).html(),document.getElementById($(this)[0].id));
        });
    }


    $(".button-collapse").sideNav();
    var addButton = $('#updateBtn');

    $('#quizEquationTextAreaInput').on('change keyup paste cut',function () {
        var text = $(this).val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("equationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationField"+i+"'></div><br>";
            }
        }
        $('#equationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationField"+i));
            }catch(e) {
                $('#equationField'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    });

    addButton.click(function () {
        var files = [];
        var filestr = "";
        var fileInputIDs = ["fileInput1","fileInput2","fileInput3","fileInput4"];
        try {
            fileInputIDs.forEach(function (fileId) {
                var nos = $('#' + fileId).get(0).files.length;
                if (nos == 0)
                    return;
                else if (nos > 1)
                    throw("Only File Can be Uploaded");

                var uploadFile = $('#' + fileId).get(0).files[0];
                var size = formatSizeUnits(uploadFile.size);

                var fileName = uploadFile.name;
                var extension = fileName.match(filePattern);

                if (!extension)
                    throw("Not a Valid File.");

                if (uploadFile.size > 204800)
                    throw("File can't exceed more than 200KB .");

                if (!(extension[0] == '.jpg' || extension[0] == '.jpeg' || extension[0] == '.png' ))
                    throw ("Only JPEG/JPG and PNG Files Can be Uploaded");

                filestr = filestr + uploadFile.name + " ("+size+") ";

                files.push(uploadFile);
            });
        }catch(e) {
            window.alert(e.toString());
            return;
        }


        var questionName = $("#quizQuestionTextAreaInput").val();
        if(!questionName) {
            window.alert("Question can't be left blank!!!");
            return;
        }
        var option1 = $("#quizOptionInput1").val();
        var option2 = $("#quizOptionInput2").val();
        var option3 = $("#quizOptionInput3").val();
        var option4 = $("#quizOptionInput4").val();
        var level = $("#levelSelect").val();
        var equation = $('#quizEquationTextAreaInput').val();
        if(!equation) equation = "";


        var correctOption = $('input[name=quizChoiceGroup]:checked').attr('data-value');

        if(!option1 || !option2 || !option3 || !option4) {
            window.alert("Options can't be left blank!!!");
            return;
        }


        var formData = new FormData();
        if(files.length > 0) {
            var bool = window.confirm("Do want to upload " + filestr + "?");
            if(bool) {
                var i = 1;
                files.forEach(function (file) {
                    formData.append('file'+i,file);
                    i++;
                });
            }
        }

        var plos = [];
        for(var i=0;i<=14;i++) {
            var checkboxValue = $('#checkBox'+i).prop('checked');
            plos[i] = checkboxValue?1:0;
        }




        formData.append('name',questionName);
        formData.append('option1',option1);
        formData.append('option2',option2);
        formData.append('option3',option3);
        formData.append('equation',equation);
        formData.append('option4',option4);
        formData.append('correctAnswer',correctOption);
        formData.append('level',level);
        formData.append('plo',JSON.stringify(plos));

        $.ajax({
            'method': 'POST',
            'type': 'POST',
            'data': formData,
            'url': './2.php',
            cache: false,
            contentType: false,
            processData: false,
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                window.alert("Question Successfully Added");

                window.location.reload();

                $('.materialize-textarea').trigger('autoresize');

                viewQuiz();
            }
        });
    });


    var resultDiv = $("#availableQuestionDiv");

    function viewCount() {
        $.ajax({
            'method': 'POST',
            'url': './3.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Can't get available Questions");
            },
            'success': function (phpdata) {
                $('.textLevel1').html(phpdata['1']);
                $('.textLevel2').html(phpdata['2']);
                $('.textLevel3').html(phpdata['3']);
            }
        });
    }

    function viewQuiz() {
        resultDiv.html(PRE_LOADER);
        viewCount();

        $.ajax({
            'method': 'POST',
            'url': './2b.php',
            'error': function () {
                window.alert("Can't get available Questions");
            },
            'success': function (phpdata) {
                resultDiv.html(phpdata);
                initializeFunction();
            }
        });
    }

    viewQuiz();

    function popitup(url) {
        var height = window.screen.height / 1.5;
        var width = window.screen.width / 1.5;
        newwindow=window.open(url,'name','height='+height+',width='+width);
        if (window.focus) {newwindow.focus()}

        if (!newwindow.closed) {newwindow.focus()}
        return false;
    }

    function initializeFunction() {
        $('.deleteButton').click(function () {
           var questionId = $(this).attr('data-id');
            $.ajax({
                'method': 'POST',
                'url': './2c.php',
                'data': {
                    'id':questionId
                },
                'error': function () {
                    window.alert("Can't get available Questions");
                },
                'success': function () {
                    window.alert("Question Deleted");

                    initializeFunction();

                    viewQuiz();
                }
            });
        });

        $('.editButton').click(function () {
            var questionId = $(this).attr('data-id');
            popitup("edit.php?q="+questionId);


        });

        $('.imageButton').click(function () {
            var questionId = $(this).attr('data-id');
            popitup("image.php?q="+questionId);
        });

        initKatex();
    }

    $(function() {
        $(window).focus(function() {
            viewQuiz();
        });
    });

});