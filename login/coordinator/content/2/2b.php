<?php
    session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired");
}

    include_once(__DIR__."/../../../../includes/general.config.php");
    include_once(__DIR__."/../../../../includes/mongo.db.config.php");

    $SLO_ID = $_SESSION['slo_id'];
    $COURSE_ID = $_SESSION['courseCode'];

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['sessionID' => $SLO_ID];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) {
        echo "<div class='row'><div class='col s12 center'><p class='center chip'>NO QUESTION AVAILABLE</p> </div><div class='clearfix'> </div> </div>";
    }

    $count = 0;
    foreach ($rows as $data) {
        $count++;
        $questionTitle = nl2br(htmlspecialchars($data->name));
        $option1 = nl2br(htmlspecialchars($data->options[0]));
        $option2 = nl2br(htmlspecialchars($data->options[1]));
        $option3 = nl2br(htmlspecialchars($data->options[2]));
        $option4 = nl2br(htmlspecialchars($data->options[3]));
        $level = $data->level;
        $correctOption = $data->correctOption;
        $id = $data->_id;
        $equations = $data->equation;
        $equations = explode('\\\\',$equations);
        $counte = 0;
        $html = "";
        foreach ($equations as $equation) {
            if($equation == '') continue;
            $html = $html."<div class='equationField' id='$id$counte'>$equation</div>";
            $counte++;
        }
        echo "<div class=\"row\">
                    <div class=\"col s12\">
                        <div class=\"card  questions\">
                            <div class=\"card-content\">
                                <span class=\"card-title\"><a class=\"nos\">$count.</a>$questionTitle</span>
                                <div class='row'>
                                    <div class='col s12'>
                                    
                                    $html
</div>
                                </div><hr>
                                <p>1. $option1</p>
                                <p>2. $option2</p>
                                <p>3. $option3</p>
                                <p>4. $option4</p><br>
                                <p class='right chip'>Correct Answer: $correctOption</p>
                                <p class='chip'>Level: $level</p>
                                <div class='clearfix'> </div>
                            </div>
                            <div class=\"card-action\">
                                <a data-id=\"$id\" class=\"editButton green-text text-darken-1\"><i class=\"fa-edit fa\"> </i> Edit</a>
                                <a data-id=\"$id\" class=\"deleteButton red-text text-darken-2\"><i class=\"fa fa-trash\"> </i> Delete</a>
                                <a data-id=\"$id\" class=\"imageButton cyan-text right text-darken-2\"><i class=\"fa fa-camera\"> </i> Image</a>
                            </div>
                        </div>
                    </div>
                </div>";
    }




?>