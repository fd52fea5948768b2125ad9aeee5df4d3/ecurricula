<?php
session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired");
}

include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");


$url = $NODE_URL."/images/upload/quiz";

$SLO_ID = $_SESSION['slo_id'];
$COURSE_ID = $_SESSION['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['sessionID' => $SLO_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.QUIZ_TABLE",$query);
$rows = $rows->toArray();

$UNIT_ID = $_SESSION['unit_id'];

$query = ['_id' => $COURSE_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$courseData = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$courseData = $courseData->toArray();

$PLO = $courseData[0]->clo[$UNIT_ID-1];

$PLO_DATA = json_decode($_POST['plo']);
$PLO_DATA = implode(",",$PLO_DATA);


if(sizeof($rows) == 0) {
    $QUIZ_ID = $SLO_ID."1";
}else {
    $LAST_VAL = $rows[sizeof($rows)-1]->_id;

    $NEXT_VAL = intval(substr_replace($LAST_VAL,"",0,strlen($SLO_ID)))+1;
    $QUIZ_ID = $SLO_ID."".$NEXT_VAL;
}


$QUESTION_NAME = $_POST['name'];
$OPTIONS = [$_POST['option1'],$_POST['option2'],$_POST['option3'],$_POST['option4']];
$CORRECT_OPTION = $_POST['correctAnswer'];
$LEVEL = $_POST['level'];
$EQUATION = $_POST['equation'];

$bulk = new MongoDB\Driver\BulkWrite;


$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert(["_id" => $QUIZ_ID,'sessionID' => $SLO_ID,
    'unitID' => $COURSE_ID."".$UNIT_ID,
    'col' => $PLO->cloName,
    'plo' => $PLO_DATA,
    'name' => $QUESTION_NAME,
    'options' => $OPTIONS,
    'level' => $LEVEL,
    'equation' => $EQUATION,
    'correctOption' => $CORRECT_OPTION
    ]);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".QUIZ_TABLE", $bulkWrite, $writeConcern);



/*** SEND FILE TO NODE ***/
$data = array(
    'qID' => $QUIZ_ID,
    'sessionID' => $_SESSION['slo_id'],
    'courseID' => $COURSE_ID,
);
$i = 0;
foreach ($_FILES as $FILE) {
    $tmpfile = $FILE['tmp_name'];
    $filename = basename($FILE['name']);

    $data["file".$i] = new CurlFile($FILE['tmp_name'],'file/exgd',$filename);
    $i++;
}



$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = curl_exec($ch);

//close connection
curl_close($ch);
/*** SEND FILE TO NODE ***/

$result = Array();
$result['error'] = 0;
$result['qid'] = $QUIZ_ID;
$result['data'] = $data;
$result['files'] = $_FILES;
$result['post'] = $_POST;
echo json_encode($result);
return;

?>