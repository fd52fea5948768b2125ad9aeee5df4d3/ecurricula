$(document).ready(function () {
    var addButton = $('#updateBtn');


    $('select').material_select();

    function initKetex() {
        var text = $('#quizEquationTextAreaInput').val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("equationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationField"+i+"'></div><br>";
            }
        }
        $('#equationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationField"+i));
            }catch(e) {
                $('#equationField'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    }

    initKetex();

    $('#quizEquationTextAreaInput').on('change keyup paste cut',function () {
        var text = $(this).val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("equationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationField"+i+"'></div><br>";
            }
        }
        $('#equationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationField"+i));
            }catch(e) {
                $('#equationField'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    });

    addButton.click(function () {
        var questionName = $("#quizQuestionTextAreaInput").val();
        if(!questionName) {
            window.alert("Question can't be left blank!!!");
            return;
        }
        var option1 = $("#quizOptionInput1").val();
        var option2 = $("#quizOptionInput2").val();
        var option3 = $("#quizOptionInput3").val();
        var option4 = $("#quizOptionInput4").val();

        var level = $("#levelSelect").val();

        var equation = $('#quizEquationTextAreaInput').val();

        var correctOption = $('input[name=quizChoiceGroup]:checked').attr('data-value');

        if(!option1 || !option2 || !option3 || !option4) {
            window.alert("Options can't be left blank!!!");
            return;
        }

        var plos = [];
        for(var i=0;i<=14;i++) {
            var checkboxValue = $('#checkBox'+i).prop('checked');
            plos[i] = checkboxValue?1:0;
        }

        //console.log(plos);

        var data = {
            'name': questionName,
            'option1': option1,
            'plo': plos,
            'option2': option2,
            'option3': option3,
            'option4': option4,
            'equation': equation,
            'correctAnswer': correctOption,
            'level': level
        };

        $.ajax({
            'method': 'POST',
            'data': data,
            'url': './2d.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Update Successful");
                }

                window.close();
            }
        });
    });

});