<?php
require_once (__DIR__."./../../../../includes/general.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/slo/list/content";

session_start();

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


if(!isset($_SESSION['slo_id'])) {
    echo "Session not found";
    exit;
}
$COURSE_CODE = $_SESSION['courseCode'];
$SLO_ID = $_SESSION['slo_id'];

$data = array(
    'sessionID' => $_SESSION['slo_id'],
    'courseID' => $COURSE_CODE
);

$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = json_decode(curl_exec($ch));

$isEmpty = true;
if($result) {
    foreach ($result as $d) {
        $data = $d->name;
        $isEmpty = false;
        $hrefURL = $NODE_IP_ARR."/public?q=".$d->path;
        echo "<div class=\"card\">
                        <div class=\"card-content\">
                            File Name: $data
                        </div>
                        <div class=\"card-action\">
                            <a href='$hrefURL' class=\"blue-text downloadBtn text-darken-1\"><i class=\"fa fa-download\"></i>&nbsp; Download</a>
                            <a data-fileName='$data' class=\"red-text deleteBtn text-darken-1\"><i class=\"fa fa-trash\"></i>&nbsp; Delete</a>
                        </div>
                    </div>";
    }
}

//close connection
curl_close($ch);

if($isEmpty) {
    echo "<div class='center'><div class='chip'>NO FILE PRESENT</div></div>";
}
?>