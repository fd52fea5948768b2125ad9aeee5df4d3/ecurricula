<?php
require_once (__DIR__."./../../../../includes/general.config.php");
require_once (__DIR__."./../../../../includes/node.mongo.config.php");
$url = $NODE_URL."/slo/delete/content";

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


if(!isset($_SESSION['slo_id'])) {
    echo "Session not found";
    exit;
}
$COURSE_CODE = $_SESSION['courseCode'];
$SLO_ID = $_SESSION['slo_id'];
$FILE_NAME = $_POST['fileName'];

$data = array(
    'sessionID' => $_SESSION['slo_id'],
    'courseID' => $COURSE_CODE,
    'fileName' => $FILE_NAME
);

$ch = curl_init();
curl_setopt($ch,CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$result = json_decode(curl_exec($ch));

echo $result;

//close connection
curl_close($ch);

?>