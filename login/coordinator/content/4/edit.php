<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

session_start();

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}



if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


$MAPPING_DATA = $A_K_MAPPING;

$UNIT_ID = $_SESSION['unit_id'];
$SESSION_ID = $_SESSION['session_id'];
$INDEX_ID = $_SESSION['index_id'];

if(!isset($_SESSION['unit_id'])) {
    displayError("Error: Login Again");
}

$COURSE_CODE = $_SESSION['courseCode'];
$SLO_SUB_ID = (intval($UNIT_ID)*100) + intval($SESSION_ID);

$FINAL_SLO_ID = $COURSE_CODE."".$SLO_SUB_ID."".$INDEX_ID;
$_SESSION['slo_id'] = $FINAL_SLO_ID;
$_SESSION['unit_id'] = $UNIT_ID;
$_SESSION['session_id'] = $SESSION_ID;


if($INDEX_ID > 2 || $INDEX_ID < 1) {
    displayError("Invalid SLO ID");
    return;
}

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $COURSE_CODE];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);
$courseData = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$courseData = $courseData->toArray();

$query = ['_id' => ($COURSE_CODE."".$UNIT_ID)];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    displayError("Unit Details Not Entered");
    return;
}

$UNIT_NAME = $rows[0]->unitName;

$_SESSION['edit_slo_id'] = $_GET['q'];

$query = ['_id' => $_GET['q']];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.LONG_QUESTION_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    displayError("Question Not Present");
    return;
}

$SELECTED_PLO = $rows[0]->plo;
$QUESTION = $rows[0]->name;
$ANSWER = $rows[0]->answer;
$LEVEL = $rows[0]->level;

$A_EQUATION = $rows[0]->aEquation;
$Q_EQUATION = $rows[0]->qEquation;


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/katex.min.css">
    <title>Session Rationale & Outcomes</title>
    <style>
        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteButton, .editButton {
            cursor: pointer;
        }

        .strong {
            font-weight: bold;
            font-family: monospace;
        }
    </style>
</head>
<body>


<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-question-circle"> </i> Short Questions - <?php echo "Session $SESSION_ID"; ?>
                <span class="right"><div class="chip">Unit <?php echo $UNIT_ID." - ".$UNIT_NAME; ?></div></span><div class="clearfix"> </div>
            </h5>
            <hr><br>
            <h5>ADD QUESITION</h5>
            <div class="row row-fix">
                <div class="col s12">
                    <div class="row row-fix">
                        <div class="input-field col s12">
                            <textarea id="QuestionTextAreaInput" class="materialize-textarea"><?php echo $QUESTION;?></textarea>
                            <label for="QuestionTextAreaInput">Question</label>
                        </div>
                    </div><br>

                    <div class="row row-fix">
                        <div class="input-field col s12 m7">
                            <textarea id="questionEquationTextAreaInput" class="materialize-textarea"><?php echo $Q_EQUATION; ?></textarea>
                            <label for="questionEquationTextAreaInput">Equations (if any)</label>
                        </div>
                        <div class="input-field col s12 m5 equationInput" id="questionEquationRenderDivQuestion">

                        </div>
                    </div><br>


                    <div class="row row-fix">
                        <div class="input-field col s12">
                            <textarea id="AnswerTextAreaInput" class="materialize-textarea"><?php echo $ANSWER;?></textarea>
                            <label for="AnswerTextAreaInput">Hints</label>
                        </div>
                    </div><br>

                    <div class="row row-fix">
                        <div class="input-field col s12 m7">
                            <textarea id="answerEquationTextAreaInput" class="materialize-textarea"><?php echo $A_EQUATION; ?></textarea>
                            <label for="answerEquationTextAreaInput">Equations (if any)</label>
                        </div>
                        <div class="input-field col s12 m5 equationInput" id="answerEquationRenderDivQuestion">

                        </div>
                    </div><br>

                    <div class="row row-fix">
                        <div class="col s12 m6 input-field">
                            <select id="levelSelect">
                                <?php
                                for($i=1;$i<4;$i++) {
                                    if($i == $LEVEL) {
                                        echo "<option selected value=\"$i\">Level $i</option>";
                                    }else {
                                        echo "<option value=\"$i\">Level $i</option>";
                                    }
                                }

                                ?>
                            </select>
                            <label for="levelSelect">Select Level</label>
                        </div>
                    </div>

                    <div class="row row-fix">
                        <div class="col s12">
                            <table class="centered">
                                <thead>
                                <tr>

                                    <?php
                                    foreach ($MAPPING_DATA as $DATUM) {
                                        $MTEXT = $DATUM[0];
                                        $MCODE = $DATUM[1];
                                        echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                                    }

                                    ?>
                                </tr>
                                </thead><tbody>

                                <?php
                                $PLOS = explode(",",$courseData[0]->clo[$UNIT_ID-1]->plo);
                                $SELECTED_PLO = explode(",",$SELECTED_PLO);
                                for($j=0;$j<=14;$j++) {
                                    if($PLOS[$j] == 1)
                                        $CHECKED = true;
                                    else
                                        $CHECKED = false;

                                    if($SELECTED_PLO[$j] == 1) {
                                        $SELECTED = "checked";
                                    }else {
                                        $SELECTED = "";
                                    }

                                    $checkBoxID = "checkBox".$j;
                                    if($CHECKED)
                                        echo "<td> <input type=\"checkbox\" class=\"filled-in\" id=\"$checkBoxID\" $SELECTED/><label for=\"$checkBoxID\"></label></td>";
                                    else
                                        echo "<td> <input type=\"checkbox\" disabled class=\"filled-in\" id=\"$checkBoxID\" /><label for=\"$checkBoxID\"></label></td>";
                                }
                                ?>

                                </tbody></table>
                        </div>
                    </div>

                    <div class="row">
                        <br>
                        <a id="updateBtn" class="right btn waves-ripple red">UPDATE</a>
                    </div>

                </div>
            </div>







        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../../../static/js/katex.min.js"></script>
<script type="text/javascript" src="edit.js"></script>

</body>
</html>
