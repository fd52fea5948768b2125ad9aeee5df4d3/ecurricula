<?php
    include_once(__DIR__."/../../../../includes/general.config.php");
    include_once(__DIR__."/../../../../includes/mongo.db.config.php");

    session_start();

    function displayError($str) {
        $errorHTML = "";
        $errorMsg = "$str";
        $BASE_PATH = "../../../..";
        include_once(__DIR__."/../../../../error.php");
        echo $errorHTML;
        exit;
    }


    if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
        displayError("Session Expired!!!");
        exit;
    }

    $MAPPING_DATA = $A_K_MAPPING;

    $UNIT_ID = $_SESSION['unit_id'];
    $SESSION_ID = $_SESSION['session_id'];
    $INDEX_ID = $_SESSION['index_id'];

    if(!isset($_SESSION['unit_id'])) {
        displayError("Error: Login Again");
    }

    $COURSE_CODE = $_SESSION['courseCode'];
    $SLO_SUB_ID = (intval($UNIT_ID)*100) + intval($SESSION_ID);

    $FINAL_SLO_ID = $COURSE_CODE."".$SLO_SUB_ID."".$INDEX_ID;
    $_SESSION['slo_id'] = $FINAL_SLO_ID;
    $_SESSION['unit_id'] = $UNIT_ID;
    $_SESSION['session_id'] = $SESSION_ID;


    if($INDEX_ID > 2 || $INDEX_ID < 1) {
        displayError("Invalid SLO ID");
        return;
    }

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['_id' => ($COURSE_CODE."".$UNIT_ID)];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);

    $rows = $client->executeQuery("$DB_NAME.UNITS_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) {
        displayError("Unit Details Not Entered");
        return;
    }

    $UNIT_NAME = $rows[0]->unitName;

    $query = ['_id' => $COURSE_CODE];
    $query = new MongoDB\Driver\Query($query,$option);
    $courseData = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
    $courseData = $courseData->toArray();

    /****SGET SESSION DETAILS ****/
    $SESSION_ID_FIXED = $_SESSION['SESSION_ID'];
    $query = ['_id' => $SESSION_ID_FIXED];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery($DB_NAME.".".$COURSE_CODE."_SESSION_TABLE",$query);
    $rows = $rows->toArray();
    if(intval($INDEX_ID) % 2 == 0) $SESSION_NAME = $rows[0]->slo[1]->name;
    else $SESSION_NAME = $rows[0]->slo[0]->name;
    /**** END OF GET SESSION ****/

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/katex.min.css">
    <title>Long Question</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        body {
            padding-left: 300px;
        }

        @media only screen and (max-width : 992px) {
            body {
                padding-left: 0;
            }
        }

        .side-nav > li {
            cursor: pointer;
        }

        .burger {
            margin-top: 12px;
        }

        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteButton, .editButton,.imageButton  {
            cursor: pointer;
        }

        .strong {
            font-weight: bold;
            font-family: monospace;
        }

        .no-margin {
            padding: 0 !important;
        }

        .no-margin-bottom {
            margin-bottom: 0 !important;
        }

        .card-heading {
            font-size: 20px;
            padding: 15px 0 15px 0;
            font-weight: 400;
        }

        .big {
            font-size: 52px;
            padding: 23px;
        }
    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="#" class="brand-logo burger left show-on-small hide-on-med-and-up"><i data-activates="slide-out-small"  class="fa button-collapse fa-bars"></i> </a>
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <img id="image" class="brand-logo logo-img s2" src="../../../../static/images/logo.png"> </img>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<ul id="slide-out" class="side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Details</a></li>
    <li><a href="<?php echo "../1"?>">Learning Plan</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Content</a></li>
    <li><a href="<?php echo "../2";?>" class="waves-effect">MCQ</a></li>
    <li><a href="<?php echo "../3"?>" class="waves-effect">Short Question</a></li>
    <li class="grey lighten-2"><a class="waves-effect">Long Question</a></li>
    <li><a class="waves-effect" href="<?php echo "../5"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6"?>">Learning Practice</a></li>
</ul>


<ul id="slide-out-small" class="hide-on-med-and-up side-nav fixed">
    <li>
        <div class="center center-align">
            <br>
            <img class="responsive-img" src="../../../../static/images/icon.128.png">
        </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Details</a></li>
    <li><a href="<?php echo "../1"?>">Learning Plan</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Course Content</a></li>
    <li><a href="<?php echo "../2";?>" class="waves-effect">MCQ</a></li>
    <li><a href="<?php echo "../3"?>" class="waves-effect">Short Question</a></li>
    <li class="grey lighten-2"><a class="waves-effect">Long Question</a></li>
    <li><a class="waves-effect" href="<?php echo "../5"?>">Learning Content</a></li>
    <li><a class="waves-effect" href="<?php echo "../6"?>">Learning Practice</a></li>
</ul>

<div class="container">
        <div class="hide-on-small-only">
            <br><br><br>
        </div>
        <div class="card">
            <div class="card-content">
                <h5><i class="fa fa-paperclip"> </i> Long Questions - <?php echo "Session $SESSION_ID [$SESSION_NAME]"; ?>
                <span class="right"><div class="chip">Unit <?php echo $UNIT_ID." - ".$UNIT_NAME; ?></div></span><div class="clearfix"> </div>
                </h5>
                <hr><br>
                <h5>ADD QUESTION</h5>
                <div class="row row-fix">
                    <div class="col s12">
                        <div class="row row-fix">
                            <div class="input-field col s12">
                                <textarea id="QuestionTextAreaInput" class="materialize-textarea"></textarea>
                                <label for="QuestionTextAreaInput">Question</label>
                            </div>
                        </div><br>

                        <div class="row row-fix">
                            <div class="input-field col s12 m7">
                                <textarea id="questionEquationTextAreaInput" class="materialize-textarea"></textarea>
                                <label for="questionEquationTextAreaInput">Equations (if any)</label>
                            </div>
                            <div class="input-field col s12 m5 equationInput" id="questionEquationRenderDivQuestion">

                            </div>
                        </div><br>




                        <h6>UPLOAD IMAGES (TYPE: PNG/JPEG . LIMIT: 200KB)</h6>


                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="col s12 m6">
                                        <form action="#">
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>File</span>
                                                    <input id="fileInput1" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input  class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col s12 m6">
                                        <form action="#">
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>File</span>
                                                    <input id="fileInput2" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 m6">
                                        <form action="#">
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>File</span>
                                                    <input id="fileInput3" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col s12 m6">
                                        <form action="#">
                                            <div class="file-field input-field">
                                                <div class="btn">
                                                    <span>File</span>
                                                    <input id="fileInput4" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>


                                <h6>OUTCOMES</h6>

                                <div class="row row-fix">
                                    <div class="col s12">
                                        <table class="centered">
                                            <thead>
                                            <tr>
                                                <?php
                                                    foreach ($MAPPING_DATA as $DATUM) {
                                                        $MTEXT = $DATUM[0];
                                                        $MCODE = $DATUM[1];
                                                        echo "<th class='tooltipped'  data-position=\"top\" data-delay=\"50\" data-tooltip=\"$MTEXT\">$MCODE</th>";
                                                    }

                                                ?>
                                            </tr>
                                            </thead><tbody>

                                            <?php
                                            $PLOS = explode(",",$courseData[0]->clo[$UNIT_ID-1]->plo);
                                            for($j=0;$j<=14;$j++) {
                                                if($PLOS[$j] == 1)
                                                    $CHECKED = true;
                                                else
                                                    $CHECKED = false;

                                                $checkBoxID = "checkBox".$j;
                                                if($CHECKED)
                                                    echo "<td> <input type=\"checkbox\" class=\"filled-in\" id=\"$checkBoxID\" /><label for=\"$checkBoxID\"></label></td>";
                                                else
                                                    echo "<td> <input type=\"checkbox\" disabled class=\"filled-in\" id=\"$checkBoxID\" /><label for=\"$checkBoxID\"></label></td>";
                                            }
                                            ?>

                                            </tbody></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <h6>QUESTION DIFFICULTY</h6><br>
                <div class="row row-fix">
                    <div class="col s12 m6 input-field">
                        <select id="levelSelect">
                            <option value="1">Level 1</option>
                            <option value="2">Level 2</option>
                            <option value="3">Level 3</option>
                        </select>
                        <label for="levelSelect">Select Level</label>
                    </div>
                </div>
                <br>
                <h6>Hints</h6>

                <div class="row row-fix">
                    <div class="input-field col s12">
                        <textarea id="AnswerTextAreaInput" class="materialize-textarea"></textarea>
                        <label for="AnswerTextAreaInput">Hints</label>
                    </div>
                </div><br>

                <div class="row row-fix">
                    <div class="input-field col s12 m7">
                        <textarea id="answerEquationTextAreaInput" class="materialize-textarea"></textarea>
                        <label for="answerEquationTextAreaInput">Equations (if any)</label>
                    </div>
                    <div class="input-field col s12 m5 equationInput" id="answerEquationRenderDivQuestion">

                    </div>
                </div><br>

                <h6>UPLOAD IMAGES FOR ANSWERS (TYPE: PNG/JPEG . LIMIT: 200KB)</h6>

                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="col s12 m6">
                                <form action="#">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input id="fileInputAnswer1" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input  class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col s12 m6">
                                <form action="#">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input id="fileInputAnswer2" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6">
                                <form action="#">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input id="fileInputAnswer3" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col s12 m6">
                                <form action="#">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input id="fileInputAnswer4" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <a id="updateBtn" class="right btn waves-ripple red">&nbsp;&nbsp;&nbsp;&nbsp;ADD&nbsp;&nbsp;&nbsp;&nbsp;</a>
                <div class="clearfix"> </div>

            </div></div>

    <div class="row">
        <div class="col s12 m4">
            <div class="card">
                <p class="card-heading green darken-2 white-text no-margin-bottom center-align">Level 1 Questions Count</p>
                <div class="card-content no-margin">
                    <p class="center big textLevel1">0</p>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <p class="card-heading green darken-2 white-text no-margin-bottom center-align">Level 2 Questions Count</p>
                <div class="card-content no-margin">
                    <p class="center big textLevel2">0</p>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <p class="card-heading green darken-2 white-text no-margin-bottom center-align">Level 3 Questions Count</p>
                <div class="card-content no-margin">
                    <p class="center big textLevel3">0</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-content">

                <h5>AVAILABLE QUESTION</h5><br>



                <div id="availableQuestionDiv">

                </div>

            </div>
        </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../../../static/js/katex.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
