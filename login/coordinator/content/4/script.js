$(document).ready(function () {
    var filePattern =  /\.([0-9a-z]+)(?:[\?#]|$)/i;

    $('select').material_select();

    var PRE_LOADER = '<br><br><br><div class="center"><div class="preloader-wrapper big active">'+
        '    <div class="spinner-layer spinner-blue-only">'+
        '      <div class="circle-clipper left">'+
        '        <div class="circle"></div>'+
        '      </div><div class="gap-patch">'+
        '        <div class="circle"></div>'+
        '      </div><div class="circle-clipper right">'+
        '        <div class="circle"></div>'+
        '      </div>'+
        '    </div>'+
        '  </div></div> ';

    function formatSizeUnits(bytes){
        if      (bytes>=1073741824) {bytes=(bytes/1073741824).toFixed(2)+' GB';}
        else if (bytes>=1048576)    {bytes=(bytes/1048576).toFixed(2)+' MB';}
        else if (bytes>=1024)       {bytes=(bytes/1024).toFixed(2)+' KB';}
        else if (bytes>1)           {bytes=bytes+' bytes';}
        else if (bytes==1)          {bytes=bytes+' byte';}
        else                        {bytes='0 byte';}
        return bytes;
    }

    function initKatex() {
        $('.equationField').each(function() {
            //console.log($(this)[0].id);
            katex.render($(this).html(),document.getElementById($(this)[0].id));
        });
    }
    $('#questionEquationTextAreaInput').on('change keyup paste cut',function () {
        var text = $(this).val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("questionEquationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationFieldQ"+i+"'></div><br>";
            }
        }
        $('#questionEquationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationFieldQ"+i));
            }catch(e) {
                $('#equationFieldQ'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    });

    $('#answerEquationTextAreaInput').on('change keyup paste cut',function () {
        var text = $(this).val();
        if(!text) {
            katex.render("Expression Here",document.getElementById("answerEquationRenderDivQuestion"));
        }

        var html = "";
        text = text.split('\\\\');
        for(var i=0;i<text.length;i++) {
            if(text !== '') {
                html += "<div id='equationFieldA"+i+"'></div><br>";
            }
        }
        $('#answerEquationRenderDivQuestion').html(html);
        for(var i=0;i<text.length;i++) {
            try {
                katex.render(text[i],document.getElementById("equationFieldA"+i));
            }catch(e) {
                $('#equationFieldA'+i).html("<p class='red-text'>"+"Syntax Error"+"</p>");
            }
        }
    });


    $(".button-collapse").sideNav();
    var addButton = $('#updateBtn');

    addButton.click(function () {
        var files = [];
        var filestr = "";
        var fileInputIDs = ["fileInput1","fileInput2","fileInput3","fileInput4"];
        try {
            fileInputIDs.forEach(function (fileId) {
                var nos = $('#' + fileId).get(0).files.length;
                if (nos == 0)
                    return;
                else if (nos > 1)
                    throw("Only File Can be Uploaded");

                var uploadFile = $('#' + fileId).get(0).files[0];
                var size = formatSizeUnits(uploadFile.size);

                var fileName = uploadFile.name;
                var extension = fileName.match(filePattern);

                if (!extension)
                    throw("Not a Valid File.");

                if (uploadFile.size > 204800)
                    throw("File can't exceed more than 200KB .");

                if (!(extension[0] == '.jpg' || extension[0] == '.jpeg' || extension[0] == '.png' ))
                    throw ("Only JPEG/JPG and PNG Files Can be Uploaded");

                filestr = filestr + uploadFile.name + " ("+size+") ";

                files.push(uploadFile);
            });
        }catch(e) {
            window.alert(e.toString());
            return;
        }

        var answerFiles = [];
        var fileAnswerIDS = ["fileInputAnswer1","fileInputAnswer2","fileInputAnswer3","fileInputAnswer4"];
        try {
            fileAnswerIDS.forEach(function (fileId) {
                var nos = $('#' + fileId).get(0).files.length;
                if (nos == 0)
                    return;
                else if (nos > 1)
                    throw("Only File Can be Uploaded");

                var uploadFile = $('#' + fileId).get(0).files[0];
                uploadFile.qa = 'a';
                var size = formatSizeUnits(uploadFile.size);

                var fileName = uploadFile.name;
                var extension = fileName.match(filePattern);

                if (!extension)
                    throw("Not a Valid File.");

                if (uploadFile.size > 204800)
                    throw("File can't exceed more than 200KB .");

                if (!(extension[0] == '.jpg' || extension[0] == '.jpeg' || extension[0] == '.png' ))
                    throw ("Only JPEG/JPG and PNG Files Can be Uploaded");

                filestr = filestr + uploadFile.name + " ("+size+") ";

                answerFiles.push(uploadFile);
            });
        }catch(e) {
            window.alert(e.toString());
            return;
        }

        var questionImageCount = files.length;


        var questionName = $("#QuestionTextAreaInput").val();
        if(!questionName) {
            window.alert("Question can't be left blank!!!");
            return;
        }

        var answer = $('#AnswerTextAreaInput').val();
        if(!answer) {
            window.alert("Answer can't be left blank!!!");
            return;
        }


        var level = $("#levelSelect").val();

        var plos = [];
        for(var i=0;i<=14;i++) {
            var checkboxValue = $('#checkBox'+i).prop('checked');
            plos[i] = checkboxValue?1:0;
        }


        var formData = new FormData();
        if(files.length > 0) {
            var i = 1;
            files.forEach(function (file) {
                formData.append('file'+i,file);
                i++;
            });
        }

        if(answerFiles.length > 0) {
            var bool = window.confirm("Do want to upload " + filestr + "?");
            if(bool) {
                var i = 1;
                answerFiles.forEach(function (file) {
                    formData.append('answerFile'+i,file);
                    i++;
                });
            }
        }

        formData.append('name',questionName);
        formData.append('answer',answer);
        formData.append('level',level);
        formData.append('plo',JSON.stringify(plos));

        var qEquation = $('#questionEquationTextAreaInput').val();
        if(!qEquation) qEquation = "";
        var aEquation = $('#answerEquationTextAreaInput').val();
        if(!aEquation) aEquation = "";


        formData.append('split',questionImageCount);
        formData.append('qEquation',qEquation);
        formData.append('aEquation',aEquation);


        $.ajax({
            'method': 'POST',
            'data': formData,
            'url': './4.php',
            'dataType': 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                window.alert("Question Successfully Added");
                console.log(phpdata);
                window.location.reload();

                $('.materialize-textarea').trigger('autoresize');

                viewQuestion();
            }
        });
    });


    var resultDiv = $("#availableQuestionDiv");

    function viewCount() {
        $.ajax({
            'method': 'POST',
            'url': './3.php',
            'dataType': 'JSON',
            'error': function () {
                window.alert("Can't get available Questions");
            },
            'success': function (phpdata) {
                $('.textLevel1').html(phpdata['1']);
                $('.textLevel2').html(phpdata['2']);
                $('.textLevel3').html(phpdata['3']);
            }
        });
    }

    function viewQuestion() {
        resultDiv.html(PRE_LOADER);
        viewCount();

        $.ajax({
            'method': 'POST',
            'url': './4b.php',
            'error': function () {
                window.alert("Can't get available Questions");
            },
            'success': function (phpdata) {
                resultDiv.html(phpdata);
                initializeFunction();
            }
        });
    }

    viewQuestion();

    function popitup(url) {
        var height = window.screen.height / 1.5;
        var width = window.screen.width / 1.5;
        newwindow=window.open(url,'name','height='+height+',width='+width);
        if (window.focus) {newwindow.focus()}

        if (!newwindow.closed) {newwindow.focus()}
        return false;
    }

    function initializeFunction() {
        $('.deleteButton').click(function () {
            var questionId = $(this).attr('data-id');
            $.ajax({
                'method': 'POST',
                'url': './4c.php',
                'data': {
                    'id':questionId
                },
                'error': function () {
                    window.alert("Can't get available Questions");
                },
                'success': function () {
                    window.alert("Question Deleted");

                    initializeFunction();

                    viewQuestion();
                }
            });
        });

        $('.editButton').click(function () {
            var questionId = $(this).attr('data-id');
            popitup("edit.php?q="+questionId);
        });

        $('.imageButton').click(function () {
            var questionId = $(this).attr('data-id');
            popitup("image.php?q="+questionId);
        });

        initKatex();
    }

    $(function() {
        $(window).focus(function() {
            viewQuestion();
        });
    });

});