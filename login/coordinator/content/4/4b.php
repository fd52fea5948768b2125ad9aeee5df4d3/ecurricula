<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}


include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

$SLO_ID = $_SESSION['slo_id'];
$COURSE_ID = $_SESSION['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['sessionID' => $SLO_ID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.LONG_QUESTION_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    echo "<div class='row'><div class='col s12 center'><p class='center chip'>NO QUESTION AVAILABLE</p> </div><div class='clearfix'> </div> </div>";
}

$count = 0;
foreach ($rows as $data) {
    $count++;
    $questionTitle = nl2br(htmlspecialchars($data->name));
    $answer = nl2br(htmlspecialchars($data->answer));
    $id = $data->_id;
    $level = $data->level;

    $A_EQUATION = $data->aEquation;
    $Q_EQUATION = $data->qEquation;

    $qEquations = explode('\\\\',$Q_EQUATION);
    $qcount= 0;
    $qHtml = "";
    foreach ($qEquations as $equation) {
        if($equation == '') continue;
        $qHtml = $qHtml."<div class='equationField' id='Q$id$qcount'>$equation</div>";
        $qcount++;
    }

    $aEquation = explode('\\\\',$A_EQUATION);
    $acount = 0;
    $aHtml = "";
    foreach ($aEquation as $equation) {
        if($equation == '') continue;
        $aHtml = $aHtml."<div class='equationField' id='A$id$acount'>$equation</div>";
        $acount++;
    }


    echo "<div class=\"row\">
                    <div class=\"col s12\">
                        <div class=\"card  questions\">
                            <div class=\"card-content\">
                                <span class=\"card-title\"><a class=\"nos\">$count.</a>$questionTitle</span>
                                <div class='row'>
                                    <div class='col s12'>
                                    
                                    $qHtml
</div>
                                </div>
                                <hr>
                                <p class='chip'>Level $level</p><br><br>
                                <p><span class='strong'>HINTS:  </span>$answer</p>
                                <div class='row'>
                                    <div class='col s12'>
                                    
                                    $aHtml
</div>
                                </div>
                                <div class='clearfix'> </div>
                            </div>
                            <div class=\"card-action\">
                                <a data-id=\"$id\" class=\"editButton green-text text-darken-1\"><i class=\"fa-edit fa\"> </i> Edit</a>
                                <a data-id=\"$id\" class=\"deleteButton red-text text-darken-2\"><i class=\"fa fa-trash\"> </i> Delete</a>
                            <a data-id=\"$id\" class=\"imageButton cyan-text right text-darken-2\"><i class=\"fa fa-camera\"> </i> Image</a>
                            </div>
                        </div>
                    </div>
                </div>";
}




?>