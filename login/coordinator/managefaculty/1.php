<?php
session_start();


include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$finalResult = [];
$SESSION_USERNAME = $_SESSION['username'];
$courseCode = $_SESSION['courseCode'];
$finalResult['error'] = 0;
$facultyID = $_POST['facultyId'];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $facultyID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);
$rows = $rows->toArray();

if(sizeof($rows) != 1) {
    displayError("No such Faculty is Present");
}

if($rows[0]->role != 'F') {
    displayError("Selected User is not a Faculty");
}

$facultyName = $rows[0]->firstName." ".$rows[0]->lastName;

$TABLE_NAME = $courseCode."_COORDINATOR_TABLE";
$query = ['_id' => $facultyID];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
$rows = $rows->toArray();
if(sizeof($rows) != 0) {
    displayError("Faculty Already Present");
}



$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert(["_id" => $facultyID,'facultyID' => $facultyID,
    'facultyName' => $facultyName
]);


$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".$TABLE_NAME", $bulkWrite, $writeConcern);


$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert(["_id" => $facultyID,'facultyID' => $facultyID,
    'facultyName' => $facultyName
]);

$DB_CHANGE = ['_id' => "UNIT_STATUS_$courseCode",
    'courseID' => $courseCode,
    'unitStatus' => [0,0,0,0,0]];

$bulk = new MongoDB\Driver\BulkWrite;
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert($DB_CHANGE);
$FACULY_TABLE_NAME = "FACULTY_".$facultyID;
$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".$FACULY_TABLE_NAME", $bulkWrite, $writeConcern);

echo json_encode($finalResult);
exit;


?>