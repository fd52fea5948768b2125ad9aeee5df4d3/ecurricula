<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
$SESSION_USERNAME = $_SESSION['username'];
$courseCode = $_SESSION['courseCode'];
$COURSE_NAME = $_SESSION['courseName'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Manage Faculty</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title"><?php echo $COURSE_NAME; ?> - Manage Faculty</span>
                <br><h5><i class="fa fa-user"></i> Add Faculty</h5><hr><br>
            <div class="row">
                <div class="col s12">
                    <div class="row">
                        <div class="input-field col s12 m4">
                            <select id="facultySelect">
                                <?php
                                $query = ['role' => 'F'];
                                $option = [];
                                $query = new MongoDB\Driver\Query($query,$option);
                                $rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);
                                $rows = $rows->toArray();

                                foreach ($rows as $data) {
                                    $id = $data->_id;
                                    $name = $id." - ".$data->firstName." ".$data->lastName;
                                    echo "<option value=\"$id\">$name</option>";
                                }
                                ?>
                            </select>
                            <label for="facultySelect">Select Faculty</label>
                        </div>
                        <div class="input-field col s12 m4 center">
                            <a class="btn green" id="addFacultyBtn">Add</a>
                        </div>
                    </div>
                </div>
            </div>
            <h5><i class="fa fa-users"></i> Added Faculty</h5><hr><br>
            <div id="facultyListDiv">

            </div>


            <div>
                <br>
                <a href="#" class="right cyan accent-4 btn waves-effect" id="exportBtn">Export</a>
                <div class="clearfix"> </div>
            </div>

        </div>
    </div>
</div>




<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../../static/js/export.js"></script>
<script type="text/javascript" src="script.js"></script>



<script>
    $('#exportBtn').click(function () {
        var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
        outputFile = outputFile.replace('.csv','') + '.csv';
        exportTableToCSV.apply(this, [$('#exportableTable'), outputFile]);
    });

</script>

</body>
</html>
