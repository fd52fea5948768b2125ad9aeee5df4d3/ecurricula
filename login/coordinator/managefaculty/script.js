$(document).ready(function(){
    $('select').material_select();
    $('#addFacultyBtn').click(function () {
        var facultyID = $('#facultySelect').val();

        if(!facultyID) {
            window.alert("Please Enter a valid Faculty ID");
            return;
        }

        $.ajax({
            'method': 'POST',
            'url': './1.php',
            'data': {
                facultyId: facultyID
            },
            'dataType': 'JSON',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                if(phpdata['error'] == 1) {
                    window.alert("Error!!!");
                }if(phpdata['error'] == 2) {
                    window.alert("Error: " + phpdata['errorMsg']);
                }else {
                    window.alert("Faculty Successfully Added");
                }
                setData();
            }
        });
    });
    
    function setData() {
        $.ajax({
            'method': 'POST',
            'url': './2.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                $('#facultyListDiv').html(phpdata);
            }
        });
    }

    setData();
});