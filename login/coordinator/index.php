<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../..";
    include_once(__DIR__."/../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];

$prefix = 'coordinator_';
$courseCode = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $SESSION_USERNAME);


$_SESSION['courseCode'] = $courseCode;

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $courseCode];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();


$L = $rows[0]->courseCategory->l;
$T = $rows[0]->courseCategory->t;
$P = $rows[0]->courseCategory->p;

$COURSE_NAME = $rows[0]->courseName;
$_SESSION['courseName'] = $COURSE_NAME;

$TOTAL_L_T_P = 3*($L + $T + $P);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/materialize.min.css">
    <title><?php echo $courseCode; ?> Coordinator Home</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .dark {
            width: 1px;
            border-right: 1px solid green;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }
    </style>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="./info/1">Course Information</a></li>
    <li><a href="#sessionSelectModal">Course Content</a></li>
    <li><a href="./managefaculty/">Manage Faculty</a></li>
    <li><a href="./profile/">Profile</a></li>
    <li><a href="./paper/">Question Paper</a></li>
    <li><a href="./details">Student Overall Report</a></li>
    <li><a href="./passwords/">Change Staff or Student Password</a></li>
    <li><a href="./student/">User Details</a></li>
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a class="dropdown-button" data-activates="dropdown1">Options&nbsp;&nbsp; <i class="fa big fa-arrow-circle-down"></i></a></li>
            <li><a  href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Coordinator Home - <?php echo $courseCode; ?></span><br>


            <h5>COURSE STATUS</h5><hr><br>
            <div class="row">
                <div class="col s12 offset-m2 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title white-text indigo center-align sub-heading">Faculty Count</span>
                            <p id="facultyCountP" class="center-align plus-pad">0</p>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title white-text indigo center-align sub-heading">Student Count</span>
                            <p id="studentCountP" class="center-align plus-pad">0</p>
                        </div>
                    </div>
                </div>
            </div>


            <br><h5>STUDENT OVERALL REPORT</h5><hr><br>
            <div class="row">
                <div class="col s12 m6 input-field">
                    <select id="courseSelectDiv">
                        <?php
                        $query = [];
                        $option = [];
                        $TABLE_NAME = "$courseCode"."_COORDINATOR_TABLE";
                        $query = new MongoDB\Driver\Query($query,$option);
                        $rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
                        $rows = $rows->toArray();

                        if($rows) {
                            foreach ($rows as $data) {
                                $facultyID = $data->facultyID;
                                $facultyName = $facultyID." - ".($data->facultyName);
                                echo "<option value='$facultyID'>$facultyName</option>";
                            }
                        }
                        ?>
                    </select>
                    <label for="courseSelectDiv">Select Faculty</label>
                </div>
                <div class="col s12 m6">
                    <br>
                    <a id="goBtn" class="btn grey darken-1 waves-effect">GO</a>
                </div>
            </div>

            <div hidden id="studentStatusDiv">

            </div>


            <div>
                <br>
                <a href="#" class="right cyan accent-4 btn waves-effect" id="exportBtn">Export</a>
                <div class="clearfix"> </div>
            </div>


        </div>
    </div>
</div>

<div id="sessionSelectModal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>MANAGE COURSE CONTENT</h4><hr><br>
        <h4>Session Materials</h4>
        <div class="row">
            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="unitSelectModal">
                            <option value="0" disabled selected>Choose a Unit</option>
                            <option value="1">Unit 1</option>
                            <option value="2">Unit 2</option>
                            <option value="3">Unit 3</option>
                            <option value="4">Unit 4</option>
                            <option value="5">Unit 5</option>
                        </select>
                        <label for="unitSelectModal">Select Unit</label>
                    </div>
                </div>
            </div>


            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="sessionSelectModal2">
                            <option value="0" disabled selected>Choose a Session</option>
                            <?php
                                for($z=1;$z<=$TOTAL_L_T_P;$z++) {
                                    echo "<option value=".$z.">Session $z</option>";
                                }
                            ?>
                        </select>
                        <label for="sessionSelectModal2">Select Session</label>
                    </div>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="sloSelectModal">
                            <option value="1">SLO 1</option>
                            <option value="2">SLO 2</option>
                        </select>
                        <label for="sloSelectModal">Select SLO</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <a id="modalSubmitButton" class="btn right waves-ripple blue">select session</a>
        </div>
        <div class="clearfix">
            <h6 class="red-text center text-darken-2" id="modalErrorText"></h6>
        </div>

        <h4>Unit Materials</h4>
        <div class="row">
            <div class="col s12 m4">
                <div class="row">
                    <div class="input-field col s12">
                        <select id="unitSelectModal2">
                            <option value="0" disabled selected>Choose a Unit</option>
                            <option value="1">Unit 1</option>
                            <option value="2">Unit 2</option>
                            <option value="3">Unit 3</option>
                            <option value="4">Unit 4</option>
                            <option value="5">Unit 5</option>
                        </select>
                        <label for="unitSelectModal2">Select Unit</label>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <a id="modalSubmitButton2" class="btn right waves-ripple blue">select unit</a>
        </div>


        <div class="clearfix">
            <h6 class="red-text center text-darken-2" id="modalErrorText2"></h6>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class=" modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../static/js/export.js"></script>
<script type="text/javascript" src="script.js"></script>


<script>
    $('#exportBtn').click(function () {
        var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
        outputFile = outputFile.replace('.csv','') + '.csv';
        exportTableToCSV.apply(this, [$('#exportableTable'), outputFile]);
    });

</script>

</body>
</html>
