<?php

include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

session_start();

function showError($msg) {
    echo "<h5 class='center-align center red-text'>$msg</h5>";
}

function showSuccess($msg) {
    echo "<h5 class='center-align center green-text'>$msg</h5>";
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    showError("Session Expired!!!");
}

$STD_ID = $_POST['studentID'];

$password = trim($_POST['password']);
$password_retype = trim($_POST['passwordRe']);


$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $STD_ID];
$option = [];
$TABLE_NAME = "USERS_TABLE";
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    showError("Invalid User ID Entered!!!");
    exit;
}


if ($password == null || strlen($password) < 8) {
    showError("Password should be of atleast 8 characters");
    exit;
}
if ($password != $password_retype) {
    showError("Password Mismatch");
    exit;
}

$role = $rows[0]->role;
if(!($role == "F" || $role == "S")) {
    showError("Password Change, Permission Denied");
    exit;
}

$password = md5($password);
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(["_id"=> $STD_ID],['$set' => ["password" => "$password"]],
    ['multi' => false, 'upsert' => true]);

$result = $client->executeBulkWrite("$DB_NAME.USERS_TABLE",$bulkWrite);
showSuccess("Password Successfully Changed for: ".$STD_ID);





?>