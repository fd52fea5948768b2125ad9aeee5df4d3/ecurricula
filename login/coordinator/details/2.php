<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $result = ['error' => 2, 'errorMsg' => $str];
    echo json_encode($result);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    displayError("Session Expired!!!");
}

$finalResult = [];
$SESSION_USERNAME = $_POST['facultyID'];
$COURSE_CODE = $_SESSION['courseCode'];
$client = new MongoDB\Driver\Manager($MONGO_URL);

$query = ['courseCode' => $COURSE_CODE];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
$COURSE_NAME = $rows[0]->courseName;

$regex = new MongoDB\BSON\Regex("");
$query = ['courseID' => $COURSE_CODE, 'studentID' => $regex];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.FACULTY_$SESSION_USERNAME",$query);
$rows = $rows->toArray();


echo "<h5>Session Learning Outcome Report ($COURSE_NAME)</h5><hr><table id='exportableTable' class=\"striped bordered\">
                    <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Student ID</th>
                        <th>Student Name</th>
                        <th class='center'>Unit 1 Completed</th>
                        <th class='center'>Unit 2 Completed</th>
                        <th class='center'>Unit 3 Completed</th>
                        <th class='center'>Unit 4 Completed</th>
                        <th class='center'>Unit 5 Completed</th>
                    </tr>
                    </thead>
                    <tbody>";

$count = 0;
foreach ($rows as $data) {
    $studentID = $data->studentID;
    $studentName = $data->studentName;
    $count++;

    $marks = $data->sloCompleted;

    echo "<tr>
                        <td class='center'>$count. </td>
                        <td>$studentID</td>
                        <td>$studentName</td>";

    for($i=0;$i<5;$i++) {
        echo "<td class='center'>".($marks[$i])." %</td>";
    }
    echo "</tr>";
}



exit;


?>