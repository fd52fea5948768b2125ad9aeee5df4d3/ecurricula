<?php
session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'C') {
    die("Session Expired!!!");
}

include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

$SESSION_USERNAME = $_SESSION['username'];
$COURSE_CODE = $_SESSION['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);


$regex = new MongoDB\BSON\Regex("");
$query = ['facultyID' => $regex];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.".$COURSE_CODE."_COORDINATOR_TABLE",$query);
$rows = $rows->toArray();

$FACULTY_COUNT = sizeof($rows);

$regex = new MongoDB\BSON\Regex("");
$query = ['courseID' => $COURSE_CODE];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.COURSE_REQUEST_TABLE",$query);
$rows = $rows->toArray();

$STUDENT_COUNT = sizeof($rows);


$result = ['f' => $FACULTY_COUNT, 's' => $STUDENT_COUNT];

echo json_encode($result);
exit;
?>