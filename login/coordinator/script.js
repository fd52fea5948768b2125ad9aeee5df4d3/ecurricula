$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    $('select').material_select();

    var studentReportDiv = $('#studentStatusDiv');

    $('#modalSubmitButton').click(function () {
        var session =  $('#sessionSelectModal2').val();
        var errorDiv = $('#modalErrorText');
        var unit =  $('#unitSelectModal').val();
        var slo = $('#sloSelectModal').val();

        if(!unit) {
            errorDiv.text("Please Select a Unit");
            return;
        }

        if(!session) {
            errorDiv.text("Please Select a Session");
            return;
        }


        window.location.href = "./content/1?u="+unit+"&i="+slo+"&s="+session;

    });

    $('#modalSubmitButton2').click(function () {
        var unit =  $('#unitSelectModal2').val();

        var errorDiv = $('#modalErrorText2');

        if(!unit) {
            errorDiv.text("Please Select a Unit");
            return;
        }


        window.location.href = "./unit?u="+unit;

    });

    function initiate() {
        $.ajax({
            'method': 'POST',
            'url': './1.php',
            'dataType': 'JSON',
            'success': function (phpdata) {
                $('#studentCountP').html(phpdata['s']);
                $('#facultyCountP').html(phpdata['f']);
            }
        });
    }

    initiate();

    $('#goBtn').click(function () {
        var facultyID = $('#courseSelectDiv').val();

        $.ajax({
            'method': 'POST',
            'data': {
                'facultyID': facultyID
            },
            'url': './2.php',
            'success': function (phpdata) {
                studentReportDiv.show();
                studentReportDiv.html(phpdata);
            }
        });
    });
});