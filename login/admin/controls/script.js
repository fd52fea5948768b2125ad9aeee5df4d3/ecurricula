$(document).ready(function () {
    var studentSwitch = $('#studentRegisterControlSwitch');
    var facultySwitch = $('#facultyRegisterControlSwitch');

    $('.controls').on('change',function () {
        var id = $(this).attr('data-name');
        var state = $(this).prop('checked')?1:0;

        $.ajax({
            'method': 'POST',
            'url': './1.php',
            'data': {
                'id': id,
                'value': state
            },
            'dataType': 'JSON',
            'success': function (phpdata) {
                console.log(phpdata);
                if(phpdata['error'] == 200) {
                    var text = (state==1)?"Enabled":"Disabled";
                    $('#'+id+"_TEXT").html(text);
                }
                else
                    window.alert("Something Wrong" + phpdata['errorMsg']);
            }
        });
    });
});