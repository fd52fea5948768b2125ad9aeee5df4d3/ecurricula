<?php
    session_start();
    if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
        die("Admin Login Required");
    }

    include_once(__DIR__."/../../../includes/general.config.php");
    include_once(__DIR__."/../../../includes/mongo.db.config.php");

    $SESSION_USERNAME = $_SESSION['username'];
    $client = new MongoDB\Driver\Manager($MONGO_URL);

    $ID = $_POST['id'];
    $STATE = $_POST['value'];

    if(intval($STATE) != 0 && intval($STATE) != 1) exit;


    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->update(
        ['_id' => $ID],
        ['$set' => ['value' => $STATE]],
        ['multi' => false, 'upsert' => false]
    );


    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME.".CONTROL_TABLE", $bulkWrite, $writeConcern);

    $result = Array();
    $result['error'] = 200;
    $result['errorMsg'] = "Admin";
    echo json_encode($result);
?>