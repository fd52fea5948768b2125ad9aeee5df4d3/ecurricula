<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = [];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.CONTROL_TABLE",$query);
$rows = $rows->toArray();

$VALUES = Array();
$STATIC_TEST = Array();
foreach ($rows as $data) {
    $VALUES[$data->_id] = ($data->value==1)?"checked":"";
    $STATIC_TEST[$data->_id] = ($data->value==1)?"Enabled":"Disabled";
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Admin Controls</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .title {
            font-weight: 400;
            font-size: 1.3em;
            line-height: 28px;
        }

        .collection-item {
            padding: 18px !important;
        }

    </style>
</head>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Admin Controls</span>
            <br>
            <ul class="collection">
                <li class="collection-item avatar">
                    <span class="title">Student Registration</span>
                    <p id="STUDENT_REGISTER_TEXT" class="grey-text lighten-2"><?php echo $STATIC_TEST['STUDENT_REGISTER'];?></p>
                    <a class="secondary-content">
                        <br>
                        <div class="switch">
                            <label>
                                Off
                                <input <?php echo $VALUES['STUDENT_REGISTER'];?> data-name="STUDENT_REGISTER" class="controls" type="checkbox">
                                <span class="lever"></span>
                                On
                            </label>
                        </div>
                    </a>
                </li>

                <li class="collection-item avatar">
                    <span class="title">Faculty Registration</span>
                    <p id="FACULTY_REGISTER_TEXT" class="grey-text lighten-2"><?php echo $STATIC_TEST['FACULTY_REGISTER'];?></p>
                    <a class="secondary-content">
                        <br>
                        <div class="switch">
                            <label>
                                Off
                                <input <?php echo $VALUES['FACULTY_REGISTER'];?> data-name="FACULTY_REGISTER" class="controls" type="checkbox">
                                <span class="lever"></span>
                                On
                            </label>
                        </div>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
