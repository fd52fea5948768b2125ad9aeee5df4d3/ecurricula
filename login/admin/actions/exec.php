<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$CMD = $_POST['cmd'];
$CMD = explode("\n",$CMD);
foreach ($CMD as $cmd) {
    echo "<pre class='red-text text-darken-1'>Executing Code: $cmd</pre>";
    $out = shell_exec($cmd);
    echo "<pre>" . $out . "</pre>";
}

?>