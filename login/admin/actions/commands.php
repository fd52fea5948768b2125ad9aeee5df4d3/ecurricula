<?php
$today = date("m-d-y"); 
$backupName = "ecurricula-".$today."-backup";

    $COMMANDS = [
        ['name' => 'List','code' => 'ls -a 2>&1', 'class' => 'red'],
        ['name' => 'PWD','code' => 'pwd 2>&1', 'class' => 'indigo'],
        ['name' => 'DB Backup','code' => "mongodump --host localhost --port 27017 --username ulcuser --password ulcuserdbpassword --out /home/bitnami/backup/$backupName 2>&1", 'class' => 'green'],
	   ['name' => 'DB Restore', 'code' => "mongorestore --host localhost --port 27017 --db ECURRICULA_DB --username ulcuser --password ulcuserdbpassword /home/bitnami/backup/ecurricula-dd-mm-yy-backup/ECURRICULA_DB 2>&1", 'class' => 'blue']

    ]

?>