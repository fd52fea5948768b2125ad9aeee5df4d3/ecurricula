$(document).ready(function () {
    $('.clckBtn').click(function () {
        var code = $(this).attr('data-cmd');

        $('#executionCodeArea').val(code);
        $('#executionCodeArea').trigger('autoresize');

    });


    $('#executeBtn').click(function () {
        var c = window.confirm("Are you sure to continue?");
        if(!c) return;
        $.ajax({
            'method': 'POST',
            'data': {
                'cmd': $('#executionCodeArea').val()
            },
            'url': 'exec.php',
            'success': function (res) {
                $('#output').html(res);
            }
        });


    });
});