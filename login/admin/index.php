<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../";
    include_once(__DIR__."/../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = [];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.COURSE_TABLE",$query);
$rows = $rows->toArray();
$COURSE_COUNT = sizeof($rows);

$query = ['role' => 'S'];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);
$rows = $rows->toArray();
$STUDENT_COUNT = sizeof($rows);


$query = ['role' => 'F'];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);
$rows = $rows->toArray();
$FACULTY_COUNT = sizeof($rows);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/materialize.min.css">
    <title>Admin Home</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }

    </style>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="./courses/">Courses</a></li>
    <li><a href="./controls/">Controls</a></li>
    <li><a href="./d3/">Curricula</a></li>
    <li><a href="./actions/">Actions</a></li>
    <li><a href="./profile/">Profile</a></li>
    <li><a href="./details">Student Overall Report</a></li>
    <li><a href="./depts/">Department</a></li>
    <li><a href="./passwords">Change User's Password</a> </li>
    <li><a href="./student/">Users Details</a></li>
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down"><img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png"></span>

        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a class="dropdown-button" data-activates="dropdown1">Options&nbsp;&nbsp; <i class="fa big fa-arrow-circle-down"></i></a></li>
            <li><a href="./../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Admin Home</span><br>


            <h5>COURSES STATUS</h5><hr><br>
            <div class="row">
                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title white-text indigo center-align sub-heading">Course Count</span>
                            <p class="center-align plus-pad"><?php echo $COURSE_COUNT; ?></p>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title white-text indigo center-align sub-heading">Faculty Count</span>
                            <p class="center-align plus-pad"><?php echo $FACULTY_COUNT; ?></p>
                        </div>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title white-text indigo center-align sub-heading">Student Count</span>
                            <p class="center-align plus-pad"><?php echo $STUDENT_COUNT; ?></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
