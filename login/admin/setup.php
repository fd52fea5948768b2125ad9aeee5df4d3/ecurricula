<?php
    //die("404 Not Found");
    $accessCode = "ulcSetup321";
    if($_GET['q'] != $accessCode) {
        die("Incorrect Details");
    }

    include_once(__DIR__."/../../includes/general.config.php");
    include_once(__DIR__."/../../includes/mongo.db.config.php");

    //Setup Connection to MongoDB
    $client = new MongoDB\Driver\Manager($MONGO_URL);

    //Create Users Table
    $COLLECTION_NAME = "USERS_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Insert Admin to Users Table
    $password = md5("ulcAdmin4321");
    $password2 = md5("password");

    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->insert(["_id"=> "admin","department" => "ULC","dob" => "2017-03-15",
        "email" => "admin@srmulc.com",
        "firstName" => "eCurricula",
        "lastName" => "admin",
        "mobile" => "0000000000",
        "password" => "$password",
        "role" => "A",
        "username" => "admin"]);

    $bulkWrite->insert(["_id"=> "director","department" => "ET","dob" => "2017-03-15",
        "email" => "admin@srmulc.com",
        "firstName" => "eCurricula",
        "lastName" => "Director",
        "mobile" => "0000000000",
        "password" => "$password2",
        "role" => "O",
        "username" => "director"]);

$bulkWrite->insert(["_id"=> "hod","department" => "ET","dob" => "2017-03-15",
    "email" => "admin@srmulc.com",
    "firstName" => "eCurricula",
    "lastName" => "HOD",
    "mobile" => "0000000000",
    "password" => "$password2",
    "role" => "O",
    "username" => "hod"]);

    $result = $client->executeBulkWrite("$DB_NAME.USERS_TABLE",$bulkWrite);

    //Create Course Table
    $COLLECTION_NAME = "COURSE_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);


    //Create Course Request Table
    $COLLECTION_NAME = "COURSE_REQUEST_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);


    //Create Course Request Table
    $COLLECTION_NAME = "CONTROL_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Insert Controls to Control Table
    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->insert(["_id"=> "STUDENT_REGISTER","value" => 0]);
    $bulkWrite->insert(["_id"=> "FACULTY_REGISTER","value" => 0]);

    $result = $client->executeBulkWrite("$DB_NAME.CONTROL_TABLE",$bulkWrite);


    //Create Learning Plan Table
    $COLLECTION_NAME = "LEARNING_PLAN_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Create Long Question Table
    $COLLECTION_NAME = "LONG_QUESTION_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Create Practice Request Table
    $COLLECTION_NAME = "PRACTICE_REQUEST_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);


    //Create Quiz Table
    $COLLECTION_NAME = "QUIZ_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);


    $COLLECTION_NAME = "NOTICE_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);
//
//    //Create Session Table
//    $COLLECTION_NAME = "SESSION_TABLE";
//    $cmd['create'] = $COLLECTION_NAME;
//    $newCollection = new MongoDB\Driver\Command($cmd);
//    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Create Short Question Request Table
    $COLLECTION_NAME = "SHORT_QUESTION_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Create Units Table
    $COLLECTION_NAME = "UNITS_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

    //Create Unit Question Table
    $COLLECTION_NAME = "UNIT_QUESTIONS_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);


    //Create D3 Course Table Table
    $COLLECTION_NAME = "D3_COURSE_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);


    //Create Department Table
    $COLLECTION_NAME = "DEPARTMENT_TABLE";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);



    //Create D3 FUTURE PROSPECTS Table
    $COLLECTION_NAME = "D3_FUTURE_PROSPECTS";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);

?>
