$(document).ready(function () {
    var courseNameField = $("#courseNameInput");
    var courseCodeField = $("#courseCodeInput");
    var courseCodeBtn = $("#courseAddBtn");
    $("#resultBox").hide();

    courseCodeBtn.click(function () {
        var code = courseCodeField.val();
        var name = courseNameField.val();

        if(code == "" || name == "") {
            window.alert("Please Enter Valid Department Details");
            return;
        }

        $.ajax({
            'method': 'POST',
            'data': {
                'name': name,
                'code': code
            },
            'url': './course.module.php',
            'dataType': 'JSON',
            'error': function (res) {
                window.alert("Something Contact Admin");
            },
            'success': function (res) {
                if (res.error == '200') {
                    $("#resultBox").show();
                    $("#courseCodePlace").text(code);
                    $("#coursePasswordPlace").text(res.password);
                } else {
                    window.alert("Error: " + res.errorMsg);
                }
            }
        });
    });

    $("#pwdDownload").click(function () {
        var code = $("#courseCodePlace").html();
        var pwd = $("#coursePasswordPlace").html();
        window.open("./pwd.php?c="+code+"&p="+pwd);
    });
});