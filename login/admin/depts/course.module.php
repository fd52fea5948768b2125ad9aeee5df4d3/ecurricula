<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");
$client = new MongoDB\Driver\Manager($MONGO_URL);

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    $res = ['error' => '404', 'errorMsg' => "Login Again"];
    exit;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

$PASSWORD = randomPassword();
$PASSWORD_MD5 = md5($PASSWORD);
$SESSION_USERNAME = $_SESSION['username'];
$COURSE_NAME = $_POST['name'];
$COURSE_CODE = strtoupper($_POST['code']);

$query = ['_id' => "department_$COURSE_CODE"];
$query = new MongoDB\Driver\Query($query,[]);
$rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);
$rows = $rows->toArray();
if(sizeof($rows) != 0) {
    $res = ['error' => '404', 'errorMsg' => "Department Already Exist"];
    echo json_encode($res);
    exit;
}

$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert(["_id" => "$COURSE_CODE","departmentCode" => "$COURSE_CODE",
    "departmentName" => "$COURSE_NAME"]);
$client->executeBulkWrite("$DB_NAME.DEPARTMENT_TABLE",$bulkWrite);


$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->insert([
    "_id" => "department_$COURSE_CODE",
    "department" => "$COURSE_NAME",
    "dob" => "00-00-0000",
    "email" => "NIL",
    "firstName" => "Department",
    "lastName" => "$COURSE_CODE",
    "mobile" => "0000000000",
    "password" => "$PASSWORD_MD5",
    "role" => "D",
    "username" => "department_$COURSE_CODE"
]);
$client->executeBulkWrite("$DB_NAME.USERS_TABLE",$bulkWrite);


/*

$cmd2['create'] = $COURSE_CODE."_COORDINATOR_TABLE";
$newCollection = new MongoDB\Driver\Command($cmd2);
$cursor = $client->executeCommand($DB_NAME,$newCollection);
*/
$res = ['error' => '200', 'password' => "$PASSWORD"];

echo json_encode($res);

?>