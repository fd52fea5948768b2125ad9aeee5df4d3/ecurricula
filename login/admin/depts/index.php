<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");
$client = new MongoDB\Driver\Manager($MONGO_URL);

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];

//GET ALL AVAILABLE TABLE
$query = [];
$option = [];
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.DEPARTMENT_TABLE",$query);
$rows = $rows->toArray();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>Manage Courses</title>
    <style>

        nav div a span img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
            padding-left: 15px !important;
        }


        .title {
            font-size: 1.1em;
            text-transform: uppercase;
            font-weight: 500;
        }

    </style>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="./../courses/">Departments</a></li>
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-small-only"><img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>

        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5><i class="fa fa-university"></i> Departments</h5><hr><br><br>
            <div class="row">
                <div class="col s12 m4">
                    <p class="title">Available Departments</p><hr>
                    <table class="bordered striped highlight" id="exportableTable">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Department Name</th>
                                <th>Department Code</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $count = 1;
                            foreach ($rows as $r) {
                                echo "<tr><td>";
                                echo $count;
                                echo "</td><td>";
                                echo $r->departmentName;
                                echo "</td><td>";
                                echo $r->departmentCode;
                                echo "<td></tr>";
                                $count++;
                            }

                        ?>
                        </tbody>
                    </table>

                    <div>
                        <br>
                        <a href="#" class="right cyan accent-4 btn waves-effect" id="exportBtn">Export</a>
                    </div>
                </div>
                <div class="col s12 m8">
                    <div class="row border" id="resultBox" hidden="hidden">
                        <div class="col s10 offset-s1">
                            <b class="indigo-text text-darken-2">DEPARTMENT CREATED</b><hr>
                            <p class="big">Username: department_<span id="courseCodePlace">15SE201J</span></p>
                            <p class="big">Password: <span id="coursePasswordPlace">ececjKJKuhiuh</span></p>

                            <a class="btn red right" id="pwdDownload">Download</a>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <span class="hide-on-med-and-up"><br><br></span>
                    <p class="title">Add Department</p><hr>
                    <div class="row">
                        <div class="col s12 m4">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <input id="courseCodeInput" type="text">
                                    <label for="courseCodeInput">Department Code</label>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m8">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <input id="courseNameInput" type="text">
                                    <label for="courseNameInput">Department Name</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a id="courseAddBtn" class="right btn-flat btn blue white-text">ADD Department</a>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../../static/js/export.js"></script>
<script type="text/javascript" src="script.js"></script>

<script>
    $('#exportBtn').click(function () {
        var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
        outputFile = outputFile.replace('.csv','') + '.csv';
        exportTableToCSV.apply(this, [$('#exportableTable'), outputFile]);
    });

</script>

</body>
</html>
