<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$COURSE_CODE = $_POST['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);

$bulk = new MongoDB\Driver\BulkWrite;
$bulk->delete(['_id' => $COURSE_CODE]);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".D3_COURSE_TABLE", $bulk, $writeConcern);


$flare = array();
$flare['name'] = "srm";
$FLARE_CHILDREN = array();
$DEPARTMENT_LIST_CMD = new MongoDB\Driver\Command(['distinct'=>'D3_COURSE_TABLE','key' =>'department']);
$DEPARTMENT_LIST_CURSOR = $client->executeCommand($DB_NAME,$DEPARTMENT_LIST_CMD);
$DEPARTMENT_LIST_CURSOR = current($DEPARTMENT_LIST_CURSOR->toArray())->values;

foreach ($DEPARTMENT_LIST_CURSOR as $department) {
    $DEPARTMENT_CHILDREN = array();
    for($i=1;$i<=8;$i++) {
        $SEMESTER_CHILDREN = array();

        /*** Query for 1 sem 1 dept courses ***/
        $query = ['department' => $department,'semester' => "$i"];
        $option = [];

        $query = new MongoDB\Driver\Query($query,$option);

        $rows = $client->executeQuery("$DB_NAME.D3_COURSE_TABLE",$query);
        $rows = $rows->toArray();
        foreach ($rows as $row) {
            $COURSE_ARRAY = array();
            $COURSE_ARRAY['name'] = $row->_id;
            $COURSE_ARRAY['size'] = 1;
            $COURSE_ARRAY['arcType'] = "SUBJECT";
            $COURSE_ARRAY['courseCode'] = $row->_id;
            $COURSE_ARRAY['children'] = array();
            array_push($SEMESTER_CHILDREN,$COURSE_ARRAY);
        }
        /*** end of query ***/

        $SEMESTER_ARRAY = array();
        $SEMESTER_ARRAY['name'] = "Semester $i";
        $SEMESTER_ARRAY['arcType'] = "SEM";
        $SEMESTER_ARRAY['size'] = 1;
        $SEMESTER_ARRAY['children'] = $SEMESTER_CHILDREN;
        array_push($DEPARTMENT_CHILDREN,$SEMESTER_ARRAY);
    }
    $DEPARTMENT_ARRAY = ["name"=>$department,"arcType"=>"DEPT","departmentCode"=>$department,"size"=>1,"children"=>$DEPARTMENT_CHILDREN];
    array_push($FLARE_CHILDREN,$DEPARTMENT_ARRAY);
}
$flare['children'] = $FLARE_CHILDREN;

file_put_contents("../../../curriculum/flare.json",json_encode($flare));
echo json_encode($flare);

?>