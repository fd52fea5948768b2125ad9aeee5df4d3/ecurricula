$(document).ready(function () {
    var contentDiv = $('#courseData');

    $('select').material_select();
    function renderMain() {
        $.ajax({
            'method': 'POST',
            'url': '1.php',
            'error': function () {
                window.alert(courseCode + " Data Not Present");
            },
            'success': function (res) {
                contentDiv.html(res);


                $(".trash-btn").click(function () {
                    var courseID = $(this).attr('data-course');

                    $.ajax({
                        'method': 'POST',
                        'data': {
                            'courseCode': courseID
                        },
                        'url': '2.php',
                        'error': function () {
                            window.alert("Error");
                        },
                        'success': function (res) {
                            window.alert("Course Successfully Deleted");
                            renderMain();
                        }
                    });
                });
            }
        });
    }

    $('#remakeD3').click(function () {
        $.ajax({
            'method': 'POST',
            'url': '3.php',
            'success': function (res) {
                window.alert("D3 Successfully Remade!!!");
                console.log(res);
            }
        });
    });

    renderMain();


    var baseURL = "manage";

    $('#addCourseBtn').click(function() {
        popitup(baseURL);
    });


    function popitup(url) {
        var height = window.screen.height / 1.5;
        var width = window.screen.width / 1.5;
        newwindow=window.open(url,'name','height='+height+',width='+width);
        if (window.focus) {newwindow.focus()}

        if (!newwindow.closed) {newwindow.focus()}
        return false;
    }

    $('#futureProspectsBtn').click(function() {
        var departmentCode = $('#departmentList').val();

        popitup("fp?q="+departmentCode);
    });

});