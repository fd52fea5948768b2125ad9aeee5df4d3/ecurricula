$(document).ready(function () {
    var contentDiv = $('#deptData');

    $('select').material_select();
    function renderMain() {
        $.ajax({
            'method': 'POST',
            'url': '1.php',
            'error': function () {
                window.alert(courseCode + " Data Not Present");
            },
            'success': function (res) {
                contentDiv.html(res);


                $(".trash-btn").click(function () {
                    var courseID = $(this).attr('data-course');

                    $.ajax({
                        'method': 'POST',
                        'data': {
                            'courseCode': courseID
                        },
                        'url': '2.php',
                        'error': function () {
                            window.alert("Error");
                        },
                        'success': function (res) {
                            window.alert("Entry Successfully Deleted");
                            renderMain();
                        }
                    });
                });
            }
        });
    }

    renderMain();


    var baseURL = "manage";

    $('#addCourseBtn').click(function() {
        popitup(baseURL);
    });


    function popitup(url) {
        var height = window.screen.height / 1.5;
        var width = window.screen.width / 1.5;
        newwindow=window.open(url,'name2','height='+height+',width='+width);
        if (window.focus) {newwindow.focus()}

        if (!newwindow.closed) {newwindow.focus()}
        return false;
    }

    $("#reloadBtn").click(function() {
        location.reload(true);
    });

});