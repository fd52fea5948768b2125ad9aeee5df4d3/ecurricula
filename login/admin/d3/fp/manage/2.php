<?php
session_start();
include_once(__DIR__."/../../../../../includes/general.config.php");
include_once(__DIR__."/../../../../../includes/mongo.db.config.php");


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}
try {

    $DEPT_CODE = $_SESSION['d3_dept_code'];
    $NAME = $_POST['name'];
    $CATEGORY = $_POST['category'];
    $COURSES = isset($_POST['courses'])?$_POST['courses']:[];

    $client = new MongoDB\Driver\Manager($MONGO_URL);

    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk->insert(['_id' => $NAME, 'name' => $NAME,'department'=>$DEPT_CODE, 'category' => $CATEGORY, 'courses' => $COURSES]);

    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $writeResult = $client->executeBulkWrite($DB_NAME . ".D3_FUTURE_PROSPECTS", $bulk, $writeConcern);

    echo "Course Successfully Added";
}catch(Exception $e) {
    echo "Error. May be Course is Already Present";
}
?>