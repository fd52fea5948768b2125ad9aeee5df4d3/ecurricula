<?php
session_start();
include_once(__DIR__."/../../../../../includes/general.config.php");
include_once(__DIR__."/../../../../../includes/mongo.db.config.php");


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$DEPT_CODE = $_SESSION['d3_dept_code'];
$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['department' => $DEPT_CODE];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.D3_COURSE_TABLE",$query);
$rows = $rows->toArray();
echo "<div class=\"row\">";
foreach ($rows as $data) {
    $subjectCode = $data->_id;
    $subjectName = $data->name;

    echo "<div class='col s12 m6'>
        <p><input type='checkbox' id='$subjectCode' class='courseCheckBox'/>
        <label for='$subjectCode'>$subjectCode - $subjectName</label> </p>
    </div>";
}

echo "</div>";
?>