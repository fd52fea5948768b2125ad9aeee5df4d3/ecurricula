$(document).ready(function () {
    $('select').material_select();
    var contentDiv = $('#courseData');

    $.ajax({
        'method': 'POST',
        'url': '1.php',
        'error': function () {
            window.alert("Error...");
        },
        'success': function (res) {
            contentDiv.html(res);

            $('#addFPBtn').click(function () {
                var name = $('#nameInput').val();
                var category = $('#fpTypeSelect').val();
                if(!name)  {
                    window.alert("Enter Valid Name...");
                    return;
                }
                var courses = [];
                $(".courseCheckBox").each(function () {
                   if($(this).prop('checked')) {
                       courses.push($(this).attr("id"));
                   }
                });

                $.ajax({
                    'method': 'POST',
                    'url': '2.php',
                    'data': {
                        'name': name,
                        'category': category,
                        'courses': courses
                    },
                    'error': function () {
                        window.alert("Error...");
                    },
                    'success': function (res) {
                        window.alert(res);
                        window.close();
                    }
                });
            });
        }
    });



});