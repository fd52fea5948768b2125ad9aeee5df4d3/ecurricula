<?php
include_once(__DIR__."/../../../../includes/general.config.php");
include_once(__DIR__."/../../../../includes/mongo.db.config.php");

session_start();


function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../../error.php");
    echo $errorHTML;
    exit;
}



if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$FIXED_CATEGORIES = [['code' => 'BS' , 'name' => 'Basic Sciences'],
    ['code' => 'HS' , 'name' => 'Humanities &amp; Social Science (incl. Arts Courses)'],
    ['code' => 'ES' , 'name' => 'Engineering Sciences'],
    ['code' => 'PC' , 'name' => 'Professional Core'],
    ['code' => 'PE' , 'name' => 'Professional Elective'],
    ['code' => 'OE' , 'name' => 'Open Elective']];


$DEPT_CODE = $_GET['q'];
$_SESSION['d3_dept_code'] = $DEPT_CODE;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../../static/css/materialize.min.css">
    <title><?php echo $DEPT_CODE ?> Future Prospects </title>
    <style>
        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .trash-btn {
            cursor: pointer;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteButton, .editButton {
            cursor: pointer;
        }

        .strong {
            font-weight: bold;
            font-family: monospace;
        }
    </style>
</head>
<body>


<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5>
                <?php echo $DEPT_CODE; ?> Future Prospects
            </h5>
            <hr><br>
            <div id="deptData">

            </div>


            <br>


            <a id="reloadBtn" class="btn blue-text btn-flat"></i> Refresh</a>
            <div class="right">
                <a id="addCourseBtn" class="btn waves-effect red"><i class="fa fa-plus"></i> &nbsp; Add F.P.</a>
            </div>
            <div class="clearfix"> </div>


        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
