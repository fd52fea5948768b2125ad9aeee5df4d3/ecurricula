<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = [];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.D3_COURSE_TABLE",$query);
$rows = $rows->toArray();
echo "<div class=\"card\">
                    <div class=\"card-content\">
                        <span class=\"card-title\">Available Courses</span>
                        <hr>
                        <ul class=\"collection\">";
foreach ($rows as $data) {
    $COURSE_NAME = $data->name;
    $COURSE_CODE = $data->_id;


    echo "<li class=\"collection-item\">$COURSE_CODE - $COURSE_NAME <a data-course='$COURSE_CODE' class='secondary-content'>&nbsp;&nbsp;&nbsp;<i data-course='$COURSE_CODE' class='trash-btn fa red-text fa-trash small'></i> </a> <a href='./edit?q=$COURSE_CODE' target='_blank' class='secondary-content'><i class='fa blue-text small fa-edit'> </i> </a> </li>";



}

echo "</ul>
                    </div>
                </div>";
?>