<?php
session_start();
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}
if(!isset($_SESSION) || $_SESSION['role'] != 'A') {
    displayError("Session Expired!!!");
    exit;
}

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = [];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.DEPARTMENT_TABLE",$query);
$rows = $rows->toArray();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title>SRM Curriculum</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .grey-bar {
            width: 100%;
            height: 2px;
        }

        .trash-btn {
            cursor: pointer;
        }

        .breadcrumb:before {
            content: '\25BA';
            color: rgba(255, 255, 255, 0.7);
            vertical-align: top;
            display: inline-block;
            font-weight: normal;
            font-style: normal;
            font-size: 20px;
            margin: 0 10px 0 8px;
            -webkit-font-smoothing: antialiased;
        }

        nav .nav-wrapper {
            padding-left: 15px !important;
        }


    </style>
</head>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a href="./../">Home</a></li>
            <li><a href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <br>
    <div class="card">
        <div class="card-content">
            <h5>SRM Curriculum</h5>
            <hr>
            <div class="content-box">
                <div id="courseData">

                </div>

                <br>

                <div class="right">
                    <a id="remakeD3" class="btn waves-effect indigo"><i class="fa fa-refresh"></i> &nbsp; Remake Chart</a>
                    <a id="addCourseBtn" class="btn waves-effect red"><i class="fa fa-plus"></i> &nbsp; Add Course</a>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

    <br>
    <div class="card">
        <div class="card-content">
            <h5>Edit Future Prospects</h5>
            <hr>
            <br>
            <div class="row">
                <div class="col s12 m6">
                    <div class="row">
                        <div class="col s12 input-field">
                            <select id="departmentList">
                                <?php
                                    foreach ($rows as $row) {
                                        $departCode = $row->departmentCode;
                                        $departName = $row->departmentName;
                                        echo "<option value='$departCode'>$departName</option>";
                                    }



                                ?>
                            </select>
                            <label for="departmentList">Department</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6">
                    <br>
                    <a id="futureProspectsBtn" class="btn waves-effect green ">Go</a>

                    <br><br>
                    </div>
                <a href="../../../curriculum" target="_blank" class="btn right btn-flat blue-text">CURRICULA <i class="fa fa-arrow-circle-right"></i> </a>

            </div>
        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
