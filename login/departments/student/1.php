<?php

include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

session_start();

$STD_ID = $_POST['studentID'];


$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['_id' => $STD_ID];
$option = [];
$TABLE_NAME = "USERS_TABLE";
$query = new MongoDB\Driver\Query($query,$option);
$rows = $client->executeQuery("$DB_NAME.$TABLE_NAME",$query);
$rows = $rows->toArray();

if(sizeof($rows) == 0) {
    echo "<br><p class='red-text'>Invalid Student ID Entered!!!</p> ";
    exit;
}

$NAME = ($rows[0]->firstName)." ".($rows[0]->lastName);
$DEPT  = $rows[0]->department;
$EMAIL = $rows[0]->email;
$DOB = $rows[0]->dob;
$MOBILE = $rows[0]->mobile;


echo "<br>
<table class='bordered striped responsive-table highlight'>
    
    <tbody>
        <tr>
            <td>Name</td>
            <td>$NAME</td>
        </tr>
        <tr>
            <td>Department</td>
            <td>$DEPT</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>$EMAIL</td>
        </tr>
        <tr>
            <td>DOB</td>
            <td>$DOB</td>
        </tr>
        <tr>
            <td>Mobile</td>
            <td>$MOBILE</td>
        </tr>
    </tbody>
</table>";


?>