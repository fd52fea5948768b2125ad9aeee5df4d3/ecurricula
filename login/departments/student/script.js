$(document).ready(function() {
    $('select').material_select();

    $('#goBtn').click(function () {
        var studentID = $('#studentSelectDiv').val();
        $.ajax({
            'method': 'POST',
            'data': {
                'studentID': studentID
            },
            'url': './1.php',
            'error': function () {
                window.alert("Something Wrong!");
            },
            'success': function (phpdata) {
                $('#resultDiv').html(phpdata);
            }
        });
    });

});