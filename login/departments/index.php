<?php
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");

function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../..";
    include_once(__DIR__."/../../error.php");
    echo $errorHTML;
    exit;
}

session_start();
if(!isset($_SESSION) || $_SESSION['role'] != 'D') {
    displayError("Session Expired!!!");
    exit;
}

$SESSION_USERNAME = $_SESSION['username'];


$prefix = 'department_';
$deptCode = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $SESSION_USERNAME);


$_SESSION['departmentCode'] = $deptCode;

$client = new MongoDB\Driver\Manager($MONGO_URL);
$query = ['department' => $deptCode];
$option = [];

$query = new MongoDB\Driver\Query($query,$option);

$rows = $client->executeQuery("$DB_NAME.D3_COURSE_TABLE",$query);
$rows = $rows->toArray();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../static/css/materialize.min.css">
    <title><?php echo $deptCode; ?> Department Home</title>
    <style>

        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .dark {
            width: 1px;
            border-right: 1px solid green;
        }


        .heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
        }

        .big {
            font-size: 1.2em !important;
        }

        .sub-heading {
            text-transform: uppercase;
            margin: -24px;
            padding: 10px;
            font-size: 1.3em !important;
        }

        .plus-pad {
            padding: 20px 0px 20px 0px;
            font-size: 4em;
        }
    </style>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">

    <li><a href="./profile/">Profile</a></li>

    <li><a href="./student/">Student Details</a></li>
</ul>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
        <ul class="right">
            <li><a class="dropdown-button" data-activates="dropdown1">Options&nbsp;&nbsp; <i class="fa big fa-arrow-circle-down"></i></a></li>
            <li><a  href="./../../">Logout</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <span class="heading grey darken-4 white-text card-title">Department Home - <?php echo $deptCode; ?></span>
            <br><h5><?php echo "$deptCode";?> Curriculum</h5>
            <hr>
            <div class="content-box">
                <div id="courseData">

                </div>

                <br>

                <div class="right">
                    <a id="addCourseBtn" class="btn waves-effect red"><i class="fa fa-plus"></i> &nbsp; Add Course</a>
                </div>
                <div class="clearfix"> </div>
            </div>

            <a href="../../curriculum" target="_blank" class="btn btn-flat blue-text">CURRICULA <i class="fa fa-arrow-circle-right"></i> </a>

            <br><br>

            <div id="deptData">

            </div>


            <br>


            <a id="reloadBtn" class="btn blue-text btn-flat"></i> Refresh</a>
            <div class="right">
                <a id="addFPBtn" class="btn waves-effect red"><i class="fa fa-plus"></i> &nbsp; Add F.P.</a>
            </div>
            <div class="clearfix"> </div>


        </div>
    </div>
</div>




<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../../static/js/export.js"></script>
<script type="text/javascript" src="script.js"></script>


<script>
    $('#exportBtn').click(function () {
        var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
        outputFile = outputFile.replace('.csv','') + '.csv';
        exportTableToCSV.apply(this, [$('#exportableTable'), outputFile]);
    });

</script>

</body>
</html>
