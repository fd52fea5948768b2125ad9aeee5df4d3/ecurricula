$(document).ready(function () {
    var contentDiv = $('#courseData');
    var contentDiv2 = $('#deptData');
    $('select').material_select();

    function renderMain2() {
        $.ajax({
            'method': 'POST',
            'url': '3.php',
            'error': function () {
                window.alert(courseCode + " Data Not Present");
            },
            'success': function (res) {
                contentDiv2.html(res);


                $(".trash-btn").click(function () {
                    var courseID = $(this).attr('data-course');

                    $.ajax({
                        'method': 'POST',
                        'data': {
                            'courseCode': courseID
                        },
                        'url': '4.php',
                        'error': function () {
                            window.alert("Error");
                        },
                        'success': function (res) {
                            window.alert("Entry Successfully Deleted");
                            renderMain2();
                        }
                    });
                });
            }
        });
    }

    function renderMain() {
        $.ajax({
            'method': 'POST',
            'url': '1.php',
            'error': function () {
                window.alert(" Data Not Present");
            },
            'success': function (res) {
                contentDiv.html(res);


                $(".trash-btn").click(function () {
                    var courseID = $(this).attr('data-course');

                    $.ajax({
                        'method': 'POST',
                        'data': {
                            'courseCode': courseID
                        },
                        'url': '2.php',
                        'error': function () {
                            window.alert("Error");
                        },
                        'success': function (res) {
                            window.alert("Course Successfully Deleted");
                            renderMain();
                        }
                    });
                });
            }
        });
    }

    renderMain();
    renderMain2();



    $('#addCourseBtn').click(function() {
        var baseURL = "manage";
        popitup(baseURL);
    });


    function popitup(url) {
        var height = window.screen.height / 1.5;
        var width = window.screen.width / 1.5;
        newwindow=window.open(url,'name','height='+height+',width='+width);
        if (window.focus) {newwindow.focus()}

        if (!newwindow.closed) {newwindow.focus()}
        return false;
    }




    $('#addFPBtn').click(function() {
        var baseURL = "addFP";
        popitup(baseURL);
    });

    $("#reloadBtn").click(function() {
        location.reload(true);
    });
});

