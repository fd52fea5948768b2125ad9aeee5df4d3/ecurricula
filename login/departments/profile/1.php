<?php

include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");
session_start();
$res = Array();
$res['error'] = 0;

//TODO: Check For Registration Closed

$username = $_SESSION['username'];

$client = new MongoDB\Driver\Manager($MONGO_URL);

$password = trim($_REQUEST['password']);
$password_retype = trim($_REQUEST['password_retype']);

if ($password == null || strlen($password) < 8) {
    $res['error'] = 1;
    $res['errorMsg'] = "Password should be of atleast 8 characters";
    echo json_encode($res);
    return;
}
if ($password != $password_retype) {
    $res['error'] = 1;
    $res['errorMsg'] = "Password Mismatch";
    echo json_encode($res);
    return;
}
$first_name = $_REQUEST['first_name'];
if ($first_name == null || !preg_match("/^[a-zA-Z]*$/",$first_name)) {
    $res['error'] = 1;
    $res['errorMsg'] = "First Name can have only letters";
    echo json_encode($res);
    return;
}
$last_name = $_REQUEST['last_name'];
if ($last_name == null || !preg_match("/^[a-zA-Z]*$/",$last_name)) {
    $res['error'] = 1;
    $res['errorMsg'] = "Last Name can have only letters";
    echo json_encode($res);
    return;
}
$email = $_REQUEST['email'];
if ($email == null || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $res['error'] = 1;
    $res['errorMsg'] = "Invalid Email ID";
    echo json_encode($res);
    return;
}
$dob = $_REQUEST['dob'];
if ($dob == null) {
    $res['error'] = 1;
    $res['errorMsg'] = "Invalid Date of Birth";
    echo json_encode($res);
    return;
}
$mobile = $_REQUEST['mobile'];
if ($mobile == null || strlen($mobile) != 10) {
    $res['error'] = 1;
    $res['errorMsg'] = "Invalid Mobile Number";
    echo json_encode($res);
    return;
}
$password = md5($password);
$bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulkWrite->update(["_id"=> $username],['$set' => ["dob" => "$dob",
    "email" => "$email",
    "firstName" => "$first_name",
    "lastName" => "$last_name",
    "mobile" => "$mobile",
    "password" => "$password"]],
    ['multi' => false, 'upsert' => false]);

$result = $client->executeBulkWrite("$DB_NAME.USERS_TABLE",$bulkWrite);

echo json_encode($res);
?>