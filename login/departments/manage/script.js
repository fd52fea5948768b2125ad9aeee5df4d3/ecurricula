$(document).ready(function () {

    $('select').material_select();

    $('#updateBtn').click(function() {
        $.ajax({
            'method': 'POST',
            'url': '1.php',
            'data': {
                'courseCode': $('#courseCodeInput').val(),
                'courseName': $('#courseNameInput').val(),
                'semester': $('#semesterSelect').val(),
                'categoryName': $('#typeSelect').find('option:selected').html(),
                'category': $('#typeSelect').val(),
                'l': $('#lectureSelect').val(),
                't': $('#theorySelect').val(),
                'p': $('#practicalSelect').val(),
                'c': $('#totalCreditSelect').val(),
                'type': $('#courseTypeSelect').val(),
                'description': $('#courseDescription').val()
            },
            'dataType': 'JSON',
            'success': function (res) {
                if(res.error == 200) {
                    window.alert("Update Successful");
                    window.close();
                }else {
                    window.alert("ERROR: " + res.errorMsg);
                }

            }
        });
    });
});