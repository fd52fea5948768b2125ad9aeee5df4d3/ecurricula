<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

session_start();


function displayError($str) {
    $errorHTML = "";
    $errorMsg = "$str";
    $BASE_PATH = "../../../..";
    include_once(__DIR__."/../../../error.php");
    echo $errorHTML;
    exit;
}



if(!isset($_SESSION) || $_SESSION['role'] != 'D') {
    displayError("Session Expired!!!");
    exit;
}

$FIXED_CATEGORIES = [['code' => 'BS' , 'name' => 'Basic Sciences'],
    ['code' => 'HS' , 'name' => 'Humanities &amp; Social Science (incl. Arts Courses)'],
    ['code' => 'ES' , 'name' => 'Engineering Sciences'],
    ['code' => 'PC' , 'name' => 'Professional Core'],
    ['code' => 'PE' , 'name' => 'Professional Elective'],
    ['code' => 'OE' , 'name' => 'Open Elective']];


if(isset($_GET['q'])) {
    $DISABLED = "disabled";
    $ACTION_TEXT = "Update";

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['_id' => $_GET['q']];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);

    $rows = $client->executeQuery("$DB_NAME.D3_COURSE_TABLE",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) {
        displayError("Invalid Data");
        return;
    }


    $COURSE_NAME = $rows[0]->name;
    $COURSE_CODE = $rows[0]->_id;
    $SEMESTER = $rows[0]->semester;
    $COURSE_CATEGORY = $rows[0]->category;
    $L = $rows[0]->l;
    $T = $rows[0]->t;
    $P = $rows[0]->p;
    $C = $rows[0]->c;
    $COURSE_TYPE = $rows[0]->type;
    $COURSE_DESC = $rows[0]->description;


}else {
    displayError("Invalid Course");
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../../../favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../../static/css/materialize.min.css">
    <title><?php echo $ACTION_TEXT ?> Course </title>
    <style>
        .row-fix {
            margin-bottom: 0;
        }

        input[type=text]:disabled {
            color: black !important;
        }

        textarea.materialize-textarea {
            padding: 10px 5px 0px 5px;
            margin-bottom: 2px !important;
        }

        [type="radio"]:not(:checked), [type="radio"]:checked {
            position: inherit;
        }

        .center-fix {
            margin-top: 10px;
        }

        .nos {
            font-weight: bold;
            font-size: 1.5em;
            font-family: monospace;
        }

        .deleteButton, .editButton {
            cursor: pointer;
        }

        .strong {
            font-weight: bold;
            font-family: monospace;
        }
    </style>
</head>
<body>


<div class="container">
    <div class="hide-on-small-only">
        <br><br><br>
    </div>
    <div class="card">
        <div class="card-content">
            <h5>
                <?php echo $ACTION_TEXT; ?> Course
            </h5>
            <hr><br>
            <div class="row">
                <div class="col s12 m4">
                    <div class="row">
                        <div class="col s12 input-field">
                            <input <?php echo $DISABLED;?> id="courseCodeInput" value="<?php echo $COURSE_CODE;?>" type="text">
                            <label for="courseCodeInput">Course Code</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m8">
                    <div class="row">
                        <div class="col s12 input-field">
                            <input id="courseNameInput" value="<?php echo $COURSE_NAME;?>" type="text">
                            <label for="courseNameInput">Course Name</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m4">
                    <div class="row">
                        <div class="col s12 input-field">
                            <select id="semesterSelect" disabled>
                                <?php
                                for($i=1;$i<9;$i++) {
                                    if($SEMESTER == $i)
                                        echo "<option selected value='$i'>Semster $i</option>";
                                    else
                                        echo "<option value='$i'>Semster $i</option>";
                                }
                                ?>
                            </select>
                            <label for="semesterSelect">Select Semester</label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col s12 m6">
                    <div class="row">
                        <div class="input-field col s12">
                            <select id="typeSelect">
                                <?php

                                foreach ($FIXED_CATEGORIES as $cat) {
                                    $code = $cat['code'];
                                    $name = $cat['name'];
                                    if($COURSE_CATEGORY == $cat['code'])
                                        echo "<option selected value=\"$code\">".$name."</option>";
                                    else
                                        echo "<option value=\"$code\">".$name."</option>";
                                }

                                ?>
                            </select>
                            <label for="typeSelect">Course Category</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6">
                    <div class="row">
                        <div class="input-field col s12 m3">
                            <select id="lectureSelect">
                                <?php
                                for($i=0;$i<5;$i++) {
                                    if($i == $L)
                                        echo "<option selected value=\"$i\">$i</option>";
                                    else
                                        echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                            <label for="lectureSelect">Lecture (L)</label>
                        </div>
                        <div class="input-field col s12 m3">
                            <select id="theorySelect">
                                <?php
                                for($i=0;$i<5;$i++) {
                                    if($i == $T)
                                        echo "<option selected value=\"$i\">$i</option>";
                                    else
                                        echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                            <label for="theorySelect">Theory (T)</label>
                        </div>
                        <div class="input-field col s12 m3">
                            <select id="practicalSelect">
                                <?php
                                for($i=0;$i<26;$i++) {
                                    if($i == $P)
                                        echo "<option selected value=\"$i\">$i</option>";
                                    else
                                        echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                            <label for="practicalSelect">Practical (P)</label>
                        </div>
                        <div class="input-field col s12 m3">
                            <select id="totalCreditSelect">
                                <?php
                                for($i=0;$i<26;$i++) {
                                    if($i == $C)
                                        echo "<option selected value=\"$i\">$i</option>";
                                    else
                                        echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                            <label for="totalCreditSelect">Total Credit (C)</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m4">
                    <div class="row">
                        <div class="col s12 input-field">
                            <select id="courseTypeSelect">
                                <?php
                                if($COURSE_TYPE == 'e') {
                                    echo "
                                <option selected value=\"e\">Elective Course</option>
                                <option value=\"c\">Core Course</option>";
                                }

                                if($COURSE_TYPE == 'c') {
                                    echo "
                                <option value=\"e\">Elective Course</option>
                                <option selected value=\"c\">Core Course</option>";
                                } ?>
                            </select>
                            <label for="courseTypeSelect">Course Type</label>
                        </div>
                    </div>
                </div>
                <div class="col s12 m8">
                    <div class="row">
                        <div class="col s12 input-field">
                            <textarea rows="1" id="courseDescription" class="materialize-textarea"><?php echo $COURSE_DESC?></textarea>
                            <label for="courseDescription">Course Description</label>
                        </div>
                    </div>
                </div>
            </div>





            <div class="row">
                <br>
                <a id="updateBtn" class="right btn waves-ripple red"><?php echo $ACTION_TEXT; ?></a>
            </div>


        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../../../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../../static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
