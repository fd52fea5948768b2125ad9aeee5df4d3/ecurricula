<?php
include_once(__DIR__."/../../../includes/general.config.php");
include_once(__DIR__."/../../../includes/mongo.db.config.php");

session_start();


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'D') {
    displayError("Session Expired!!!");
    exit;
}

$COURSE_NAME = $_POST['courseName'];
$COURSE_CODE = $_POST['courseCode'];
$SEMESTER = $_POST['semester'];
$COURSE_CATEGORY = $_POST['category'];
$L = $_POST['l'];
$T = $_POST['t'];
$P = $_POST['p'];
$C = $_POST['c'];
$COURSE_CATEGORY_NAME = $_POST['categoryName'];
$COURSE_TYPE = $_POST['type'];
$COURSE_DESC = $_POST['description'];
$DEPARTMENT = $_SESSION['departmentCode'];

$dbChange = ['name' => $COURSE_NAME,
                'semester' => $SEMESTER,
'category' => $COURSE_CATEGORY,'categoryName'=>$COURSE_CATEGORY_NAME,'department' => $DEPARTMENT,
'l' => $L,'t' => $T,'p' => $P,'c' => $C,
'type' => $COURSE_TYPE,'description' => $COURSE_DESC];

$client = new MongoDB\Driver\Manager($MONGO_URL);
$bulk = new MongoDB\Driver\BulkWrite;
$bulk->update(
    ['_id' => $COURSE_CODE],
    ['$set' => $dbChange],
    ['multi' => false, 'upsert' => false]
);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".D3_COURSE_TABLE", $bulk, $writeConcern);


echo json_encode(['error' => 200]);

?>