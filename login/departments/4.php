<?php
session_start();
include_once(__DIR__."/../../includes/general.config.php");
include_once(__DIR__."/../../includes/mongo.db.config.php");


function displayError($str) {
    echo json_encode(['error' => 404, 'errorMsg' => $str]);
    exit;
}

if(!isset($_SESSION) || $_SESSION['role'] != 'D') {
    displayError("Session Expired!!!");
    exit;
}

$COURSE_CODE = $_POST['courseCode'];

$client = new MongoDB\Driver\Manager($MONGO_URL);

$bulk = new MongoDB\Driver\BulkWrite;
$bulk->delete(['_id' => $COURSE_CODE]);

$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$writeResult = $client->executeBulkWrite($DB_NAME.".D3_FUTURE_PROSPECTS", $bulk, $writeConcern);




?>