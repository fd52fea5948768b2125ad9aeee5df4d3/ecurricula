<?php
    include_once(__DIR__."/../includes/general.config.php");
    include_once(__DIR__."/../includes/mongo.db.config.php");

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $password = md5($_POST['password']);
    $query = ['username' => $_POST['username'],'password' => $password];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);

    $rows = $rows->toArray();

    if(sizeof($rows) == 1) {
        $res = array('error' => '200', 'role' => $rows[0]->role);

        session_start();
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['role'] = $rows[0]->role;
        $_SESSION['userFullName'] = $rows[0]->firstName." ".$rows[0]->lastName;

    }else {
        $res = array('error' => '404');
    }
    echo  json_encode($res);
?>