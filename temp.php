<?php
    include_once(__DIR__."/includes/mongo.db.config.php");
    include_once(__DIR__."/includes/general.config.php");

    $client = new MongoDB\Driver\Manager($MONGO_URL);

    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);

    for($unit=1;$unit<=50;$unit++) {
        $hint = ['hint1','hint2','hint2'];
        $bulkWrite->insert(["_id" => "15SE201J1011$unit","sessionName"=>"Introduction", "sessionID" => "15SE201J1011", "unitID" => "15SE201J1",
            "question" => "This is a Sample question Nu. $unit" ,"hints" => $hint]);
    }

    $result = $client->executeBulkWrite("$DB_NAME.LONG_QUESTION_TABLE",$bulkWrite);



/*
    $regex = new MongoDB\BSON\Regex('');

    $query = ['marks' => $regex];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.STD_DB_RA1511008010257",$query);

    foreach ($rows as $r) {
        echo json_encode($r)."<br><br>";
    }

*/

?>