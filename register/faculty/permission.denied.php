<html>
<head>
    <title>eCurricula Register</title>
    <!-- BASIC SETUP (DO NOT CHANGE) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../static/js/jquery-3.1.1.min.js"></script>
    <script src="script.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="../../static/css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" href="../../static/css/font-awesome.min.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- DONT CHANGE ABOVE IT -->
</head>
<style>
    .container {
        margin-top: 80px;
    }
    nav div a img.logo-img {
        height: 100%;
        padding: 4px;
        margin-left: 40px;
    }
    .main-logo {
        margin-bottom: 50px;
    }
    .login {
        padding: 20px;
        font-size: 1.3em;
        margin-bottom: 15px;
    }
    #error {
        margin-top: 10px;
    }
    @media only screen and (max-width : 992px) {
        nav div a img.logo-img {
            margin-left: 0;
        }
    }
    .dropdown-content li>a, .dropdown-content li>span {
        color: #1a237e;
    }
    .picker__date-display{
        background-color: #3949ab;
    }
    .picker__weekday-display {
        background-color: #283593;
    }
    .picker__day--selected, .picker__day--selected:hover, .picker--focused .picker__day--selected {
        background-color: #3949ab;
    }
    .picker__close, .picker__today {
        color: #d81b60;
    }
    .large {
        font-size: 500px;
    }

    .big {
        font-size: 3em;
        text-transform: uppercase;
        font-weight: 200;
    }
</style>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../../static/images/logo.png"> </img></a>
        <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col s12 l10 offset-l1 m12 ">
            <div class="card">
                <div class="login green darken-2 white-text"><?php echo $ERROR_USER_NAME; ?> REGISTRATION</div>
                <div class="card-content">

                    <p class="center"><i class="fa large red-text text-darken-1 fa-exclamation-triangle"></i> </p>
                    <br><br>
                    <p class="big center">Registration is Closed</p>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- BASIC SETUP (DO NOT CHANGE) -->
<script type="text/javascript" src="../../static/js/materialize.min.js"></script>
<!-- DONT CHANGE ABOVE IT -->
</body>
</html>