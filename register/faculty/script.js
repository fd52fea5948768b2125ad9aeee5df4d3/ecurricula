$(document).ready(function(){
    var errorDiv = $('#errorMsg');
    var departmentList = $('#departmentList');
    errorDiv.hide();
    $calender = $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 70, // Creates a dropdown of 100 years to control year
        max: true
    });
    //console.log("DEBUG MODE");

    $('select').material_select();
    $('#button').click(function(){
        var username = $('#username').val();
        var department_id = departmentList.find(':selected').val();
        var department_data = departmentList.find(':selected').text();
        var password = $('#password').val();
        var password_retype = $('#passwordRetype').val();
        var first_name = $('#firstName').val();
        var last_name = $('#lastName').val();
        var email = $('#email').val();
        var picker = $('.datepicker').pickadate('picker');
        var dob = picker.get('select', 'yyyy-mm-dd')
        var mobile = $('#mobilenumber').val();
        $.ajax({
            type: 'POST',
            data: {
                'username': username,
                'dept_id': department_id,
                'dept_name': department_data,
                'password': password,
                'password_retype': password_retype,
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'dob': dob,
                'mobile': mobile
            },
            url: './register.do.php',
            dataType: 'json',
            beforeSend: function(){
                $('#errorMsg').hide();
            },
            success: function(codedata) {
                errorDiv.html("");
                if(codedata['error'] == 1) {
                    //Error Occured
                    errorDiv.show();
                    errorDiv.html(codedata['errorMsg']);
                }else if(codedata['error'] == 0){
                    window.alert("Registration Successful");
                    window.location.href = "./../../";
                }else {
                    window.alert("Something is Wrong... Contact Admin");
                }
            },
            error: function () {
                errorDiv.show();
                errorDiv.html("Error 0x0003");

            }
        });
    });
});