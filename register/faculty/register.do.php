<?php
    include_once(__DIR__."/../../includes/general.config.php");
    include_once(__DIR__."/../../includes/mongo.db.config.php");

    $res = Array();
    $res['error'] = 0;
    
    //TODO: Check For Registration Closed

    $username = trim($_REQUEST['username']);
    if($username == null || strlen($username) < 6) {
        $res['error'] = 1;
        $res['errorMsg'] = "INVALID USERNAME";
        echo json_encode($res);
        return;
    }
    if(strpos($username," ") !== false) {
        $res['error'] = 1;
        $res['errorMsg'] = "WHITESPACES ARE NOT ALLOWED";
        echo json_encode($res);
        return;
    }

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['username' => $username];
    $options = [];
    $query = new MongoDB\Driver\Query($query,$options);
    $rows = $client->executeQuery("$DB_NAME.USERS_TABLE",$query);

    if(sizeof($rows->toArray()) > 0) {
        $res['error'] = 1;
        $res['errorMsg'] = "USER ALREADY EXIST";
        echo json_encode($res);
        return;
    }

    $dept_id = $_REQUEST['dept_id'];
    if($dept_id == null) {
        $res['error'] = 1;
        $res['errorMsg'] = "Please Select a Department";
        echo json_encode($res);
        return;
    }

    $dept_name = $_REQUEST['dept_name'];
    $password = trim($_REQUEST['password']);
    $password_retype = trim($_REQUEST['password_retype']);
    if ($password == null || strlen($password) < 8) {
        $res['error'] = 1;
        $res['errorMsg'] = "Password should be of atleast 8 characters";
        echo json_encode($res);
        return;
    }
    if ($password != $password_retype) {
        $res['error'] = 1;
        $res['errorMsg'] = "Password Mismatch";
        echo json_encode($res);
        return;
    }
    $first_name = $_REQUEST['first_name'];
    if ($first_name == null || !preg_match("/^[a-zA-Z]*$/",$first_name)) {
        $res['error'] = 1;
        $res['errorMsg'] = "First Name can have only letters";
        echo json_encode($res);
        return;
    }
    $last_name = $_REQUEST['last_name'];
    if ($last_name == null || !preg_match("/^[a-zA-Z]*$/",$last_name)) {
        $res['error'] = 1;
        $res['errorMsg'] = "Last Name can have only letters";
        echo json_encode($res);
        return;
    }
    $email = $_REQUEST['email'];
    if ($email == null || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $res['error'] = 1;
        $res['errorMsg'] = "Invalid Email ID";
        echo json_encode($res);
        return;
    }
    $dob = $_REQUEST['dob'];
    if ($dob == null) {
        $res['error'] = 1;
        $res['errorMsg'] = "Invalid Date of Birth";
        echo json_encode($res);
        return;
    }
    $mobile = $_REQUEST['mobile'];
    if ($mobile == null || strlen($mobile) != 10) {
        $res['error'] = 1;
        $res['errorMsg'] = "Invalid Mobile Number";
        echo json_encode($res);
        return;
    }
    $password = md5($password);

    $COLLECTION_NAME = "FACULTY_$username";
    $cmd['create'] = $COLLECTION_NAME;
    $newCollection = new MongoDB\Driver\Command($cmd);
    $cursor = $client->executeCommand($DB_NAME,$newCollection);
    $res['MESSAGE'] = json_encode($cursor->toArray());

    //TODO: Insert User INTO Users Table
    $bulkWrite = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulkWrite->insert(["_id"=> $username,"department" => $dept_name,"dob" => "$dob",
    "email" => "$email",
    "firstName" => "$first_name",
    "lastName" => "$last_name",
    "mobile" => "$mobile",
    "password" => "$password",
    "role" => "F",
    "username" => "$username"]);

    $result = $client->executeBulkWrite("$DB_NAME.USERS_TABLE",$bulkWrite);

    echo json_encode($res);
?>