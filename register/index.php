<?php
    include_once(__DIR__."/../includes/general.config.php");
    include_once(__DIR__."/../includes/mongo.db.config.php");

    $client = new MongoDB\Driver\Manager($MONGO_URL);
    $query = ['_id' => 'STUDENT_REGISTER'];
    $option = [];
    $query = new MongoDB\Driver\Query($query,$option);
    $rows = $client->executeQuery("$DB_NAME.CONTROL_TABLE",$query);
    $rows = $rows->toArray();

    if($rows[0]->value == 0) {
        $ERROR_USER_NAME = "STUDENT";
        include_once (__DIR__."/permission.denied.php");
        exit;
    }
?>
<html>
<head>
    <title>Student Registration</title>
    <!-- BASIC SETUP (DO NOT CHANGE) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../static/js/jquery-3.1.1.min.js"></script>
    <script src="script.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="../static/css/materialize.min.css" media="screen,projection" />
    <link rel="stylesheet" type="text/css" href="../static/css/font-awesome.min.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- DONT CHANGE ABOVE IT -->
</head>
<style>
    .container {
        margin-top: 80px;
    }
    nav div a img.logo-img {
        height: 100%;
        padding: 4px;
        margin-left: 40px;
    }
    .main-logo {
        margin-bottom: 50px;
    }
    .login {
        padding: 20px;
        font-size: 1.3em;
        margin-bottom: 15px;
    }
    #error {
        margin-top: 10px;
    }
    @media only screen and (max-width : 992px) {
        nav div a img.logo-img {
            margin-left: 0;
        }
    }
    .dropdown-content li>a, .dropdown-content li>span {
        color: #1a237e;
    }
    .picker__date-display{
        background-color: #3949ab;
    }
    .picker__weekday-display {
        background-color: #283593;
    }
    .picker__day--selected, .picker__day--selected:hover, .picker--focused .picker__day--selected {
        background-color: #3949ab;
    }
    .picker__close, .picker__today {
        color: #d81b60;
    }
</style>
<body>
<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL  ?>"><img id="image" class="brand-logo logo-img s2" src="../static/images/logo.png"> </img></a>
        <a href="#" class="brand-logo  center hide-on-med-and-down"><?php echo $NAVBAR_TEXT; ?></a>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col s12 l10 offset-l1 m12 ">
            <div class="card">
                <div class="login green darken-2 white-text">STUDENT REGISTRATION</div>
                <div class="card-content">
                    <!-- Username -->
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-user prefix"></i>
                                    <input id="username" type="text" name="username" class="validate">
                                    <label for="username">Registration Number</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6">
                            <div class="row input-field">
                                <div class="col s12 input-field">
                                    <select id="departmentList">
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="1"> Computer Science and Eng. </option>
                                        <option value="2"> Information Technology </option>
                                        <option value="3"> Software Engineering </option>
                                        <option value="4"> Civil Engineering </option>
                                        <option value="5"> Mechanical engineering </option>
                                        <option value="6"> Automobile Engineering </option>
                                        <option value="7"> Aerospace Engineering </option>
                                        <option value="8"> Mechatronics </option>
                                        <option value="9">  Electronics and Comm. </option>
                                        <option value="10"> Telecommunication </option>
                                        <option value="11"> Electrical & Electronics </option>
                                        <option value="12"> Electronics and Instru. </option>
                                        <option value="13"> Instru. & Ctrl Eng </option>
                                        <option value="14"> Chemical Engineering </option>
                                        <option value="15"> Biotechnology </option>
                                        <option value="16"> Biomedical Engineering </option>
                                        <option value="17"> Genetic Engineering </option>
                                        <option value="18"> Food Process Engineering </option>
                                        <option value="19"> Nanotechnology </option>
                                        <option value="20"> Nuclear Engineering </option>
                                    </select>
                                    <label>Department</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Username -->

                    <!-- Password -->
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-lock prefix"></i>
                                    <input id="password" type="password" name="username" class="validate">
                                    <label for="password">Password</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-lock prefix"></i>
                                    <input id="passwordRetype" type="password" name="username" class="validate">
                                    <label for="passwordRetype">Confirm Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Password -->
                    <!-- Name -->
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-user-circle-o prefix"></i>
                                    <input id="firstName" type="text" name="username" class="validate">
                                    <label for="firstName">First Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-user-circle-o prefix"></i>
                                    <input id="lastName" type="text" name="username" class="validate">
                                    <label for="lastName">Last Name</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Name -->
                    <!-- Email -->
                    <div class="row">
                        <div class="col s12 m8 l8">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-envelope prefix"></i>
                                    <input id="email" type="text" name="username" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m4 l4">

                            <label for="dob">Date of Birth</label>
                            <input id="dob" type="date" class="datepicker">

                        </div>
                    </div>
                    <!-- Email -->
                    <!-- Mobile -->
                    <div class="row">
                        <div class="col s10 m8 l6">
                            <div class="row">
                                <div class="col s12 input-field">
                                    <i class="fa fa-mobile-phone prefix"></i>
                                    <input id="mobilenumber" type="tel" name="username" class="validate">
                                    <label for="mobilenumber">Mobile Number</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Moblie -->
                    <div class="row center red-text text-darken-2 flow-text">
                        <p id ="errorMsg">Error Message Will Come Here...</p>
                    </div>
                    <div class="row center">
                        <a class="waves-effect waves-light btn-flat grey darken-3 white-text" id="button"><b>Register as student</b></a>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- BASIC SETUP (DO NOT CHANGE) -->
<script type="text/javascript" src="../static/js/materialize.min.js"></script>
<!-- DONT CHANGE ABOVE IT -->
</body>
</html>