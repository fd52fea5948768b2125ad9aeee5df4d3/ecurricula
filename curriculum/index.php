<?php
include_once(__DIR__."/../includes/general.config.php");
session_start();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./../favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../static/css/materialize.min.css">
    <title>SRM Curriculum</title>
    <style>
        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }

        .sub-flex {
            display: inline;
        }

        .flex-main {
            display: flex;
            justify-content: center;
        }

        .flex-item {
            margin-right: 10px;
            margin-left: 10px;
        }

        circle,
        path {
            cursor: pointer;
        }

        circle {
            fill: none;
            pointer-events: all;
        }

        .data {
            width: 340px;


        }

        h6 {
            border-bottom: 1px white dashed;
            line-height: 30px;
        }
        
        .sub-flex > .card {
            cursor: pointer;
        }

        .sub-flex > .card:hover {
            background: whitesmoke;
        }

        .sub-flex > .card:active {
            background: lightgrey;
        }

        .no-pad-top {
            padding-top: 5px !important;
        }

        .collapsible-body {
           padding: 15px;
        }

        .deptCourseChip {
            cursor: pointer;
        }
        
        .hint {
            color: #a8a8a8;
            line-height: 10px;
            font-family: monospace;
            font-size: 15px;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="../static/images/logo.png"></span>
        </a>
        <a href="./../" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
    </div>
</nav>

<div class="flex-main">
    <br>

    <div id="departmentCardDiv" hidden class="card blue-grey data">
        <div id="departmentCardContent" class="card-content white-text">

        </div>
    </div>

    <div class="card flex-item"><span id="svgCircle" class="circle"></span></div>





    <div id="courseCardDiv" hidden class="card grey lighten-5 data">
        <div id="courseCardContent" class="card-content">

        </div>
    </div>
</div>



<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="../static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../static/js/materialize.min.js"></script>
<script type="text/javascript" src="../static/js/d3.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
