var currentSelectedID = "";
var currentColor = "";

var margin = {top: 350, right: 380, bottom: 350, left: 380},
    radius = Math.min(margin.top, margin.right, margin.bottom, margin.left) - 10;

function filter_min_arc_size_text(d, i) {
    return (d.dx*d.depth*radius/3)>14;
}

var hue = d3.scale.category10();

var luminance = d3.scale.sqrt()
    .domain([0, 1e6])
    .clamp(true)
    .range([90, 20]);

var svg = d3.select("#svgCircle").append("svg")
    .attr("width", margin.left + margin.right)
    .attr("height", margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var partition = d3.layout.partition()
    .sort(function(a, b) { return d3.ascending(a.name, b.name); })
    .size([2 * Math.PI, radius]);

var arc = d3.svg.arc()
    .startAngle(function(d) { return d.x; })
    .endAngle(function(d) { return d.x + d.dx - .01 / (d.depth + .5); })
    .innerRadius(function(d) { return radius / 3 * d.depth; })
    .outerRadius(function(d) { return radius / 3 * (d.depth + 1) - 1; });

function format_number(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function format_description(d) {
    var description = d.description;
    return  '<b>' + d.name + '</b></br>'+ d.description + '<br> (' + format_number(d.value) + ')';
}

function computeTextRotation(d) {
    return (d.x +d.dx/2)*180/Math.PI - 90;
}

function mouseOverArc(d) {
    d3.select(this).attr("stroke","black");
}

function mouseOutArc(){
    d3.select(this).attr("stroke","")
}

function mouseMoveArc (d) {
}

var root_ = null;

d3.json("flare.json", function(error, root) {
    if (error) return console.warn(error);
    // Compute the initial layout on the entire tree to sum sizes.
    // Also compute the full name and fill color for each node,
    // and stash the children so they can be restored as we descend.

    partition
        .value(function(d) { return d.size; })
        .nodes(root)
        .forEach(function(d) {
            d._children = d.children;
            d.sum = d.value;
            d.key = key(d);
            d.fill = fill(d);
        });

    // Now redefine the value function to use the previously-computed sum.
    partition
        .children(function(d, depth) { return depth < 2 ? d._children : null; })
        .value(function(d) { return d.sum; });

    var center = svg.append("circle")
        .attr("r", radius / 3)
        .on("click", zoomOut);

    center.append("title")
        .text("zoom out");

    var partitioned_data=partition.nodes(root).slice(1);

    var path = svg.selectAll("path")
        .data(partitioned_data)
        .enter().append("path")
        .attr("d", arc)
        .style("fill", function(d) { return fill(d); })
        .each(function(d) { this._current = updateArc(d); })
        .on("click", zoomIn)
        .on("mouseover", mouseOverArc)
        .on("mousemove", mouseMoveArc)
        .on("mouseout", mouseOutArc);


    var texts = svg.selectAll("text")
        .data(partitioned_data)
        .enter().append("text")
        .filter(filter_min_arc_size_text)
        .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
        .attr("x", function(d) { return radius / 3 * d.depth; })
        .attr("dx", function(d) { return 25; }) // margin
        .attr("dy", ".35em") // vertical-align
        .text(function(d,i) {return d.name});

    function zoomIn(p,d) {
        myFunction(p);
        if (p.depth > 1) p = p.parent;
        if (!p.children) return;
        zoom(p, p);
    }

    function zoomOut(p) {
        if (!p.parent) return;
        zoom(p.parent, p);
    }

    // Zoom to the specified new root.
    function zoom(root, p) {
        if (document.documentElement.__transition__) return;

        // Rescale outside angles to match the new layout.
        var enterArc,
            exitArc,
            outsideAngle = d3.scale.linear().domain([0, 2 * Math.PI]);

        function insideArc(d) {
            return p.key > d.key
                ? {depth: d.depth - 1, x: 0, dx: 0} : p.key < d.key
                    ? {depth: d.depth - 1, x: 2 * Math.PI, dx: 0}
                    : {depth: 0, x: 0, dx: 2 * Math.PI};
        }

        function outsideArc(d) {
            return {depth: d.depth + 1, x: outsideAngle(d.x), dx: outsideAngle(d.x + d.dx) - outsideAngle(d.x)};
        }

        center.datum(root);

        // When zooming in, arcs enter from the outside and exit to the inside.
        // Entering outside arcs start from the old layout.
        if (root === p) enterArc = outsideArc, exitArc = insideArc, outsideAngle.range([p.x, p.x + p.dx]);

        var new_data=partition.nodes(root).slice(1);

        path = path.data(new_data, function(d) { return d.key; });

        // When zooming out, arcs enter from the inside and exit to the outside.
        // Exiting outside arcs transition to the new layout.
        if (root !== p) enterArc = insideArc, exitArc = outsideArc, outsideAngle.range([p.x, p.x + p.dx]);

        d3.transition().duration(d3.event.altKey ? 7500 : 750).each(function() {
            path.exit().transition()
                .style("fill-opacity", function(d) { return d.depth === 1 + (root === p) ? 1 : 0; })
                .attrTween("d", function(d) { return arcTween.call(this, exitArc(d)); })
                .remove();

            path.enter().append("path")
                .style("fill-opacity", function(d) { return d.depth === 2 - (root === p) ? 1 : 0; })
                .style("fill", function(d) { return fill(d); })
                .on("click", zoomIn)
                .on("mouseover", mouseOverArc)
                .on("mousemove", mouseMoveArc)
                .on("mouseout", mouseOutArc)
                .each(function(d) { this._current = enterArc(d); });


            path.transition()
                .style("fill-opacity", 1)
                .attrTween("d", function(d) { return arcTween.call(this, updateArc(d)); });



        });


        texts = texts.data(new_data, function(d) { return d.key; });

        texts.exit()
            .remove();
        texts.enter()
            .append("text");

        texts.style("opacity", 0)
            .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
            .attr("x", function(d) { return radius / 3 * d.depth; })
            .attr("dx", "6") // margin
            .attr("dy", ".35em") // vertical-align
            .filter(filter_min_arc_size_text)
            .text(function(d,i) {return d.name})
            .transition().delay(750).style("opacity", 1)


    }
});


function key(d) {
    var k = [], p = d;
    while (p.depth) {
        k.push(p.name);
        p = p.parent;
    }
    return k.reverse().join(".");
}


function arcTween(b) {
    var i = d3.interpolate(this._current, b);
    this._current = i(0);
    return function(t) {
        return arc(i(t));
    };
}

function updateArc(d) {
    return {depth: d.depth, x: d.x, dx: d.dx};
}


function fill(d) {
    if(d.arcID && d.arcID == currentSelectedID) {
        return currentColor;
    }
    var p = d;
    while (p.depth > 1) p = p.parent;
    var c = d3.lab(hue(p.name));
    c.l = luminance(d.sum);
    return c;
}

d3.select(self.frameElement).style("height", margin.top + margin.bottom + "px");

/***** ULC Script ****/
var departmentCardDiv = $("#departmentCardDiv");
var courseCardDiv = $("#courseCardDiv");

departmentCardDiv.hide();

function myFunction(d) {
    if(d.arcType && d.arcType === "DEPT") {
        var deptName = d.departmentCode;
        $.ajax({
            'method': 'POST',
            'url': "department.manager.php",
            'data': {
                'department': deptName
            },
            'error': function () {
                //window.alert(deptName + " Data Not Present");
            },
            'success': function (res) {
                departmentCardDiv.fadeIn();
                $('#departmentCardContent').html(res);

                $('.collapsible').collapsible();


                $('.deptCourseChip').click(function () {
                    var course = $(this).attr('data-course');
                    loadCourse(course);
                });
            }
        });
    }else if(d.arcType && d.arcType == "SUBJECT") {
        var courseCode = d.courseCode;
        loadCourse(courseCode)

    }
}

function loadCourse(courseCode) {
    $.ajax({
        'method': 'POST',
        'url': 'course.manager.php',
        'data': {
            'courseID': courseCode
        },
        'error': function () {
            //window.alert(courseCode + " Data Not Present");
        },
        'success': function (res) {
            courseCardDiv.fadeIn();
            $('#courseCardContent').html(res);

            initiateCourseClick();
        }
    });
}

/*
function initiateDepartmentClick() {
    $('.cardBtnDept').click(function () {
        $('.cardBtnDept').each(function () {
            $(this).css('color',"#000000");
            $(this).css('background-color',"#FFFFFF");

        });

        var c = $(this).attr('data-color');
        var id = $(this).attr('data-key');

        var rgb = parseInt(c, 16);   // convert rrggbb to decimal
        var r = (rgb >> 16) & 0xff;  // extract red
        var g = (rgb >>  8) & 0xff;  // extract green
        var b = (rgb >>  0) & 0xff;  // extract blue

        var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

        if (luma < 40) {
            $(this).css('color',"#FFFFFF");
        }

        $(this).css('background-color',"#"+c);

        changeD3Color(id,c);
    });
}

*/

function changeD3Color(key,col) {
    currentSelectedID = key;
    currentColor = col;
    d3.selectAll("path").each(function (d) {
        if(d.arcType && d.arcType == "SUBJECT") {
            if(d.arcID == key) {
                this.style.fill = col;
            }else {
                this.style.fill = fill(d);
            }
        }
    });
}

function initiateCourseClick() {

}
