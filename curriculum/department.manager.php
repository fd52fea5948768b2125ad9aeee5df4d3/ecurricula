<?php
include_once(__DIR__."/../includes/general.config.php");
include_once(__DIR__."/../includes/mongo.db.config.php");
$client = new MongoDB\Driver\Manager($MONGO_URL);

$DEPARTMENT_CODE = $_POST['department'];

$DEPARTMENT_LIST = [["CSE","Computer Science and Eng."],["IT","Information Technology"],
    ["SE","Software Engineering"],["CE","Civil Engineering"],
    ["ME","Mechanical engineering"],["AE","Automobile Engineering"],
    ["AEE","Aerospace Engineering"],["MEC","Mechatronics"],
    ["EC","Electronics and Comm."],["TC","Telecommunication"],
    ["EEE","Electrical & Electronics"],["EI","Electronics and Instru."],
    ["ICE","Instru. & Ctrl Eng"],["CE","Chemical Engineering"],
    ["BT","Biotechnology"],["BE","Biomedical Engineering"],
    ["GE","Genetic Engineering"],["FPE","Food Process Engineering"],
    ["N.Tech","Nanotechnology"],["NE","Nuclear Engineering"],["CS","Cognitive Science"]];
$DEPARTMENT_NAME = "Department of ";
foreach ($DEPARTMENT_LIST as $depts) {
    if($depts[0] == $DEPARTMENT_CODE) {
        $DEPARTMENT_NAME = $DEPARTMENT_NAME.$depts[1];
        break;
    }
}

$PEOS = [['JT','Job Title Options'],['HS','Higher Study Options'],["EO","Entrepreneur Options"]];

echo "<h5>$DEPARTMENT_NAME</h5>
            <hr>
            <h6>Future Prospects</h6>
";

/*
if(sizeof($JOB_TITLES) > 0) {
    $globalCount = 0;
    $count = 0;
    foreach ($JOB_TITLES as $title) {
        $id=$DEPARTMENT_CODE."".$globalCount;
        $color = $JOB_COLOR[$count];
        echo "<div class=\"card\">
                    <div data-key='$id' data-color='$color' class=\"cardBtnDept card-content\">
                        $title
                    </div>
                </div>";
        $globalCount++;
        $count++;
    }
    echo "</div></br>";
}
*/

/*
 *

      <span>Lorem ipsum dolor sit amet.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
    </li>
  </ul>
*/

foreach ($PEOS as $PEO) {
    $query = ['department' => $DEPARTMENT_CODE,'category'=>$PEO[0]];
    $option = [];

    $query = new MongoDB\Driver\Query($query,$option);

    $rows = $client->executeQuery("$DB_NAME.D3_FUTURE_PROSPECTS",$query);
    $rows = $rows->toArray();

    if(sizeof($rows) == 0) continue;

    echo "<p class=\"grey-text text-lighten-3\">$PEO[1]</p><div class=\"black-text sub-flex\">";
    echo "<ul class=\"collapsible\" data-collapsible=\"accordion\">";
    foreach ($rows as $row) {
        echo "<li><div class=\"collapsible-header\">$row->_id</div><div class=\"collapsible-body white\"><span><span class='hint'>Courses to Study: <br></span> <br>";

        foreach ($row->courses as $course) {
            echo "<div data-course='$course' class='chip deptCourseChip'>$course</div>  <br>";
        }

        echo "</span></div><li>";
    }
    echo "</ul>";
}
?>
