<?php

$DEPARTMENT_NAME = "Department of Information Technology";
$DEPARTMENT_CODE = "IT";
$JOB_TITLES = ['Computer Programmers','System Software Engineer','Web developers','Network and Database Admin'];
$JOB_COLOR = ['0000FF','00FF00','0F0F0F','0F00FF'];
$HIGHER_STUDY_OPTIONS = [];
$HIGHER_STUDY_COLOR = [];
$ENTREPRENEUR_OPTION = [];
$ENTREPRENEUR_COLOR = [];

echo "<h5>$DEPARTMENT_NAME</h5>
            <hr>
            <h6>Future Prospects</h6>
";

if(sizeof($JOB_TITLES) > 0) {
    echo "<p class=\"grey-text text-lighten-1\">Job Title Options</p><div class=\"black-text sub-flex\">";
    $globalCount = 0;
    $count = 0;
    foreach ($JOB_TITLES as $title) {
        $id=$DEPARTMENT_CODE."".$globalCount;
        $color = $JOB_COLOR[$count];
        echo "<div class=\"card low-pad\">
                    <div data-key='$id' data-color='$color' class=\"cardBtnDept low-pad card-content\">
                        $title
                    </div>
                </div>";
        $globalCount++;
        $count++;
    }
    echo "</div></br>";
}

if(sizeof($HIGHER_STUDY_OPTIONS) > 0) {
    echo "<p class=\"grey-text text-lighten-1\">Higher Study Options</p><div class=\"black-text sub-flex\">";
    $count = 0;
    foreach ($HIGHER_STUDY_OPTIONS as $title) {
        $id=$DEPARTMENT_CODE."".$globalCount;
        $color = $HIGHER_STUDY_COLOR[$count];
        echo "<div class=\"card\">
                    <div data-key='$id' data-color='$color' class=\"cardBtnDept card-content\">
                        $title
                    </div>
                </div>";
        $globalCount++;

        $count++;
    }
    echo "</div></br>";

}

if(sizeof($ENTREPRENEUR_OPTION) > 0) {
    echo "<p class=\"grey-text text-lighten-1\">Entrepreneur Options</p><div class=\"black-text sub-flex\">";
    $count = 0;
    foreach ($ENTREPRENEUR_OPTION as $title) {
        $id=$DEPARTMENT_CODE."".$globalCount;
        $color = $ENTREPRENEUR_COLOR[$count];
        echo "<div class=\"card\">
                    <div data-key='$id' data-color='$color' class=\"cardBtnDept card-content\">
                        $title
                    </div>
                </div>";
        $globalCount++;

        $count++;
    }
    echo "</div></br>";
}
?>
