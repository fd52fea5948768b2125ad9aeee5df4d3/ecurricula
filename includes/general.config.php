<?php
    $NAVBAR_TEXT = "SRM eCurricula";
    $HREF_URL = "http://www.srmuniv.ac.in";

    $DEBUG = true;
    if($DEBUG) {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    $SCHOOL = 0;

    $MAPPING_DATA_ET = [["Apply Math, Science &amp; Eng.","A"],["Design, Conduct, Experiments","B"],
        ["Design System, Component or Procedure","C"],["Work in Multidisciplinary teams","D"],
        ["Identify & Solve Eng. Problems","E"],["Professional & Ethical responsibility","F"],
        ["Communicate Effectively","G"],["Impact of Eng. Solutions","H"],
        ["Life Long Learning","I"],["Contemporary Issues","J"],
        ["Modern Engineering Tools","K"]];

    $A_K_MAPPING_DATA = [$MAPPING_DATA_ET];
    $A_K_MAPPING = $A_K_MAPPING_DATA[$SCHOOL];



?>