$(document).ready(function () {
    var progressBar = $(".progress");
    var errorDiv = $(".errorDiv");

    $('.modal').modal();

    progressBar.hide();
    errorDiv.hide();

    $('body').keyup(function (event) {
        if (event.keyCode == 13) {
            $("#loginBtn").click();
        }
    });


    $("#loginBtn").click(function () {
        var username = $("#uname").val();
        var password = $("#pwd").val();

        $.ajax({
            'method': 'POST',
            'data': {
                'username': username,
                'password': password
            },
            'url': './login/login.verify.php',
            'dataType': 'JSON',
            'error': function () {
                progressBar.hide();
                errorDiv.show();
                errorDiv.text("Error: 0X0001");
            },
            'beforeSend': function () {
                errorDiv.hide();
                progressBar.show();
            },
            'success': function (res) {
                progressBar.hide();
                if (res.error == '200') {
                    if (res.role == 'S') {
                        window.location.href = './login/student';
                    }else if(res.role == 'C') {
                        window.location.href = "./login/coordinator";
                    } else if(res.role == 'A') {
                        window.location.href = "./login/admin";
                    }else if(res.role == 'F') {
                        window.location.href = './login/faculty';
                    }else if(res.role == 'O') {
                        window.location.href = './login/reports';
                    }else if(res.role == 'D') {
                        window.location.href = './login/departments';
                    }
                } else {
                    errorDiv.show();
                    errorDiv.text("Wrong Credentials");
                }
            }
        });
    });
});