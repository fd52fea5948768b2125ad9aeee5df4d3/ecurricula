<?php
    include_once(__DIR__."/includes/general.config.php");
    session_start();
    session_destroy();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="./favicon.ico">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="static/css/materialize.min.css">
    <title>Login</title>
    <style>
        .flex-main {
            min-height: 80vh;
            display: flex;
            align-content: center;
            align-items: center;
            justify-content: center;
        }
        .login {
            width: 400px;
            padding-bottom: 25px;
            max-width: 100vw;
        }
        .card-title {
            padding: 5px 0 5px 15px;
        }
        .card-content {
            padding: 5px;
        }
        nav div a img.logo-img {
            height: 100%;
            padding: 4px;
        }
        .seperator {
            width: 100%;
            border-bottom: 1px solid;
            border-color: #cfd8dc;
            clear: both;
        }
        .extra {
            padding: 14px !important;
        }
        .foot-text {
             text-transform: uppercase;
             color: #757575;
        }
        .card {
            padding-bottom: 0;
        }

    </style>
</head>
<body>

<nav>
    <div class="nav-wrapper  green darken-2">
        <a href="<?php echo $HREF_URL; ?>">
            <span class="hide-on-med-and-down">
            <img id="image" class="brand-logo logo-img s2" src="static/images/logo.png"></span>
        </a>
        <a href="#" class="brand-logo  center hide-on-small-only"><?php echo $NAVBAR_TEXT; ?></a>
    </div>
</nav>

<div class="container flex-main">
    <div class="card login">
        <div class="card-title green darken-2">
            <h5 class="white-text">eCurricula SIGN IN</h5>
        </div>
        <div class="card-content">
         
            <div class="input-field">
                <i class="fa fa-user prefix"></i>
                <input id="uname" type="text">
                <label for="uname">Username</label>
            </div>

            <div class="input-field">
                <i class="fa fa-lock prefix"></i>
                <input id="pwd" type="password">
                <label for="pwd">Password</label>
            </div>
            <div class="progress">
                <div class="indeterminate grey darken-3"></div>
            </div>
            <div class="errorDiv center-align red-text text-darken-3">
                Error will go here
            </div>
            <div class="right">
                <br>
                <a class="btn btn-flat grey-text text-darken-3" href="#modal2"/"><b>Register</b></a>
                <a class="btn grey darken-3" id="loginBtn"><b>LOGIN</b></a>
            </div>

            <div class="clearfix"> </div>
        </div>
        <div class="seperator grey"> </div>
        <div class="extra">
            <a class="left foot-text" href="#modal1" id="about">About </a>
            <a class="right foot-text" href="./curriculum" target="_blank">Curriculum</a>
            <div style="clear: both"></div>
        </div>


    </div>
</div>

<div id="modal1" class="modal">
    <div class="modal-content">
        <h4 class="center-align"> SRM University eCurricula</h4>

        <p class="space-top" id="modal-content-msg"> eCurricula is a learning management tool for engineering courses.</p><br>

        <p class="space-top red-text text-darken-3 right-align" id="modal-footer-msg">* For any Queries Contact Your Respective Faculty Advisor</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-blue btn-flat">CLOSE</a>
    </div>
</div>

<div id="modal2" class="modal">
    <div class="modal-content">
        <h4 class="center-align"> Registration</h4><hr>
        <br><br>

        <div class="row">
            <div class="col s12 m6 center">
                <a class="btn blue darken-1 white-text" href="register/faculty">Faculty Register</a>
            </div>

            <div class="col s12 m6 center">
                <a class="btn green darken-1 white-text" href="register">Student Register</a>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-blue btn-flat">CLOSE</a>
    </div>
</div>


<!-- JS FILES DON'T CHANGE -->
<script type="text/javascript" src="static/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="static/js/materialize.min.js"></script>
<script type="text/javascript" src="script.js"></script>

</body>
</html>
